# Restaurant React App

This is a single page application written in React.

## Instructions to develop
- `yarn install`
- `yarn start` - starts a dev server
- `yarn test` - runs the tests (Jest)
- `yarn prettier` - runs prettier agains all files

[See create-react-app for more details](https://github.com/facebookincubator/create-react-app)

## Environment Variables
Development variables are stored inside `.env.development`.

If you want to overwrite any variable just add the correct variable into your operative system environment variables. Make sure to restart the terminal and run again `yarn start`. 

## Local api to test
A mocking api is available at `mockApi` folder and will start at `http://localhost:4000`

To use this api: create a `.env.development.local` env file with the var: `REACT_APP_API_URL=http://localhost:4000`

Another Readme is provided in the `mockApi` folder.

## Compile to production
- `yarn build` - compiles the app to the build folder

## Prettier
Formatting is done via precommit hook using `husky` and `prettier`.

## Folder structure
Followed these patterns:

- **Reducers files**: https://github.com/erikras/ducks-modular-redux
- **Components**: https://www.smashingmagazine.com/2016/09/how-to-scale-react-applications/
