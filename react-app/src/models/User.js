import * as roles from './Roles'

/**
 * User Domain Model
 */
class User {
  constructor(id, name, phone, role, companyId) {
    this.id = id
    this.name = name
    this.phone = phone
    this.role = role // array
    this.companyId = companyId
  }

  hasRole(role) {
    if (this.role === role) {
      // direct role
      return true
    }

    // role does not exist! something is wrong
    if (!roles.defaultRoles[this.role]) return false

    // not a direct role? lets search in the child roles
    return roles.defaultRoles[this.role].includes(role)
  }

  canEditRole(employee) {
    return roles.defaultRoles[this.role].includes(employee.role)
  }
}

export default User
