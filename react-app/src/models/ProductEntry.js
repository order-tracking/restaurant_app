/**
 * Container for grouping Product Response with the choosen ItemType
 */
class ProductEntry {
  constructor(product, type, qty = 1) {
    this.product = product // product view dto
    this.type = type // product type view dto
    this.qty = qty
    this.note = ''
  }
}

export default ProductEntry
