/**
 * Default Roles
 */
export const DEVELOPER = 'Developer'
export const OWNER = 'Owner'
export const ADMIN = 'Admin'
export const EMPLOYEE = 'Employee'
export const DISTRIBUTOR = 'Distributor'

// just a few roles, easier to manage this way :p
export const defaultRoles = {
  [DEVELOPER]: [OWNER, ADMIN, EMPLOYEE, DISTRIBUTOR],
  [OWNER]: [ADMIN, EMPLOYEE, DISTRIBUTOR],
  [ADMIN]: [EMPLOYEE, DISTRIBUTOR],
  [EMPLOYEE]: [DISTRIBUTOR],
  [DISTRIBUTOR]: []
}

export function getAllowedRoles(role) {
  const toReturn = defaultRoles[role]
  if (toReturn) return toReturn
  throw new Error('No Role named ' + role)
}
