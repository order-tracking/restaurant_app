import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import registerServiceWorker from './registerServiceWorker'
import configureStore from './redux/store'
import Dashboard from './features/dashboard/Dashboard'
import Home from './features/home/Home'
import Auth from './features/auth/Auth'
import PageNotFoundComponent from './components/PageNotFoundComponent'
// import { Log } from 'oidc-client'
import 'string.prototype.includes'
import 'semantic-ui-css/semantic.min.css'
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css'
import './assets/css/main.css'
import { createUserManager } from './helpers/OIDCHelpers'

if (process.env.NODE_ENV !== 'production') {
  // Log.level = Log.INFO
  // Log.logger = console // add logging to the oidc customer
}

// Single instance for all the app, handles silent renew
window.UserManager = createUserManager()

// Axios needs to be loaded after UserManager
require('./repositories/axiosConfig')

const store = configureStore()
window.Store = store // needed the HandleAuthComponent

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Switch>
            <Route path="/dashboard" component={Dashboard} />
            <Route path="/auth" component={Auth} />
            <Route path="/" component={Home} />
            <Route component={PageNotFoundComponent} />
          </Switch>
        </Router>
      </Provider>
    )
  }
}

ReactDOM.render(<App />, document.getElementById('root'))
registerServiceWorker()

// Check env vars
console.log('API_URL', process.env.REACT_APP_API_URL)
console.log('IDENTITY_URL', process.env.REACT_APP_IDENTITY_URL)
console.log('HYPERTRACK_KEY', process.env.REACT_APP_HYPERTRACK_KEY)
