import { combineReducers } from 'redux'
import { reducer as toastrReducer } from 'react-redux-toastr'
import * as dashboardReducers from '../features/dashboard/reducers'
import * as authReducers from '../features/auth/reducers'

// Where we can add more reducers
const rootReducer = combineReducers({
  ...dashboardReducers,
  ...authReducers,
  toastr: toastrReducer
})

export default rootReducer
