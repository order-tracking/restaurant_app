import { createStore, applyMiddleware } from 'redux'
import rootReducer from './rootReducer'
import thunk from 'redux-thunk'
import apiCallMiddleware from './middlewares/apiCallMiddleware'

// Chrome Redux Devtools
import { composeWithDevTools } from 'redux-devtools-extension'

const middleware = [apiCallMiddleware, thunk]

// Only include redux-immutable-state-invariant in development
if (process.env.NODE_ENV !== 'production') {
  middleware.push(require('redux-immutable-state-invariant').default())
}

function configureStore(initialState) {
  return createStore(
    rootReducer,
    initialState,
    composeWithDevTools(applyMiddleware(...middleware))
  )
}

export default configureStore
