import React from 'react'
import { Segment, Header } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import { Helmet } from 'react-helmet'

class UnAuthorizedComponent extends React.PureComponent {
  render() {
    const { homeLink } = this.props
    return (
      <div>
        <Helmet title="Not Authorized" />
        <Header as="h4" attached="top">
          401 Not Authorized
        </Header>
        <Segment attached>
          <p>Sorry bro, you have not permission to acess that page.</p>
          <p>
            Go to back to the <Link to={homeLink}>beginning</Link>.
          </p>
        </Segment>
      </div>
    )
  }
}

UnAuthorizedComponent.defaultProps = {
  homeLink: '/'
}

export default UnAuthorizedComponent
