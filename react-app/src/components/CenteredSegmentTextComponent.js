import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Segment } from 'semantic-ui-react'

class CenteredSegmentTextComponent extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <Segment basic textAlign="center">
        <strong>{this.props.text}</strong>
      </Segment>
    )
  }
}

CenteredSegmentTextComponent.propTypes = {
  text: PropTypes.string.isRequired
}

export default CenteredSegmentTextComponent
