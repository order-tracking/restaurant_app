import React from 'react'
import Segment from 'semantic-ui-react/dist/es/elements/Segment/Segment'

const PageLoadingComponent = () => {
  return <Segment className="page-loader" loading={true} />
}

export default PageLoadingComponent
