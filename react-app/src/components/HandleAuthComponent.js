import React from 'react'
import PageLoadingComponent from './PageLoadingComponent'
import { login } from '../features/auth/authReducer'

/**
 * Tries to load the user and renew the token before loading the children
 * components
 */
class HandleAuthComponent extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true
    }
  }

  componentDidMount() {
    // Login the user or renew the access token if needed
    window.UserManager
      .getUser()
      .then(user => {
        if (user && user.expired) {
          // silent renew
          return window.UserManager.signinSilent()
        }

        return user
      })
      .then(user => {
        if (user) window.Store.dispatch(login(user))
        this.setState({ loading: false })
      })
      .catch(error => {
        console.log('No user or silent renew failed', error)
        // The cookie from the OP is expired, we need to login again from the
        // front channel
        if (error.message === 'login_required') {
          this.setState({ loading: false })
          // dont force login, let the user continue logged out
          // window.UserManager
          //   .signinRedirect()
          //   .then(user => {
          //     if (!user) {
          //       return
          //     }
          //
          //     window.Store.dispatch(login(user))
          //     this.setState({ loading: false })
          //   })
          //   .catch(error => {
          //     console.error(error)
          //     this.setState({ loading: false })
          //   })
        } else {
          this.setState({ loading: false })
        }
      })
  }

  render() {
    if (this.state.loading) {
      return <PageLoadingComponent />
    } else {
      return <div>{this.props.children}</div>
    }
  }
}

export default HandleAuthComponent
