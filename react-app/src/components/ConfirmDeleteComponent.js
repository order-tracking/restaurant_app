import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Confirm } from 'semantic-ui-react'

class ConfirmDeleteComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      confirmOpen: false
    }

    this.closeConfirm = () => this.setState({ confirmOpen: false })
    this.openConfirm = () => this.setState({ confirmOpen: true })
  }

  render() {
    return (
      <div>
        <Button
          type="button"
          floated={this.props.floated}
          color={this.props.color}
          onClick={this.openConfirm}
        >
          Delete
        </Button>
        <Confirm
          open={this.state.confirmOpen}
          content="Are you sure?"
          onCancel={this.closeConfirm}
          onConfirm={this.props.delete}
        />
      </div>
    )
  }
}

ConfirmDeleteComponent.propTypes = {
  delete: PropTypes.func.isRequired,
  floated: PropTypes.string,
  color: PropTypes.string
}

export default ConfirmDeleteComponent
