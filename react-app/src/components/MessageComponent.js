import React from 'react'
import PropTypes from 'prop-types'
import { Message } from 'semantic-ui-react'

/**
 * Same as Message component from Semantic Ui
 * but with a time prop to auto hide
 *
 * example:
 * <Message time={3000} content='All others semantic props here' />
 */
class MessageComponent extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      visible: true
    }

    this.handleDismiss = this.handleDismiss.bind(this)
  }

  componentDidMount() {
    if (this.props.time) {
      setTimeout(() => {
        this.handleDismiss()
      }, this.props.time)
    }
  }

  handleDismiss() {
    this.setState({ visible: false })

    if (this.props.onDismiss) this.props.onDismiss()
  }

  render() {
    const { time, ...props } = this.props // exclude time from props

    if (this.state.visible) {
      return <Message onDismiss={this.handleDismiss} {...props} />
    }

    return null
  }
}

MessageComponent.propTypes = {
  content: PropTypes.string.isRequired
}

export default MessageComponent
