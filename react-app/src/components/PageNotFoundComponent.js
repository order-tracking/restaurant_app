import React from 'react'
import { Segment, Header } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import { Helmet } from 'react-helmet'

class PageNotFoundComponent extends React.PureComponent {
  render() {
    const { homeLink } = this.props

    return (
      <div>
        <Helmet title="Not Found" />
        <Header as="h4" attached="top">
          404 Not Found
        </Header>
        <Segment attached>
          <p>That page does not exist.</p>
          <p>
            Go to back to the <Link to={homeLink}>beginning</Link>.
          </p>
        </Segment>
      </div>
    )
  }
}

PageNotFoundComponent.defaultProps = {
  homeLink: '/'
}

export default PageNotFoundComponent
