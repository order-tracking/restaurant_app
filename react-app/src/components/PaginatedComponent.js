import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, List } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'

class PaginatedComponent extends Component {
  render() {
    return (
      <div>
        <List animated celled verticalAlign="middle">
          {this.props.children}
        </List>
        {(this.props.hasNextPage || this.props.hasPreviousPage) && (
          <div>
            <Button
              floated="right"
              disabled={!this.props.hasNextPage}
              as={NavLink}
              to={this.props.nextLink}
            >
              Next
            </Button>
            <Button
              floated="left"
              disabled={!this.props.hasPreviousPage}
              as={NavLink}
              to={this.props.previousLink}
            >
              Previous
            </Button>
          </div>
        )}
      </div>
    )
  }
}

PaginatedComponent.propTypes = {
  hasNextPage: PropTypes.bool.isRequired,
  nextLink: PropTypes.string.isRequired,
  hasPreviousPage: PropTypes.bool.isRequired,
  previousLink: PropTypes.string.isRequired
}

export default PaginatedComponent
