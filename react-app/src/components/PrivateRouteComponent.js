import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import PropTypes from 'prop-types'

class PrivateRoute extends React.Component {
  render() {
    let { component: Component, session, minRole, ...rest } = this.props
    let redirectPath = '/'
    let hasPermission = false

    if (session.isAuthenticated) {
      if (session.user.hasRole(minRole)) {
        hasPermission = true
      } else {
        redirectPath = '/dashboard/unauthorized'
      }
    }

    return (
      <Route
        {...rest}
        render={props =>
          hasPermission ? (
            <Component {...props} />
          ) : (
            <Redirect
              to={{ pathname: redirectPath, state: { from: props.location } }}
            />
          )}
      />
    )
  }
}

PrivateRoute.propTypes = {
  minRole: PropTypes.string.isRequired,
  component: PropTypes.any.isRequired,
  session: PropTypes.object.isRequired,
  path: PropTypes.string.isRequired
}

export default PrivateRoute
