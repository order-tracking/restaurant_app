import React from 'react'
import PropTypes from 'prop-types'
import ValidationField from './ValidationField'
import { Form, Message } from 'semantic-ui-react'

/**
 * Example usage:
 *
 * The onSubmit event sends an object key-pair with the input name and value
 * and is only triggered when all fields are valid
 *
 * Caution with parent renders, rendering this component can be costly
 *
 * If the user submits the form clicking enter, caution because the onSubmit
 * will be called for every enter press
 *
 * <FormValidation onSubmit={this.handleSubmit}>
 *    <ValidationField
 *       name="name"
 *       label="Name"
 *       required={false} // by default is true, no need to pass this prop
 *       rules={[isEmail, min(6)]}
 *    />
 *    <Button type="submit">Create</Button>
 * </FormValidation>
 */
class FormValidation extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      revealError: this.props.revealErrors,
      customError: this.props.customError
    }

    // no need to be in state, we do not want to re-render
    this.fieldCount = 0
    this.allFieldStatus = {}
    this.allFieldValues = {}

    this.handleStatus = this.handleStatus.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleDismiss = this.handleDismiss.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    // when the props changes we have to update the state manually
    if (nextProps.customError !== this.state.customError) {
      this.setState({ customError: nextProps.customError })
    }
  }

  updateAllFields() {
    this.fieldCount = 0

    return this.recursiveCloneChildren(this.props.children)
  }

  recursiveCloneChildren(children) {
    // pass onStatus handler to children input fields
    return React.Children.map(children, child => {
      var childProps = {}
      if (React.isValidElement(child) && child.type === ValidationField) {
        childProps = {
          onStatus: this.handleStatus,
          revealError: this.state.revealError
        }
        this.fieldCount++
      }
      if (child && child.props) {
        // String has no Prop
        childProps.children = this.recursiveCloneChildren(child.props.children)
        return React.cloneElement(child, childProps)
      }
      return child
    })
  }

  handleStatus(name, isValid, value) {
    this.allFieldStatus[name] = isValid
    this.allFieldValues[name] = value
  }

  handleSubmit(e) {
    e.preventDefault()

    this.triggerValidation()
  }

  allFieldsValid() {
    let totalValid = 0
    for (let field in this.allFieldStatus) {
      if (this.allFieldStatus[field]) {
        totalValid++
      }
    }

    return totalValid === this.fieldCount
  }

  /**
   * Calls the optional func if all fields are valid or the submit handler
   * if no optional func is passed
   * @param optionalFunc
   */
  triggerValidation(optionalFunc = null) {
    if (this.allFieldsValid()) {
      if (optionalFunc) {
        optionalFunc(this.allFieldValues)
      } else {
        this.props.onSubmit(this.allFieldValues)
      }
    } else {
      // re-render to force displaying all errors
      if (!this.state.revealError) this.setState({ revealError: true })
    }
  }

  handleDismiss() {
    this.setState({ customError: null })
  }

  render() {
    // this operation is cloning children ValidationField elements
    // can be costly watchout for parent renders
    // we can debounce to only run this once but then elements inside will
    // not be rendered if changed by the parent component
    let children = this.updateAllFields()

    return (
      <Form onSubmit={this.handleSubmit}>
        {this.state.customError && (
          <Message
            negative
            onDismiss={this.handleDismiss}
            content={this.state.customError}
          />
        )}

        {children}
      </Form>
    )
  }
}

FormValidation.defaultProps = {
  revealErrors: false,
  customError: null
}

FormValidation.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  revealErrors: PropTypes.bool,
  customError: PropTypes.string
}

export default FormValidation
