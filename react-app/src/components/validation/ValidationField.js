import React from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Label } from 'semantic-ui-react'

/**
 * The validation occurs in the onChange event
 *
 * you can pass the events onChange and onBlur if you need the value
 * before submitting
 *
 * Name and value are passed to the events:
 * onChange((name, value) => {})
 * onBlur((name, value) => {})
 *
 * Example rule:
 *
 * Should return a string in case of error, and true if valid
 * function noSpaces(value) {
 *   if(value.indexOf(' ') !== -1) {
 *      return 'This field cannot contain spaces.'
 *   }
 *
 *   return true
 * }
 *
 *  <ValidationField
 *     name="name"
 *     label="Name"
 *     required={false} // by default is true, no need to pass this prop
 *     rules={[isEmail, min(6)]}
 *  />
 */
class ValidationField extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      name: this.props.name,
      value: this.props.value,
      touched: false,
      error: false,
      errorMessage: null
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleBlur = this.handleBlur.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== '' && nextProps.value !== this.state.value) {
      this.changedValue(nextProps.value)
    }
  }

  componentDidMount() {
    // we want to emit the success feedback event if our input is not required
    this.validateField(this.state.value)
  }

  handleChange(event) {
    this.changedValue(event.target.value)
  }

  changedValue(value) {
    this.setState({ value: value, touched: true })

    if (this.props.onChange) {
      this.props.onChange(this.props.name, value)
    }

    this.validateField(value)
  }

  handleBlur(event) {
    if (this.props.onBlur) {
      this.props.onBlur(this.props.name, event.target.value)
    }
  }

  validateField(val) {
    if (this.props.required || (!this.props.required && val.length > 0)) {
      for (let validator of this.props.rules) {
        // validator returns the error string if not valid
        let res = validator(val)
        if (res !== true) {
          this.sendNegativeFeedback(val, res)
          return
        }
      }
    }

    if (this.props.required) {
      if (val === '') {
        this.sendNegativeFeedback(val, 'This field is required.')
      } else {
        this.sendPositiveFeedback(val)
      }
    } else {
      this.sendPositiveFeedback(val)
    }
  }

  sendPositiveFeedback(value) {
    this.setState({
      error: false,
      errorMessage: null
    })
    this.props.onStatus(this.props.name, true, value)
  }

  sendNegativeFeedback(value, message) {
    this.setState({
      error: true,
      errorMessage: message
    })
    this.props.onStatus(this.props.name, false, value)
  }

  render() {
    const { label, name, placeholder, type, action, fluid } = this.props
    const { value } = this.state
    const showError =
      (this.props.revealError || this.state.touched) && this.state.error

    return (
      <Form.Field error={showError} required={this.props.required}>
        <label>{label}</label>

        <Input
          fluid={fluid}
          name={name}
          value={value}
          placeholder={placeholder}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
          type={type}
          action={action}
        />

        {showError && (
          <Label basic color="red" pointing>
            {this.state.errorMessage}
          </Label>
        )}
      </Form.Field>
    )
  }
}

ValidationField.defaultProps = {
  placeholder: '',
  value: '',
  required: true,
  type: 'text',
  rules: [],
  revealError: false,
  action: false,
  fluid: true
}

ValidationField.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  revealError: PropTypes.bool,
  onChange: PropTypes.func,
  onStatus: PropTypes.func,
  onBlur: PropTypes.func,
  rules: PropTypes.array,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  value: PropTypes.any,
  type: PropTypes.string,
  action: PropTypes.any,
  fluid: PropTypes.bool
}

export default ValidationField
