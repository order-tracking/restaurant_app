import React from 'react'
import PropTypes from 'prop-types'
import PageLoadingComponent from '../../components/PageLoadingComponent'

class OIDCCallback extends React.Component {
  componentDidMount() {
    this.props.callback() // invoke the callback
  }

  render() {
    return <PageLoadingComponent />
  }
}

OIDCCallback.propTypes = {
  callback: PropTypes.func.isRequired
}

export default OIDCCallback
