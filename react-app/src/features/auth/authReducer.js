import * as reduxHelpers from '../../helpers/ReduxHelpers'
import User from '../../models/User'

/*
 |--------------------------------------------------------------------------
 | Types
 |--------------------------------------------------------------------------
 */
export const LOGIN_REQUEST = 'otrack/auth/LOGIN_REQUEST'
export const LOGIN_SUCCESS = 'otrack/auth/LOGIN_SUCCESS'
export const LOGIN_FAILURE = 'otrack/auth/LOGIN_FAILURE'

export const LOGOUT_DONE = 'otrack/auth/LOGOUT_DONE'

/*
 |--------------------------------------------------------------------------
 | Reducer
 |--------------------------------------------------------------------------
 */
const initialState = {
  isSigningIn: false,
  isAuthenticated: false,
  user: null,
  jwt: null
}

function logoutState(state, action) {
  return {
    ...state,
    isSigningIn: false,
    isAuthenticated: false,
    user: null,
    jwt: null
  }
}

export default reduxHelpers.createReducer(initialState, {
  [LOGIN_REQUEST]: (state, action) => {
    return {
      ...state,
      isSigningIn: true
    }
  },
  [LOGIN_SUCCESS]: (state, action) => {
    return {
      ...state,
      isSigningIn: false,
      isAuthenticated: true,
      user: new User(
        action.response.profile.employeeId,
        action.response.profile.name,
        action.response.profile.phoneNumber,
        action.response.profile.role,
        action.response.profile.companyId
      ),
      jwt: action.response
    }
  },
  [LOGIN_FAILURE]: logoutState,
  [LOGOUT_DONE]: logoutState
})

/*
 |--------------------------------------------------------------------------
 | Action Creators
 |--------------------------------------------------------------------------
 */

/**
 * Launches the login popup if not logged
 */
export function requestLogin() {
  const userManager = window.UserManager
  return {
    types: [LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE],
    callAPI: () =>
      userManager.getUser().then(user => {
        if (user && !user.expired) {
          console.log('user! no popup needed', user)
          return user
        }
        console.log('no user! launching popup')

        return userManager.signinRedirect().then(user => {
          console.log('Popup done!', user)
          if (user) {
            return user
          }

          throw new Error('user not logged')
        })
      })
  }
}

/**
 * Login with the user id_token provided
 *
 * @param user
 * @returns {{type: string, response: *}}
 */
export function login(id_token) {
  return { type: LOGIN_SUCCESS, response: id_token }
}

/**
 * Launch logout popup
 */
export function requestLogout() {
  const userManager = window.UserManager

  return dispatch => {
    userManager
      .signoutRedirect()
      .then(res => {
        console.log('logout response', res)
      })
      .catch(e => {
        console.log('redirect failed', e)
      })
  }
}

/**
 * Reset user session status
 */
export function logout() {
  return {
    type: LOGOUT_DONE
  }
}
