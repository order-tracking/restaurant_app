import OIDCCallback from './OIDCCallback'
import * as React from 'react'
import { HOMEPAGE } from '../dashboard/routes'

const BASE = '/auth'

export default [
  {
    path: BASE + '/signin-oidc',
    exact: true,
    render: props => (
      <OIDCCallback
        callback={() =>
          window.UserManager
            .signinRedirectCallback()
            .then(function(user) {
              props.history.push(HOMEPAGE)
              console.log('signin response success', user)
            })
            .catch(function(err) {
              console.log(err)
            })}
      />
    )
  },
  {
    path: BASE + '/signout-oidc',
    exact: true,
    render: props => (
      <OIDCCallback
        callback={() =>
          window.UserManager
            .signoutRedirectCallback()
            .then(function(resp) {
              props.history.push('/')
            })
            .catch(function(err) {
              console.error(err)
              props.history.push('/')
            })}
      />
    )
  },
  {
    path: BASE + '/silent-renew-oidc',
    exact: true,
    render: props => (
      <OIDCCallback
        callback={() => window.UserManager.signinSilentCallback()}
      />
    )
  }
]
