import React from 'react'
import { Route, Switch } from 'react-router-dom'
import routes from './routes'
import PageNotFoundComponent from '../../components/PageNotFoundComponent'

const Auth = () => {
  return (
    <div>
      <Switch>
        {routes.map((route, i) => (
          <Route
            key={i}
            path={route.path}
            exact={route.exact}
            render={route.render}
          />
        ))}
        <Route component={PageNotFoundComponent} />}
      </Switch>
    </div>
  )
}
export default Auth
