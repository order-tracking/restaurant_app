import React from 'react'
import logo from '../../../assets/img/otrack_vertical.svg'
import { Image } from 'semantic-ui-react'
import { Helmet } from 'react-helmet'

const Homepage = props => {
  return (
    <div>
      <Helmet title="Home" />
      <Image src={logo} alt="oTrack" size="big" centered />
    </div>
  )
}

export default Homepage
