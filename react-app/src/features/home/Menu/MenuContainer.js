import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { requestLogin, requestLogout } from '../../auth/authReducer'
import MenuBar from './Menu'

class MenuContainer extends React.Component {
  render() {
    return (
      <MenuBar
        session={this.props.session}
        onLogin={this.props.actions.requestLogin}
        onLogout={this.props.actions.requestLogout}
      />
    )
  }
}

MenuContainer.propTypes = {
  session: PropTypes.object.isRequired
}

const mapStateToProps = (state, ownProps) => {
  return {
    session: state.session
  }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ requestLogin, requestLogout }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuContainer)
