import React from 'react'
import PropTypes from 'prop-types'
import { Link, NavLink } from 'react-router-dom'
import { Icon, Container, Menu, Button } from 'semantic-ui-react'

class MenuBar extends React.Component {
  render() {
    const logged = this.props.session.isAuthenticated
    const user = this.props.session.user
    return (
      <Menu color="blue" inverted fluid borderless className="no-border-radius">
        <Container>
          <Menu.Item as={NavLink} to="/" exact>
            <Icon name="home" />
            <p>Home</p>
          </Menu.Item>
          <Menu.Item as={NavLink} to="/contact" exact>
            <Icon name="call" />
            <p>Contact</p>
          </Menu.Item>

          {logged && (
            <Menu.Menu position="right">
              <Menu.Item>Hello {user.name}</Menu.Item>
              <Menu.Item as={Link} to="/dashboard/home">
                <Icon name="rocket" />
                <p>Dashboard</p>
              </Menu.Item>
              <Menu.Item
                as={Button}
                icon="sign out"
                onClick={this.props.onLogout}
                title="Logout"
              />
            </Menu.Menu>
          )}

          {!logged && (
            <Menu.Menu position="right">
              <Menu.Item
                as={Button}
                icon="sign in"
                onClick={this.props.onLogin}
                title="Login"
                content="Login"
              />
            </Menu.Menu>
          )}
        </Container>
      </Menu>
    )
  }
}

MenuBar.propTypes = {
  session: PropTypes.object.isRequired,
  onLogin: PropTypes.func.isRequired,
  onLogout: PropTypes.func.isRequired
}

export default MenuBar
