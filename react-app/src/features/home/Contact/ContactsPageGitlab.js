import React, { Component } from 'react'
import { Button, Divider, Form, Header, Segment } from 'semantic-ui-react'
import { isEmail } from '../../../helpers/ValidationRules'
import FormValidation from '../../../components/validation/FormValidation'
import ValidationField from '../../../components/validation/ValidationField'

class ContactsPageGitlab extends Component {
  constructor(props) {
    super(props)
    this.state = {
      content: ''
    }
    this.onSubmit = this.onSubmit.bind(this)
    this.onChange = this.onChange.bind(this)
  }

  onSubmit(contact) {
    console.log('EMAIL', { email: contact.email, content: this.state.content })
  }

  onChange(event, { value }) {
    this.setState({ content: value })
  }

  render() {
    const { content } = this.state
    return (
      <Segment basic>
        <Header textAlign="center">Contact Us</Header>
        <Divider />
        <FormValidation onSubmit={this.onSubmit}>
          <ValidationField
            name="email"
            label="Email"
            placeholder="johnsmith@email.com"
            type="text"
            rules={[isEmail]}
          />
          <Form.TextArea
            label="Content"
            placeholder="Explain to us your interest in our service"
            type="text"
            value={content}
            onChange={this.onChange}
          />
          <Divider hidden clearing />
          <Button type="submit" floated="right">
            Submit
          </Button>
        </FormValidation>
      </Segment>
    )
  }
}

export default ContactsPageGitlab
