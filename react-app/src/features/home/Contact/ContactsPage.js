import React, { Component } from 'react'
import logo from '../../../assets/img/otrack_vertical.svg'
import { Card, Divider, Header, Icon, Image, Segment } from 'semantic-ui-react'

class ContactsPage extends Component {
  render() {
    return (
      <Segment basic>
        <Header textAlign="center">Contact Us</Header>
        <Divider />
        <Segment basic textAlign="center">
          <Card.Group>
            <Card centered>
              <Image src={logo} />
              <Card.Content>
                <Card.Header content="Nuno Reis" />
              </Card.Content>
              <Card.Content extra>
                <a href="mailto:nuno.m.reis@netcabo.pt">
                  <Icon name="mail" />
                  nuno.m.reis@netcabo.pt
                </a>
              </Card.Content>
            </Card>
            <Card centered>
              <Image src={logo} />
              <Card.Content>
                <Card.Header content="Carlos Florêncio" />
              </Card.Content>
              <Card.Content extra>
                <a href="mailto:carlosmflorencio@gmail.com">
                  <Icon name="mail" />
                  carlosmflorencio@gmail.com
                </a>
              </Card.Content>
            </Card>
          </Card.Group>
        </Segment>
      </Segment>
    )
  }
}

export default ContactsPage
