import HomePage from './Home/HomePage'
import ContactsPage from './Contact/ContactsPage'

export default [
  {
    path: '/',
    exact: true,
    component: HomePage
  },
  {
    path: '/contact',
    exact: true,
    component: ContactsPage
  }
]
