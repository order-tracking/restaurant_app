import React from 'react'
import PageNotFoundComponent from '../../components/PageNotFoundComponent'
import { Grid, Divider } from 'semantic-ui-react'
import { Route, Switch } from 'react-router-dom'
import { Helmet } from 'react-helmet'
import routes from './routes'
import MenuContainer from './Menu/MenuContainer'
import HandleAuthComponent from '../../components/HandleAuthComponent'

const Home = () => {
  return (
    <HandleAuthComponent>
      <Helmet titleTemplate="%s - oTrack" />
      <MenuContainer />

      <Grid container>
        <Grid.Row>
          <Grid.Column>
            <Switch>
              {routes.map((route, i) => (
                <Route
                  key={i}
                  path={route.path}
                  exact={route.exact}
                  component={route.component}
                />
              ))}
              <Route component={PageNotFoundComponent} />}
            </Switch>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column textAlign="right">
            <Divider />
            © {new Date().getFullYear()} oTrack
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </HandleAuthComponent>
  )
}
export default Home
