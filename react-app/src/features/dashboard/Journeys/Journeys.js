import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link, NavLink } from 'react-router-dom'
import {
  Button,
  Divider,
  Dropdown,
  Grid,
  Header,
  Icon,
  Segment
} from 'semantic-ui-react'
import JourneysPaginatedList from './JourneysPaginatedList'
import { withRouter } from 'react-router'
import CenteredSegmentTextComponent from '../../../components/CenteredSegmentTextComponent'
import { Helmet } from 'react-helmet'
import * as Routes from '../routes'
import * as UrlHelpers from '../../../helpers/UrlHelpers'

class Journeys extends Component {
  constructor(props) {
    super(props)
    let params = new URLSearchParams(props.location.search)
    this.state = {
      isLoading: true,
      params: {
        page: parseInt(params.get('page'), 10) || 1,
        limit: parseInt(params.get('limit'), 10) || 5,
        search: params.get('search') || 'incomplete'
      },
      options: []
    }

    this.getData = this.getData.bind(this)
    this.getDropdownValue = this.getDropdownValue.bind(this)
  }

  getData() {
    const { page, limit, search } = this.state.params
    this.props.getJourneys(page, limit, search).then(_ =>
      this.setState({
        isLoading: false,
        options: Options.map(option => {
          return {
            ...option,
            as: NavLink,
            to:
              Routes.JOURNEYSPAGE +
              '?' +
              UrlHelpers.getQueryString({
                ...this.state.params,
                search: option.key
              })
          }
        })
      })
    )
  }

  getDropdownValue() {
    const { options } = this.state
    const opt = options.find(opt => opt.key === this.state.params.search)
    return opt ? opt.value : undefined
  }

  componentWillReceiveProps(nextProps) {
    let nextParams = new URLSearchParams(nextProps.location.search)
    let page = parseInt(nextParams.get('page'), 10)
    let limit = parseInt(nextParams.get('limit'), 10)
    let search = nextParams.get('search')
    if (
      (nextParams.get('page') && page !== this.state.params.page) ||
      (nextParams.get('limit') && limit !== this.state.params.limit) ||
      (nextParams.get('search') && search !== this.state.params.search)
    ) {
      const stateParams = {
        page: page || this.state.params.page,
        limit: limit || this.state.params.limit,
        search: search || this.state.params.search
      }
      this.setState({
        isLoading: true,
        params: stateParams
      })
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.params !== prevState.params) {
      this.getData()
    }
  }

  componentDidMount() {
    this.props.setBreadcrumbs([
      {
        key: 'Dashboard',
        content: 'Dashboard',
        as: Link,
        to: Routes.HOMEPAGE
      },
      { key: 'Journeys', content: 'Journeys', active: true }
    ])
    this.getData()
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextProps.journeys !== this.props.journeys ||
      nextState.isLoading !== this.state.isLoading
    )
  }

  render() {
    const { params, isLoading } = this.state
    return (
      <Segment loading={isLoading}>
        <Helmet title="Journeys" />
        <Grid stackable columns={2}>
          <Grid.Column width={6}>
            <Button
              as={NavLink}
              to={Routes.ADDJOURNEYPAGE}
              floated="right"
              content="Create a Journey"
              icon="plus"
              labelPosition="left"
              primary
            />
            <Header as="h4" floated="left">
              <Icon name="motorcycle" />
              <Header.Content>
                Journeys
                <Header.Subheader>Company Journeys</Header.Subheader>
              </Header.Content>
            </Header>
          </Grid.Column>
          <Grid.Column width={10}>
            <Dropdown
              fluid
              placeholder={'Options'}
              selection
              options={this.state.options}
              value={this.getDropdownValue()}
            />
          </Grid.Column>
        </Grid>
        <Divider clearing />
        {this.props.journeys.count > 0 && (
          <JourneysPaginatedList
            journeys={this.props.journeys}
            deleteJourney={this.props.deleteJourney}
            nextLink={
              Routes.JOURNEYSPAGE +
              '?' +
              UrlHelpers.getQueryString({
                ...params,
                page: params.page + 1
              })
            }
            previousLink={
              Routes.JOURNEYSPAGE +
              '?' +
              UrlHelpers.getQueryString({
                ...params,
                page: params.page - 1
              })
            }
          />
        )}
        {this.props.journeys.count <= 0 && (
          <CenteredSegmentTextComponent text="No Journeys" />
        )}
      </Segment>
    )
  }
}

const Options = [
  {
    key: 'all',
    text: 'All',
    value: 'all'
  },
  {
    key: 'complete',
    text: 'Complete',
    value: 'complete'
  },
  {
    key: 'incomplete',
    text: 'Incomplete',
    value: 'incomplete'
  }
]

Journeys.propTypes = {
  journeys: PropTypes.object.isRequired,
  setBreadcrumbs: PropTypes.func.isRequired,
  getJourneys: PropTypes.func.isRequired,
  deleteJourney: PropTypes.func.isRequired
}

export default withRouter(Journeys)
