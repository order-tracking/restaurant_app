import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { changeBreadcrumbs } from '../Layout/layoutReducer'
import Journeys from './Journeys'
import { deleteJourney, getJourneys } from './journeysReducer'

class JourneysPage extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <Journeys
        journeys={this.props.journeys}
        setBreadcrumbs={this.props.actions.changeBreadcrumbs}
        getJourneys={this.props.actions.getJourneys}
        deleteJourney={this.props.actions.deleteJourney}
      />
    )
  }
}

JourneysPage.propTypes = {
  journeys: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
}

const mapStateToProps = (state, ownProps) => {
  return {
    journeys: state.journeys
  }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      { changeBreadcrumbs, getJourneys, deleteJourney },
      dispatch
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(JourneysPage)
