import React, { Component } from 'react'
import CreateJourney from './CreateJourney'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { changeBreadcrumbs } from '../../Layout/layoutReducer'
import { createJourney } from '../journeysReducer'
import { actions as employeeActions } from '../../Employees/employeesReducer'
import { getReadyOrders } from '../../Orders/ordersReducer'

class CreateJourneyPage extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <CreateJourney
        employees={this.props.employees}
        orders={this.props.orders}
        setBreadcrumbs={this.props.actions.changeBreadcrumbs}
        createJourney={this.props.actions.createJourney}
        getEmployees={this.props.actions.getEmployees}
        getReadyOrders={this.props.actions.getReadyOrders}
      />
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    employees: state.employees,
    orders: state.orders
  }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      {
        changeBreadcrumbs,
        createJourney,
        getEmployees: employeeActions.getEmployees,
        getReadyOrders
      },
      dispatch
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateJourneyPage)
