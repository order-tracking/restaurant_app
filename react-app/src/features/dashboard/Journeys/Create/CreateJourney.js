import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Button,
  Checkbox,
  Divider,
  Dropdown,
  Grid,
  Header,
  Icon,
  List,
  Segment
} from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import { getKms } from '../../../../helpers/OrderHelpers'
import { toastr } from 'react-redux-toastr'
import { withRouter } from 'react-router'
import CenteredSegmentTextComponent from '../../../../components/CenteredSegmentTextComponent'
import * as Routes from '../../routes'
import { Helmet } from 'react-helmet'
import { getTimeFromNowText } from '../../../../helpers/DateHelpers'

class CreateJourney extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      orders: [],
      journey: {
        employeeId: undefined,
        orders: []
      }
    }

    this.changeOrder = this.changeOrder.bind(this)
    this.changeEmployee = this.changeEmployee.bind(this)
    this.createJourney = this.createJourney.bind(this)
  }

  componentDidMount() {
    this.props.setBreadcrumbs([
      {
        key: 'Dashboard',
        content: 'Dashboard',
        as: Link,
        to: Routes.HOMEPAGE
      },
      {
        key: 'Journeys',
        content: 'Journeys',
        as: Link,
        to: Routes.JOURNEYSPAGE
      },
      { key: 'Create', content: 'Create', active: true }
    ])

    Promise.all([
      this.props.getEmployees(),
      this.props.getReadyOrders().then(_ => {
        this.setState({
          orders: this.props.orders.items.filter(
            order => order.readyToDistribute
          )
        })
      })
    ]).then(_ =>
      this.setState({
        isLoading: false
      })
    )
  }

  createJourney() {
    this.props
      .createJourney(this.state.journey)
      .then(_ => {
        toastr.success('Journey successfully created')
        this.props.history.goBack()
      })
      .catch(error => toastr.error(error))
  }

  // Value equals order id
  changeOrder(event, { value, checked }) {
    const { journey } = this.state
    const order = this.props.orders.items.find(order => order.id === value)
    this.setState({
      journey: {
        ...journey,
        orders: checked
          ? [...journey.orders, order]
          : journey.orders.filter(order => order.id !== value)
      }
    })
  }

  // Value equals employee id
  changeEmployee(event, { value }) {
    this.setState({
      journey: {
        ...this.state.journey,
        employeeId: value
      }
    })
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextState.journey !== this.state.journey ||
      nextState.isLoading !== this.state.isLoading
    )
  }

  render() {
    const { employees } = this.props
    const { isLoading, journey, orders } = this.state
    return (
      <Segment loading={isLoading}>
        <Helmet title="Create Journey" />
        <Header as="h4">
          <Icon name="add circle" />
          <Header.Content>Create Journey</Header.Content>
        </Header>
        {!isLoading && (
          <div>
            <Grid stackable columns={2}>
              <Grid.Column width={3} stretched>
                <Header as="h4">
                  <Icon name="motorcycle" />
                  <Header.Content>
                    Employee to Deliver
                    <Header.Subheader>Choose your employee</Header.Subheader>
                  </Header.Content>
                </Header>
              </Grid.Column>
              <Grid.Column width={13}>
                <Dropdown
                  placeholder="Select Employee"
                  fluid
                  selection
                  options={MapEmployeeToOptions(employees)}
                  onChange={this.changeEmployee}
                />
              </Grid.Column>
            </Grid>
            <Segment>
              <Header>Ready to Distribute Orders</Header>
              <Divider />
              {orders.length <= 0 && (
                <CenteredSegmentTextComponent text="No Orders ready to Distribute" />
              )}
              {orders.length > 0 && (
                <List>
                  {orders.map(order => {
                    return (
                      <List.Item key={order.id}>
                        <List.Content floated="left">
                          <Checkbox
                            value={order.id}
                            onChange={this.changeOrder}
                          />
                        </List.Content>
                        <List.Content>
                          <List.Header>
                            {order.address}, {order.address2}
                          </List.Header>
                          <List.Description>
                            Created{' '}
                            <strong>
                              {getTimeFromNowText(order.createdAt)}
                            </strong>{' '}
                            &nbsp;
                            <strong>Distance:</strong> {getKms(order.distance)}{' '}
                            km
                          </List.Description>
                        </List.Content>
                      </List.Item>
                    )
                  })}
                </List>
              )}
              {journey.employeeId &&
                journey.orders.length > 0 && (
                  <Button onClick={this.createJourney}>Create</Button>
                )}
            </Segment>
          </div>
        )}
      </Segment>
    )
  }
}

function MapEmployeeToOptions(employees) {
  let toReturn = []
  employees.items.forEach(employee => {
    toReturn.push({
      key: employee.id,
      text: employee.name + ' (' + employee.phoneNumber + ')',
      value: employee.id
    })
  })
  return toReturn
}

CreateJourney.propTypes = {
  employees: PropTypes.object.isRequired,
  orders: PropTypes.object.isRequired,
  setBreadcrumbs: PropTypes.func.isRequired,
  createJourney: PropTypes.func.isRequired,
  getEmployees: PropTypes.func.isRequired,
  getReadyOrders: PropTypes.func.isRequired
}

export default withRouter(CreateJourney)
