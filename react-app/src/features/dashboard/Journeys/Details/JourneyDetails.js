import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Divider, Header, Icon, Segment } from 'semantic-ui-react'
import { withRouter } from 'react-router'
import * as Routes from '../../routes'
import { Link } from 'react-router-dom'
import ConfirmDeleteComponent from '../../../../components/ConfirmDeleteComponent'
import { toastr } from 'react-redux-toastr'
import JourneySubMenu from './JourneySubMenu'

class JourneyDetails extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      journey: {
        employee: {},
        orders: []
      }
    }

    this.deleteJourney = this.deleteJourney.bind(this)
  }

  componentDidMount() {
    this.props.setBreadcrumbs([
      { key: 'Dashboard', content: 'Dashboard', as: Link, to: Routes.HOMEPAGE },
      {
        key: 'Journeys',
        content: 'Journeys',
        as: Link,
        to: Routes.JOURNEYSPAGE
      },
      { key: 'Details', content: 'Details', active: true }
    ])

    this.props
      .getJourney(this.props.match.params.id)
      .then(journey => this.setState({ journey: journey, isLoading: false }))
  }

  deleteJourney() {
    this.props
      .deleteJourney(this.props.match.params.id)
      .then(_ => {
        this.props.history.goBack()
        toastr.success('Journey successfully deleted', '', {
          showCloseButton: false
        })
      })
      .catch(error => toastr.error(error))
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextState.journey !== this.state.journey ||
      nextState.isLoading !== this.state.isLoading
    )
  }

  render() {
    const { isLoading, journey } = this.state
    return (
      <Segment loading={isLoading}>
        <Segment basic>
          <Header as="h4" floated="left">
            <Icon name="motorcycle" />
            <Header.Content>
              Journey Details
              <Header.Subheader>Company Journey</Header.Subheader>
            </Header.Content>
          </Header>
          <ConfirmDeleteComponent
            delete={this.deleteJourney}
            floated="right"
            color="red"
          />
          <Divider clearing />
          <Segment clearing>
            <Header floated="left">
              <Icon name="user" />
              <Header.Content>
                Distributor
                <Header.Subheader>{journey.employee.name}</Header.Subheader>
              </Header.Content>
            </Header>
            <Header floated="right">
              <Icon name="map" />
              <Header.Content>
                Journey Status
                <Header.Subheader>
                  {journey.completed
                    ? 'Complete'
                    : journey.onGoing ? 'On Going' : 'Waiting'}
                </Header.Subheader>
              </Header.Content>
            </Header>
          </Segment>
          {!isLoading && <JourneySubMenu journey={journey} />}
        </Segment>
      </Segment>
    )
  }
}

JourneyDetails.propTypes = {
  setBreadcrumbs: PropTypes.func.isRequired,
  getJourney: PropTypes.func.isRequired,
  deleteJourney: PropTypes.func.isRequired
}

export default withRouter(JourneyDetails)
