import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Divider, Header, Segment } from 'semantic-ui-react'
import CenteredSegmentTextComponent from '../../../../components/CenteredSegmentTextComponent'
import HyperTrackComponent from './HyperTrackComponent'

class OrderSubMenuComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const { order } = this.props
    return (
      <Segment basic>
        <Header as="h4" floated="left">
          <Header.Content>
            Address: {order.address}, {order.address2}
          </Header.Content>
        </Header>
        <Header as="h4" floated="right">
          <Header.Content>Order made by: {order.customer.name}</Header.Content>
        </Header>
        <Divider clearing />
        {!order.onGoing &&
          !order.deliveredAt && (
            <CenteredSegmentTextComponent text="Order has not started yet" />
          )}
        {(order.onGoing || order.deliveredAt) && (
          <HyperTrackComponent order={order} />
        )}
      </Segment>
    )
  }
}

OrderSubMenuComponent.propTypes = {
  order: PropTypes.object.isRequired
}

export default OrderSubMenuComponent
