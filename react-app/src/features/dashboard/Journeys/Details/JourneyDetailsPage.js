import React, { Component } from 'react'
import PropTypes from 'prop-types'
import JourneyDetails from './JourneyDetails'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { changeBreadcrumbs } from '../../Layout/layoutReducer'
import { deleteJourney, getJourney } from '../journeysReducer'

class JourneyDetailsPage extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <JourneyDetails
        setBreadcrumbs={this.props.actions.changeBreadcrumbs}
        getJourney={this.props.actions.getJourney}
        deleteJourney={this.props.actions.deleteJourney}
      />
    )
  }
}

JourneyDetailsPage.propTypes = {
  journeys: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
}

const mapStateToProps = (state, ownProps) => {
  return {
    journeys: state.journeys
  }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      { changeBreadcrumbs, getJourney, deleteJourney },
      dispatch
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(JourneyDetailsPage)
