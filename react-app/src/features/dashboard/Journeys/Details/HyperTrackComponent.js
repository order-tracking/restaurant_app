import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Iframe from 'react-iframe'

// Publishable Key
const HYPERTRACK_KEY = process.env.REACT_APP_HYPERTRACK_KEY

class HyperTrackComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const { order, height } = this.props
    return (
      <div style={{ height }}>
        <Iframe
          url={
            'https://dashboard.hypertrack.com/widget/actions;lookup_id=' +
            order.id +
            '?key=' +
            HYPERTRACK_KEY
          }
          width="100%"
          height="100%"
          position="relative"
        />
      </div>
    )
  }
}

HyperTrackComponent.defaultProps = {
  height: '400px'
}

HyperTrackComponent.propTypes = {
  order: PropTypes.object.isRequired,
  height: PropTypes.string.isRequired
}

export default HyperTrackComponent
