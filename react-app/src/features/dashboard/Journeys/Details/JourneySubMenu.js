import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Menu, Segment } from 'semantic-ui-react'
import OrderSubMenuComponent from './OrderSubMenuComponent'

class JourneySubMenu extends Component {
  constructor(props) {
    super(props)
    this.state = {
      activeOrder: undefined
    }

    this.changeActiveOrder = this.changeActiveOrder.bind(this)
  }

  componentDidMount() {
    const { journey } = this.props
    let activeOrder =
      journey.orders.find(order => order.onGoing) || journey.orders[0]
    this.setState({ activeOrder })
  }

  // Value equal to order id
  changeActiveOrder(event, { value }) {
    this.setState({
      activeOrder: this.props.journey.orders.find(order => order.id === value)
    })
  }

  render() {
    const { activeOrder } = this.state
    const { journey } = this.props
    return (
      <div>
        <Menu attached="top" tabular>
          {activeOrder &&
            journey.orders.map(order => {
              return (
                <Menu.Item
                  key={order.id}
                  active={order.id === activeOrder.id}
                  onClick={this.changeActiveOrder}
                  value={order.id}
                >
                  Order #{order.id}{' '}
                  {order.onGoing
                    ? ' (On Going)'
                    : order.deliveredAt ? ' (Delivered)' : ''}
                </Menu.Item>
              )
            })}
        </Menu>
        <Segment attached="bottom">
          {activeOrder && <OrderSubMenuComponent order={activeOrder} />}
        </Segment>
      </div>
    )
  }
}

JourneySubMenu.propTypes = {
  journey: PropTypes.object.isRequired
}

export default JourneySubMenu
