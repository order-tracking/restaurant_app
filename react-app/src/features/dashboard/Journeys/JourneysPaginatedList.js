import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { List, Segment } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'
import * as Routes from '../routes'
import PaginatedComponent from '../../../components/PaginatedComponent'
import * as DateHelpers from '../../../helpers/DateHelpers'

class JourneysPaginatedList extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const { journeys } = this.props
    return (
      <Segment clearing basic>
        <PaginatedComponent
          hasNextPage={journeys.hasNextPage}
          nextLink={this.props.nextLink}
          hasPreviousPage={journeys.hasPreviousPage}
          previousLink={this.props.previousLink}
        >
          {journeys.items.map(journey => (
            <List.Item
              key={journey.id}
              className="pt10 pb10"
              as={NavLink}
              to={Routes.JOURNEYDETAILSPAGE.replace(':id', journey.id)}
            >
              <List.Icon
                size="large"
                verticalAlign="middle"
                name={journey.completed ? 'check' : 'wait'}
              />
              <List.Content>
                <List.Header>
                  ID: #{journey.id} - Created:{' '}
                  {DateHelpers.dateNormalWithHours(journey.createdAt)}
                </List.Header>
                <List.Description>
                  Designated Employee: {journey.employee.name} ({journey.employee.phoneNumber})
                </List.Description>
              </List.Content>
            </List.Item>
          ))}
        </PaginatedComponent>
      </Segment>
    )
  }
}

JourneysPaginatedList.propTypes = {
  journeys: PropTypes.object.isRequired,
  deleteJourney: PropTypes.func.isRequired,
  nextLink: PropTypes.string.isRequired,
  previousLink: PropTypes.string.isRequired
}

export default JourneysPaginatedList
