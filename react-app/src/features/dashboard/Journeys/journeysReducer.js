import * as helpers from '../../../helpers/ReduxHelpers'
import JourneysRepository from '../../../repositories/JourneysRepository'

/*
 |--------------------------------------------------------------------------
 | Types
 |--------------------------------------------------------------------------
 */

export const GET_JOURNEYS_SUCCESS =
  'otrack/dashboard/journeys/GET_JOURNEYS_SUCCESS'

export const GET_JOURNEY_SUCCESS =
  'otrack/dashboard/journeys/GET_JOURNEY_SUCCESS'

export const CREATE_JOURNEY_SUCCESS =
  'otrack/dashboard/journeys/CREATE_JOURNEY_SUCCESS'

export const DELETE_JOURNEY_SUCCESS =
  'otrack/dashboard/journeys/DELETE_JOURNEY_SUCCESS'

/*
 |--------------------------------------------------------------------------
 | Reducer
 |--------------------------------------------------------------------------
 */
const initialState = {
  count: -1,
  limit: -1,
  page: -1,
  hasNextPage: false,
  hasPreviousPage: false,
  items: []
}

export default helpers.createReducer(initialState, {
  [GET_JOURNEYS_SUCCESS]: (state, action) => {
    return action.journeys
  },
  [GET_JOURNEY_SUCCESS]: (state, action) => {
    return {
      ...state,
      items: state.items.map(journey => {
        if (journey.id === action.journey.id) {
          return action.journey
        }
        return journey
      })
    }
  },
  [DELETE_JOURNEY_SUCCESS]: (state, action) => {
    return {
      ...state,
      items: state.items.filter(journey => journey.id !== action.id)
    }
  },
  [CREATE_JOURNEY_SUCCESS]: (state, action) => {
    return {
      ...state,
      items: [...state.items, action.journey]
    }
  }
})

/*
 |--------------------------------------------------------------------------
 | Action Creators
 |--------------------------------------------------------------------------
 */
export function loadJourneysSuccess(journeys) {
  return { type: GET_JOURNEYS_SUCCESS, journeys }
}

export function loadJourneySuccess(journey) {
  return { type: GET_JOURNEY_SUCCESS, journey }
}

export function createJourneySuccess(journey) {
  return { type: CREATE_JOURNEY_SUCCESS, journey }
}

export function deleteJourneySuccess(id) {
  return { type: DELETE_JOURNEY_SUCCESS, id }
}

export function getJourneys(page, limit, search) {
  return dispatch => {
    return JourneysRepository.getJourneys(
      page,
      limit,
      search
    ).then(journeys => {
      console.log(journeys)
      dispatch(loadJourneysSuccess(journeys))
      return journeys
    })
  }
}

export function getJourney(id) {
  return dispatch => {
    return JourneysRepository.getJourney(id).then(journey => {
      console.log(journey)
      dispatch(loadJourneySuccess(journey))
      return journey
    })
  }
}

export function createJourney(journey) {
  return dispatch => {
    return JourneysRepository.createJourney(journey).then(journey => {
      console.log(journey)
      dispatch(createJourneySuccess(journey))
      return journey
    })
  }
}

export function deleteJourney(id) {
  return dispatch => {
    return JourneysRepository.deleteJourney(id).then(_ => {
      dispatch(deleteJourneySuccess(id))
    })
  }
}
