import React, { Component } from 'react'
import PropTypes from 'prop-types'
import * as Routes from '../../routes'
import { Link } from 'react-router-dom'
import { Button, Divider, Header, Icon, Segment } from 'semantic-ui-react'
import FormValidation from '../../../../components/validation/FormValidation'
import ValidationField from '../../../../components/validation/ValidationField'
import { max, min } from '../../../../helpers/ValidationRules'
import { toastr } from 'react-redux-toastr'
import { withRouter } from 'react-router'
import CampaignsRepository from '../../../../repositories/CampaignsRepository'

class AddCampaign extends Component {
  constructor(props) {
    super(props)
    this.createCampaign = this.createCampaign.bind(this)
  }

  componentDidMount() {
    this.props.setBreadcrumbs([
      { key: 'Dashboard', content: 'Dashboard', as: Link, to: Routes.HOMEPAGE },
      {
        key: 'Campaigns',
        content: 'Campaigns',
        as: Link,
        to: Routes.CAMPAIGNSPAGE
      },
      { key: 'Add', content: 'Add', active: true }
    ])
  }

  createCampaign(campaign) {
    CampaignsRepository.create(campaign)
      .then(campaign => {
        console.log(campaign)
        toastr.success('Campaign successfully created', '', {
          showCloseButton: false
        })
        this.props.history.goBack()
      })
      .catch(error => toastr.error(error))
  }

  render() {
    return (
      <Segment>
        <Header as="h4" floated="left">
          <Icon name="announcement" />
          <Header.Content>
            Add Campaign
            <Header.Subheader>Company Campaigns</Header.Subheader>
          </Header.Content>
        </Header>
        <br />
        <Divider clearing />
        <FormValidation onSubmit={this.createCampaign}>
          <ValidationField
            name="title"
            label="Title"
            placeholder="Title to be easy to know which message it is"
            type="text"
            rules={[min(3)]}
          />
          <ValidationField
            name="message"
            label="Message"
            placeholder="Message to be sent to all the customers"
            type="text"
            rules={[max(1500)]}
          />
          <Button type="submit">Create</Button>
        </FormValidation>
      </Segment>
    )
  }
}

AddCampaign.propTypes = {
  setBreadcrumbs: PropTypes.func.isRequired
}

export default withRouter(AddCampaign)
