import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { changeBreadcrumbs } from '../../Layout/layoutReducer'
import AddCampaign from './AddCampaign'

class AddCampaignPage extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return <AddCampaign setBreadcrumbs={this.props.actions.changeBreadcrumbs} />
  }
}

function mapStateToProps(state, ownProps) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ changeBreadcrumbs }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddCampaignPage)
