import React, { Component } from 'react'
import PropTypes from 'prop-types'
import CenteredSegmentTextComponent from '../../../components/CenteredSegmentTextComponent'
import { Button, Divider, Header, Icon, Segment } from 'semantic-ui-react'
import { Helmet } from 'react-helmet'
import PaginatedComponent from '../../../components/PaginatedComponent'
import { List } from 'semantic-ui-react'
import * as Routes from '../routes'
import { Link, NavLink } from 'react-router-dom'
import CampaignsRepository from '../../../repositories/CampaignsRepository'
import { dateNormal } from '../../../helpers/DateHelpers'

class Campaigns extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      campaigns: {
        count: 0,
        limit: 0,
        page: 1,
        hasNextPage: false,
        hasPreviousPage: false,
        items: []
      }
    }
  }

  componentDidMount() {
    this.props.setBreadcrumbs([
      {
        key: 'Dashboard',
        content: 'Dashboard',
        as: Link,
        to: Routes.HOMEPAGE
      },
      { key: 'Campaigns', content: 'Campaigns', active: true }
    ])

    CampaignsRepository.getCampaigns().then(campaigns => {
      console.log(campaigns)
      this.setState({ isLoading: false, campaigns })
    })
  }

  render() {
    const { isLoading, campaigns } = this.state
    return (
      <Segment loading={isLoading}>
        <Helmet title="Campaigns" />
        <Segment basic>
          <Header as="h4" floated="left">
            <Icon name="announcement" />
            <Header.Content>
              Campaigns
              <Header.Subheader>Company Campaigns</Header.Subheader>
            </Header.Content>
          </Header>
          <Button
            as={NavLink}
            to={Routes.ADDCAMPAIGNSPAGE}
            floated="right"
            content="Create a new Campaign"
            icon="plus"
            labelPosition="left"
            primary
          />
        </Segment>
        <Divider clearing />
        <Segment basic>
          {campaigns.count > 0 && (
            <PaginatedComponent
              hasNextPage={campaigns.hasNextPage}
              nextLink=""
              hasPreviousPage={campaigns.hasPreviousPage}
              previousLink=""
            >
              {campaigns.items.map(item => {
                return (
                  <List.Item key={item.id} className="pt10 pb10">
                    <List.Icon
                      size="large"
                      verticalAlign="middle"
                      name="announcement"
                    />
                    <List.Content>
                      <List.Header>
                        {item.title} ({item.message})
                      </List.Header>
                      <List.Description>
                        <strong>Created at: </strong>{' '}
                        {dateNormal(item.createdAt)},{' '}
                        <strong>Total SMS Sent: </strong> {item.totalSmsSent}
                      </List.Description>
                    </List.Content>
                  </List.Item>
                )
              })}
            </PaginatedComponent>
          )}
          {campaigns.count <= 0 && (
            <CenteredSegmentTextComponent text="No Campaigns yet" />
          )}
        </Segment>
      </Segment>
    )
  }
}

Campaigns.propTypes = {
  setBreadcrumbs: PropTypes.func.isRequired
}

export default Campaigns
