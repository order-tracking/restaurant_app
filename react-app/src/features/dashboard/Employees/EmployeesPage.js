import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Employees from './Employees'
import { actions as employeesActions } from './employeesReducer'
import { changeBreadcrumbs } from '../Layout/layoutReducer'

class EmployeesPage extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <Employees
        employees={this.props.employees}
        actions={this.props.actions}
      />
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {
    employees: state.employees
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      Object.assign({}, { changeBreadcrumbs }, employeesActions),
      dispatch
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EmployeesPage)
