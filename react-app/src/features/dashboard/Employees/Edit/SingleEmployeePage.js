import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import SingleEmployee from './SingleEmployee'
import { actions as employeesActions } from '../employeesReducer'
import { changeBreadcrumbs } from '../../Layout/layoutReducer'
import { getAllowedRoles } from '../../../../models/Roles'

class SingleEmployeePage extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <SingleEmployee
        userSession={this.props.user}
        roles={getAllowedRoles(this.props.user.role)}
        changeBreadcrumbs={this.props.actions.changeBreadcrumbs}
        getEmployee={this.props.actions.getEmployee}
        editEmployee={this.props.actions.editEmployee}
        deleteEmployee={this.props.actions.deleteEmployee}
      />
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {
    user: state.session.user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      Object.assign({}, { changeBreadcrumbs }, employeesActions),
      dispatch
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SingleEmployeePage)
