import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Divider, Form, Grid, Segment } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import { toastr } from 'react-redux-toastr'
import { withRouter } from 'react-router'
import * as Routes from '../../routes'
import ConfirmDeleteComponent from '../../../../components/ConfirmDeleteComponent'
import CenteredSegmentTextComponent from '../../../../components/CenteredSegmentTextComponent'

class SingleEmployee extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      confirmOpen: false,
      employee: {
        id: undefined,
        firstName: undefined,
        lastName: undefined,
        roleId: undefined
      }
    }

    this.updateEmployeeState = this.updateEmployeeState.bind(this)
    this.editEmployee = this.editEmployee.bind(this)
    this.deleteEmployee = this.deleteEmployee.bind(this)
    this.closeConfirm = () => this.setState({ confirmOpen: false })
    this.openConfirm = () => this.setState({ confirmOpen: true })
  }

  componentDidMount() {
    this.props.changeBreadcrumbs([
      {
        key: 'Dashboard',
        content: 'Dashboard',
        as: Link,
        to: Routes.HOMEPAGE
      },
      {
        key: 'Employees',
        content: 'Employees',
        as: Link,
        to: Routes.EMPLOYEESPAGE
      },
      { key: 'Edit', content: 'Edit Employee', active: true }
    ])

    this.props.getEmployee(this.props.match.params.id).then(employee =>
      this.setState({
        isLoading: false,
        employee
      })
    )
  }

  updateEmployeeState(event, { name, value }) {
    this.setState({
      employee: {
        ...this.state.employee,
        [name]: value
      }
    })
  }

  editEmployee() {
    const { employee } = this.state
    this.props
      .editEmployee(employee)
      .then(_ =>
        toastr.success('Employee Successfully Edited', '', {
          showCloseButton: false
        })
      )
      .catch(error => toastr.error(error))
  }

  deleteEmployee() {
    this.props
      .deleteEmployee(this.state.employee.id)
      .then(_ => {
        this.props.history.goBack()
        toastr.success('Employee Successfully Deleted', '', {
          showCloseButton: false
        })
      })
      .catch(error =>
        toastr.error('Unable to Delete Employee. Please try again')
      )
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextState.employee !== this.state.employee ||
      nextState.isLoading !== this.state.isLoading
    )
  }

  render() {
    const { roles, userSession } = this.props
    const { employee, isLoading } = this.state
    const canEditEmployee = this.props.userSession.hasRole(employee.role)
    return (
      <Segment loading={isLoading}>
        {canEditEmployee && (
          <Grid>
            <Grid.Row>
              <Grid.Column width={16}>
                <Form>
                  <Form.Input
                    label="Name"
                    placeholder="Name"
                    name="name"
                    value={employee.name}
                    onChange={this.updateEmployeeState}
                  />
                  <Form.Input
                    label="Phone Number"
                    placeholder="Phone NUmber"
                    readOnly
                    name="phoneNumber"
                    value={employee.phoneNumber}
                  />
                  {userSession.id !== this.props.match.params.id &&
                    userSession.canEditRole(employee) && (
                      <Form.Select
                        label="Role"
                        options={MapToOptions(roles)}
                        placeholder="Role"
                        name="role"
                        value={employee.role}
                        onChange={this.updateEmployeeState}
                      />
                    )}
                </Form>
                <Divider hidden />
                <Button onClick={this.editEmployee} floated="left">
                  Submit
                </Button>
                <ConfirmDeleteComponent
                  delete={this.deleteEmployee}
                  floated="right"
                  color="red"
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        )}
        {!canEditEmployee && (
          <CenteredSegmentTextComponent text="Cannot edit this employee" />
        )}
      </Segment>
    )
  }
}

function MapToOptions(roles) {
  let toReturn = []
  roles.forEach(role => {
    toReturn.push({
      key: role,
      text: role,
      value: role
    })
  })
  return toReturn
}

SingleEmployee.propTypes = {
  userSession: PropTypes.object.isRequired,
  roles: PropTypes.array.isRequired,
  changeBreadcrumbs: PropTypes.func.isRequired,
  getEmployee: PropTypes.func.isRequired,
  editEmployee: PropTypes.func.isRequired,
  deleteEmployee: PropTypes.func.isRequired
}

export default withRouter(SingleEmployee)
