import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { List, Segment } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'
import * as Routes from '../routes'
import PaginatedComponent from '../../../components/PaginatedComponent'

class EmployeesPaginatedList extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const { employees } = this.props
    return (
      <Segment basic vertical clearing>
        <PaginatedComponent
          hasNextPage={employees.hasNextPage}
          nextLink={this.props.nextLink}
          hasPreviousPage={employees.hasPreviousPage}
          previousLink={this.props.previousLink}
        >
          {employees.items.map(employee => (
            <List.Item
              key={employee.id}
              className="pt10 pb10"
              as={NavLink}
              to={Routes.EMPLOYEEPAGE.replace(':id', employee.id)}
            >
              <List.Icon
                name="user circle"
                size="large"
                verticalAlign="middle"
              />
              <List.Content>
                <List.Header>
                  {employee.name} ({employee.phoneNumber})
                </List.Header>
                <List.Description>Role: {employee.role}</List.Description>
              </List.Content>
            </List.Item>
          ))}
        </PaginatedComponent>
      </Segment>
    )
  }
}

EmployeesPaginatedList.propTypes = {
  employees: PropTypes.object.isRequired,
  deleteEmployee: PropTypes.func.isRequired,
  nextLink: PropTypes.string.isRequired,
  previousLink: PropTypes.string.isRequired
}

export default EmployeesPaginatedList
