import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Divider, Header, Icon, Segment } from 'semantic-ui-react'
import { Link, NavLink } from 'react-router-dom'
import EmployeesPaginatedList from './EmployeesPaginatedList'
import { withRouter } from 'react-router'
import { Helmet } from 'react-helmet'
import CenteredSegmentTextComponent from '../../../components/CenteredSegmentTextComponent'
import * as Routes from '../routes'
import * as UrlHelpers from '../../../helpers/UrlHelpers'

class Employees extends Component {
  constructor(props) {
    super(props)
    let params = new URLSearchParams(props.location.search)
    this.state = {
      isLoading: true,
      params: {
        page: parseInt(params.get('page'), 10) || 1,
        limit: parseInt(params.get('limit'), 10) || 5
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    let nextParams = new URLSearchParams(nextProps.location.search)
    let page = parseInt(nextParams.get('page'), 10)
    let limit = parseInt(nextParams.get('limit'), 10)
    if (
      (nextParams.get('page') && page !== this.state.params.page) ||
      (nextParams.get('limit') && limit !== this.state.params.limit)
    ) {
      this.setState({
        isLoading: true,
        params: {
          page: page || this.state.params.page,
          limit: limit || this.state.params.limit
        }
      })
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { page, limit } = this.state.params
    if (this.state.params !== prevState.params) {
      this.props.actions
        .getEmployees(page, limit)
        .then(_ => this.setState({ isLoading: false }))
    }
  }

  componentDidMount() {
    const { page, limit } = this.state.params
    this.props.actions.changeBreadcrumbs([
      {
        key: 'Dashboard',
        content: 'Dashboard',
        as: Link,
        to: '/dashboard/home'
      },
      { key: 'Employees', content: 'Employees', active: true }
    ])

    this.props.actions
      .getEmployees(page, limit)
      .then(_ => this.setState({ isLoading: false }))
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextProps.employees !== this.props.employees ||
      nextState.isLoading !== this.state.isLoading
    )
  }

  render() {
    const { isLoading, params } = this.state
    return (
      <Segment loading={isLoading}>
        <Helmet title="Employees" />
        <Segment basic>
          <Header as="h4" floated="left">
            <Icon name="users" />
            <Header.Content>
              Employees
              <Header.Subheader>Company Employees</Header.Subheader>
            </Header.Content>
          </Header>
          <Button
            as={NavLink}
            to={Routes.VIEWEMPLOYEESPAGE}
            floated="right"
            content="View Online Distributors"
            icon="motorcycle"
            labelPosition="right"
          />
          <Button
            as={NavLink}
            to={Routes.ADDEMPLOYEEPAGE}
            floated="right"
            content="Create a new Employee"
            icon="plus"
            labelPosition="left"
            primary
          />
        </Segment>
        <Divider clearing />
        {this.props.employees.count > 0 && (
          <EmployeesPaginatedList
            employees={this.props.employees}
            nextLink={
              Routes.EMPLOYEESPAGE +
              '?' +
              UrlHelpers.getQueryString({
                ...params,
                page: params.page + 1
              })
            }
            previousLink={
              Routes.EMPLOYEESPAGE +
              '?' +
              UrlHelpers.getQueryString({
                ...params,
                page: params.page - 1
              })
            }
            deleteEmployee={this.props.actions.deleteEmployee}
          />
        )}
        {this.props.employees.count <= 0 && (
          <CenteredSegmentTextComponent text="No Employees yet" />
        )}
      </Segment>
    )
  }
}

Employees.propTypes = {
  employees: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
}

export default withRouter(Employees)
