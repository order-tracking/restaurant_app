import React, { Component } from 'react'
import ViewEmployees from './ViewEmployees'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { changeBreadcrumbs } from '../../Layout/layoutReducer'

class ViewEmployeesPage extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <ViewEmployees setBreadcrumbs={this.props.actions.changeBreadcrumbs} />
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      Object.assign({}, { changeBreadcrumbs }),
      dispatch
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewEmployeesPage)
