import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Divider, Header, Icon, Segment } from 'semantic-ui-react'
import { Helmet } from 'react-helmet'
import Iframe from 'react-iframe'
import HypertrackRepository from '../../../../repositories/HypertrackRepository'

class ViewEmployees extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      hypertrack_key: undefined
    }
  }

  componentDidMount() {
    this.props.setBreadcrumbs([
      {
        key: 'Dashboard',
        content: 'Dashboard',
        as: Link,
        to: '/dashboard/home'
      },
      {
        key: 'Employees',
        content: 'Employees',
        as: Link,
        to: '/dashboard/employees'
      },
      { key: 'View', content: 'View', active: true }
    ])

    HypertrackRepository.getSecretKey().then(keys =>
      this.setState({ isLoading: false, hypertrack_key: keys.sk })
    )
  }

  render() {
    const { isLoading, hypertrack_key } = this.state
    return (
      <Segment loading={isLoading}>
        <Helmet title="View Employees" />
        <Segment basic>
          <Header as="h4" floated="left">
            <Icon name="users" />
            <Header.Content>
              Online Distributors
              <Header.Subheader>
                Company Online Distributors on Map
              </Header.Subheader>
            </Header.Content>
          </Header>
        </Segment>
        <br />
        <Divider clearing />
        <Segment basic>
          <div style={{ height: '600px' }}>
            {!isLoading && (
              <Iframe
                url={
                  'https://dashboard.hypertrack.com/widget/map/users?key=' +
                  hypertrack_key
                }
                width="100%"
                height="100%"
                position="relative"
              />
            )}
          </div>
        </Segment>
      </Segment>
    )
  }
}

export default ViewEmployees
