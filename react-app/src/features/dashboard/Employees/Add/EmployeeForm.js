import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Form, Header, Icon, Segment } from 'semantic-ui-react'

class EmployeeForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      employee: {}
    }

    this.onSubmit = this.onSubmit.bind(this)
    this.updateEmployeeState = this.updateEmployeeState.bind(this)
  }

  updateEmployeeState(event, { name, value }) {
    let employee = this.state.employee
    employee[name] = value
    this.setState({ employee })
  }

  onSubmit() {
    this.props.createEmployee(this.state.employee)
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextState.employee !== this.state.employee ||
      nextProps.roles !== this.props.roles ||
      nextState.isLoading !== this.state.isLoading
    )
  }

  render() {
    const { roles } = this.props
    const { employee } = this.state
    return (
      <Segment basic>
        <Header as="h4">
          <Icon name="add user" />
          <Header.Content>Create Employee</Header.Content>
        </Header>
        <Form>
          <Form.Input
            label="Name"
            placeholder="Name"
            name="name"
            value={employee.name}
            onChange={this.updateEmployeeState}
          />
          <Form.Input
            label="Phone Number"
            placeholder="Phone number"
            name="phoneNumber"
            value={employee.phoneNumber}
            onChange={this.updateEmployeeState}
          />
          <Form.Input
            label="Password"
            placeholder="Password"
            name="password"
            value={employee.password}
            type="password"
            onChange={this.updateEmployeeState}
          />
          <Form.Select
            label="Role"
            options={MapToOptions(roles)}
            placeholder="Role"
            name="role"
            value={employee.role}
            onChange={this.updateEmployeeState}
          />
        </Form>
        <br />
        <Button onClick={this.onSubmit}>Submit</Button>
      </Segment>
    )
  }
}

function MapToOptions(roles) {
  let toReturn = []
  roles.forEach(role => {
    toReturn.push({
      key: role,
      text: role,
      value: role
    })
  })
  return toReturn
}

EmployeeForm.propTypes = {
  createEmployee: PropTypes.func.isRequired,
  roles: PropTypes.array.isRequired
}

export default EmployeeForm
