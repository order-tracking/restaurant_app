import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Segment } from 'semantic-ui-react'
import EmployeeForm from './EmployeeForm'
import * as Routes from '../../routes'
import { toastr } from 'react-redux-toastr'
import { withRouter } from 'react-router'

class CreateEmployee extends Component {
  constructor(props) {
    super(props)
    this.state = {}

    this.createEmployee = this.createEmployee.bind(this)
  }

  createEmployee(employee) {
    this.props.actions
      .createEmployee(employee)
      .then(emp => {
        toastr.success('Employee Successfully Created', '', {
          showCloseButton: false
        })
        this.props.history.push(Routes.EMPLOYEEPAGE.replace(':id', emp.id))
      })
      .catch(error => toastr.error(error))
  }

  componentDidMount() {
    this.props.actions.changeBreadcrumbs([
      {
        key: 'Dashboard',
        content: 'Dashboard',
        as: Link,
        to: Routes.HOMEPAGE
      },
      {
        key: 'Employees',
        content: 'Employees',
        as: Link,
        to: Routes.EMPLOYEESPAGE
      },
      { key: 'Create', content: 'Create', active: true }
    ])
  }

  render() {
    return (
      <Segment>
        <EmployeeForm
          roles={this.props.roles}
          createEmployee={this.createEmployee}
        />
      </Segment>
    )
  }
}

CreateEmployee.propTypes = {
  roles: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
}

export default withRouter(CreateEmployee)
