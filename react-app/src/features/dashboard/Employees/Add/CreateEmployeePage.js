import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { changeBreadcrumbs } from '../../Layout/layoutReducer'
import { actions } from '../employeesReducer'
import CreateEmployee from './CreateEmployee'
import { getAllowedRoles } from '../../../../models/Roles'

class CreateEmployeePage extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <CreateEmployee
        roles={getAllowedRoles(this.props.user.role)}
        actions={this.props.actions}
      />
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {
    user: state.session.user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      { changeBreadcrumbs, createEmployee: actions.createEmployee },
      dispatch
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateEmployeePage)
