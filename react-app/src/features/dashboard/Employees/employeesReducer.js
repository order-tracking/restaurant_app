import * as helpers from '../../../helpers/ReduxHelpers'
import EmployeesRepository from '../../../repositories/EmployeesRepository'

/*
 |--------------------------------------------------------------------------
 | Types
 |--------------------------------------------------------------------------
 */
export const CREATE_EMPLOYEE_SUCCESS =
  'otrack/dashboard/employees/CREATE_EMPLOYEE_SUCCESS'

export const GET_EMPLOYEES_SUCCESS =
  'otrack/dashboard/employees/GET_EMPLOYEES_SUCCESS'

export const GET_EMPLOYEE_SUCCESS =
  'otrack/dashboard/employees/GET_EMPLOYEE_SUCCESS'

export const DELETE_EMPLOYEE_SUCCESS =
  'otrack/dashboard/employees/DELETE_EMPLOYEE_SUCCESS'

export const EDIT_EMPLOYEE_SUCCESS =
  'otrack/dashboard/employees/EDIT_EMPLOYEE_SUCCESS'

/*
 |--------------------------------------------------------------------------
 | Reducer
 |--------------------------------------------------------------------------
 */
const initialState = {
  count: -1,
  limit: -1,
  page: -1,
  hasNextPage: false,
  hasPreviousPage: false,
  items: []
}

export default helpers.createReducer(initialState, {
  [GET_EMPLOYEES_SUCCESS]: (state, action) => {
    return action.employees
  },
  [GET_EMPLOYEE_SUCCESS]: (state, action) => {
    let employee = state.items.find(
      employee => employee.id === action.employee.id
    )

    if (employee === undefined) {
      return { ...state, items: [...state.items, action.employee] }
    }
    return {
      ...state,
      items: helpers.updateItemInArray(
        state.items,
        employee.id,
        old => action.employee
      )
    }
  },
  [CREATE_EMPLOYEE_SUCCESS]: (state, action) => {
    return { ...state, items: [...state.items, action.employee] }
  },
  [DELETE_EMPLOYEE_SUCCESS]: (state, action) => {
    return {
      ...state,
      items: state.items.filter(employee => employee.id !== action.id)
    }
  },
  [EDIT_EMPLOYEE_SUCCESS]: (state, action) => {
    console.log('Edit employee success!')
    return state
  }
})

/*
 |--------------------------------------------------------------------------
 | Action Creators
 |--------------------------------------------------------------------------
 */
const loadEmployeesSuccess = employees => {
  return { type: GET_EMPLOYEES_SUCCESS, employees }
}

const getEmployeeSuccess = employee => {
  return { type: GET_EMPLOYEE_SUCCESS, employee }
}

const createEmployeeSuccess = employee => {
  return { type: CREATE_EMPLOYEE_SUCCESS, employee }
}

const editEmployeeSuccess = id => {
  return { type: EDIT_EMPLOYEE_SUCCESS, id }
}

const deleteEmployeeSuccess = id => {
  return { type: DELETE_EMPLOYEE_SUCCESS, id }
}

const getEmployees = (page = 1, limit = 5) => {
  return dispatch => {
    return EmployeesRepository.getEmployees(page, limit).then(employees => {
      console.log(employees)
      dispatch(loadEmployeesSuccess(employees))
    })
  }
}

const getEmployee = id => {
  return dispatch => {
    return EmployeesRepository.getEmployee(id).then(employee => {
      console.log(employee)
      dispatch(getEmployeeSuccess(employee))
      return employee
    })
  }
}

const createEmployee = employee => {
  return dispatch => {
    return EmployeesRepository.createEmployee(employee).then(employee => {
      console.log(employee)
      dispatch(createEmployeeSuccess(employee))
      return employee
    })
  }
}

const editEmployee = employee => {
  return dispatch => {
    return EmployeesRepository.editEmployee(employee).then(result => {
      console.log(result)
      dispatch(editEmployeeSuccess(employee.id))
    })
  }
}

const deleteEmployee = id => {
  return dispatch => {
    return EmployeesRepository.deleteEmployee(id).then(result => {
      dispatch(deleteEmployeeSuccess(id))
    })
  }
}

export const actions = {
  loadEmployeesSuccess,
  createEmployeeSuccess,
  deleteEmployeeSuccess,
  getEmployeeSuccess,
  getEmployees,
  getEmployee,
  createEmployee,
  deleteEmployee,
  editEmployee
}
