import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { DragDropContext } from 'react-dnd'
import MultiBackend from 'react-dnd-multi-backend'
import { Grid, Segment } from 'semantic-ui-react'
import ProcessGridColumnComponent from './ProcessGridColumnComponent'
import { connect } from 'react-redux'
import * as DnDHelpers from '../../../helpers/DnDHelpers'

class PipelineKanban extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true
    }

    this.getData = this.getData.bind(this)
  }

  getData() {
    return this.props.getProcessPipeline(this.props.processId)
  }

  componentDidMount() {
    this.getData().then(_ => this.setState({ isLoading: false }))
    this.intervalId = setInterval(this.getData, 2000)
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.processId !== prevProps.processId) {
      this.setState({ isLoading: true })
      this.getData().then(_ => this.setState({ isLoading: false }))
      clearInterval(this.intervalId)
      this.intervalId = setInterval(this.getData, 2000)
    }
  }

  // Required due to polling information every 2 seconds
  shouldComponentUpdate(nextProps, nextState) {
    return (
      this.props.pipeline !== nextProps.pipeline ||
      this.props.processId !== nextProps.processId ||
      nextState.isLoading !== this.state.isLoading
    )
  }

  componentWillUnmount() {
    clearInterval(this.intervalId)
  }

  render() {
    const { isLoading } = this.state
    const { pipeline } = this.props
    return (
      <Segment basic loading={isLoading}>
        <Grid stackable columns="equal">
          {pipeline.processes &&
            pipeline.processes.map(process => {
              return (
                <ProcessGridColumnComponent
                  key={process.id}
                  last={false} //Required to know if item is completed or not
                  process={process}
                  editItem={this.props.editItem}
                />
              )
            })}
          {/*Last Process which defines if an item is ready to be assembled. i.e: It's process is complete*/}
          <ProcessGridColumnComponent
            last={true} //Required to know if item is completed or not
            process={{ name: 'Done' }}
            editItem={this.props.editItem}
          />
        </Grid>
      </Segment>
    )
  }
}

PipelineKanban.propTypes = {
  pipeline: PropTypes.object.isRequired,
  processId: PropTypes.string.isRequired,
  editItem: PropTypes.func.isRequired,
  getProcessPipeline: PropTypes.func.isRequired
}

function mapStateToProps(state) {
  return {
    pipeline: state.processPipelines.currentProcess
  }
}

export default connect(mapStateToProps)(
  DragDropContext(MultiBackend(DnDHelpers.getBackends()))(PipelineKanban)
)
