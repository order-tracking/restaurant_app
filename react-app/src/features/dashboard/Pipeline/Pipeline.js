import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Helmet } from 'react-helmet'
import {
  Button,
  Divider,
  Dropdown,
  Grid,
  Header,
  Icon,
  Segment
} from 'semantic-ui-react'
import { Link, NavLink } from 'react-router-dom'
import { withRouter } from 'react-router'
import PipelineKanban from './PipelineKanban'
import * as Routes from '../routes'
import CenteredSegmentTextComponent from '../../../components/CenteredSegmentTextComponent'

class Pipeline extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      options: []
    }

    this.getData = this.getData.bind(this)
  }

  getData() {
    this.props.getProcessPipelines().then(_ => {
      // Redirect to a specific Process Pipeline if none is specified
      if (!this.props.match.params.id && this.props.pipelines.items[0]) {
        const id = this.props.pipelines.items[0].id
        this.props.history.push(Routes.PIPELINEPAGE.replace(':id', id))
      } else {
        this.setState({
          isLoading: false,
          options: MapPipelinesToOptions(this.props.pipelines)
        })
      }
    })
  }

  // Used to get the value of a Pipeline to provide the Dropdown
  getValue(options) {
    const opt = options.find(
      opt => opt.key === parseInt(this.props.match.params.id, 10)
    )
    return opt ? opt.value : undefined
  }

  componentDidMount() {
    this.props.setBreadcrumbs([
      { key: 'Dashboard', content: 'Dashboard', as: Link, to: Routes.HOMEPAGE },
      { key: 'Pipelines', content: 'Pipeline', active: true }
    ])

    this.getData()
  }

  //TODO:
  // shouldComponentUpdate(nextProps, nextState) {
  //   return (
  //     nextProps.options !== this.props.options ||
  //     nextState.isLoading !== this.state.isLoading
  //   )
  // }

  render() {
    const { isLoading, options } = this.state
    return (
      <Segment loading={isLoading} clearing>
        <Helmet title="Pipeline" />
        <Grid stackable columns={2}>
          <Grid.Column width={6}>
            <Button
              as={NavLink}
              to={Routes.ASSEMBLEPAGE}
              floated="right"
              content="Assemble Orders"
              icon="shopping basket"
              labelPosition="left"
              primary
            />
            <Header as="h4" floated="left">
              <Icon name="fork" />
              <Header.Content>
                Pipelines
                <Header.Subheader>Company Pipelines</Header.Subheader>
              </Header.Content>
            </Header>
          </Grid.Column>
          <Grid.Column width={10}>
            <Dropdown
              fluid
              placeholder={'Pipelines'}
              selection
              options={options}
              value={this.getValue(options)}
              onFocus={this.getData}
            />
          </Grid.Column>
        </Grid>
        <Divider />
        {this.props.match.params.id && (
          <PipelineKanban
            processId={this.props.match.params.id}
            getProcessPipeline={this.props.getProcessPipeline}
            editItem={this.props.editItem}
          />
        )}
        {!this.props.match.params.id && (
          <CenteredSegmentTextComponent text="No Pipelines yet" />
        )}
      </Segment>
    )
  }
}

function MapPipelinesToOptions(pipelines) {
  let toReturn = []
  pipelines.items.forEach(pipeline => {
    let itemsToBeCompleted = 0
    pipeline.processes.forEach(process => {
      itemsToBeCompleted += process.items.length
    })

    toReturn.push({
      key: pipeline.id,
      text: pipeline.name,
      value: pipeline.id,
      label: { color: 'black', content: itemsToBeCompleted, circular: true },
      as: NavLink,
      to: Routes.PIPELINEPAGE.replace(':id', pipeline.id)
    })
  })
  return toReturn
}

Pipeline.propTypes = {
  pipelines: PropTypes.object.isRequired,
  setBreadcrumbs: PropTypes.func.isRequired,
  getProcessPipelines: PropTypes.func.isRequired,
  getProcessPipeline: PropTypes.func.isRequired,
  editItem: PropTypes.func.isRequired
}

export default withRouter(Pipeline)
