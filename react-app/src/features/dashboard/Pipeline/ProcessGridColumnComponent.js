import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Divider, Grid } from 'semantic-ui-react'
import { DropTarget } from 'react-dnd'
import ItemGridRowComponent from './ItemGridRowComponent'
import * as DnDHelpers from '../../../helpers/DnDHelpers'

const itemTarget = {
  drop(props) {
    return {
      processId: props.process.id,
      completed: props.last
    }
  }
}

class ProcessGridColumnComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const { connectDropTarget, process } = this.props
    return connectDropTarget(
      // react-dnd only allows native elements be passed.
      // className is equal to <Grid.Column textAlign="center" >
      <div className="center aligned column">
        <Grid.Row>
          <b>{process.name}</b>
        </Grid.Row>
        <Divider />
        {process.items &&
          process.items.map(item => {
            return (
              <ItemGridRowComponent
                key={item.id}
                item={item}
                editItem={this.props.editItem}
              />
            )
          })}
      </div>
    )
  }
}

ProcessGridColumnComponent.propTypes = {
  last: PropTypes.bool.isRequired,
  process: PropTypes.object.isRequired,
  editItem: PropTypes.func.isRequired
}

export default DropTarget('Item', itemTarget, DnDHelpers.dropTargetCollect)(
  ProcessGridColumnComponent
)
