import React, { Component } from 'react'
import { connect } from 'react-redux'
import { changeBreadcrumbs } from '../Layout/layoutReducer'
import { bindActionCreators } from 'redux'
import Pipeline from './Pipeline'
import {
  editItem,
  getProcessPipeline,
  getProcessPipelines
} from '../Processes/processesReducer'

class PipelinePage extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <Pipeline
        pipelines={this.props.pipelines}
        setBreadcrumbs={this.props.actions.changeBreadcrumbs}
        getProcessPipelines={this.props.actions.getProcessPipelines}
        getProcessPipeline={this.props.actions.getProcessPipeline}
        editItem={this.props.actions.editItem}
      />
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {
    pipelines: state.processPipelines.list
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      { changeBreadcrumbs, getProcessPipelines, getProcessPipeline, editItem },
      dispatch
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PipelinePage)
