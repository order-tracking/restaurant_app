import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Header, Label, Segment } from 'semantic-ui-react'
import { DragSource } from 'react-dnd'
import { toastr } from 'react-redux-toastr'
import * as DnDHelpers from '../../../helpers/DnDHelpers'

const itemSource = {
  beginDrag(props) {
    return {
      item: props.item
    }
  },

  endDrag(props, monitor, component) {
    const dropResult = monitor.getDropResult()
    console.log(dropResult)
    console.log('PROPS', props)

    if (!dropResult || dropResult.processId === props.item.processId) {
      return
    }
    let item = Object.assign({}, props.item)
    item.processId = dropResult.processId
    item.completed = dropResult.completed

    props
      .editItem(item)
      .then(_ => {
        if (dropResult.completed) {
          toastr.success('Item Completed', '', {
            showCloseButton: false
          })
        }
      })
      .catch(error => toastr.error('Could not update. Try again'))
  }
}

class ItemGridRowComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const { connectDragSource, isDragging, item } = this.props
    return connectDragSource(
      <div
        style={{
          opacity: isDragging ? 0.5 : 1,
          fontSize: 25,
          fontWeight: 'bold',
          cursor: 'move'
        }}
      >
        <Segment raised>
          <Label attached="top" content={'Item from Order ' + item.orderId} />
          <Header as="h5">
            <Header.Content>
              {item.quantity} {item.product.name}{' '}
              {item.productType ? item.productType.name : ''}
              <Header.Subheader>{item.note}</Header.Subheader>
            </Header.Content>
          </Header>
        </Segment>
      </div>
    )
  }
}

ItemGridRowComponent.propTypes = {
  item: PropTypes.object.isRequired,
  editItem: PropTypes.func.isRequired
}

export default DragSource('Item', itemSource, DnDHelpers.dragSourceCollect)(
  ItemGridRowComponent
)
