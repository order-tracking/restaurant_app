import React from 'react'
import { Sidebar } from 'semantic-ui-react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { Helmet } from 'react-helmet'
import SidebarMenu from './Layout/SidebarMenu'
import MainContent from './Layout/MainContent'
import HandleAuthComponent from '../../components/HandleAuthComponent'

const Dashboard = ({ collapsed, session }) => {
  return (
    <HandleAuthComponent>
      <Helmet titleTemplate="%s - Dashboard oTrack" />
      <div className="sidebar-container">
        {!collapsed && <SidebarMenu session={session} />}
      </div>

      <Sidebar.Pusher className={!collapsed ? '' : 'no-padding-left'}>
        <MainContent />
      </Sidebar.Pusher>
    </HandleAuthComponent>
  )
}

Dashboard.propTypes = {
  collapsed: PropTypes.bool.isRequired,
  session: PropTypes.object.isRequired
}

const mapStateToProps = (state, ownProps) => {
  return {
    collapsed: state.page.menu_collapsed,
    session: state.session
  }
}

export default connect(mapStateToProps)(Dashboard)
