import * as helpers from '../../../helpers/ReduxHelpers'
import ProcessPipelinesRepository from '../../../repositories/ProcessPipelinesRepository'
import OrdersRepository from '../../../repositories/OrdersRepository'

/*
 |--------------------------------------------------------------------------
 | Types
 |--------------------------------------------------------------------------
 */
export const CREATE_PROCESSPIPELINE_SUCCESS =
  'otrack/dashboard/processes/CREATE_PROCESSPIPELINE_SUCCESS'

export const GET_PROCESSPIPELINES_SUCCESS =
  'otrack/dashboard/processes/GET_PROCESSPIPELINES_SUCCESS'

export const GET_PROCESSPIPELINE_SUCCESS =
  'otrack/dashboard/processes/GET_PROCESSPIPELINE_SUCCESS'

export const DELETE_PROCESSPIPELINE_SUCCESS =
  'otrack/dashboard/processes/DELETE_PROCESSPIPELINE_SUCCESS'

export const EDIT_PROCESSPIPELINE_SUCCESS =
  'otrack/dashboard/processes/EDIT_PROCESSPIPELINE_SUCCESS'

export const EDIT_ORDERITEM_SUCCESS =
  'otrack/dashboard/processes/EDIT_ORDERITEM_SUCCESS'

/*
 |--------------------------------------------------------------------------
 | Reducer
 |--------------------------------------------------------------------------
 */
const initialState = {
  list: {
    count: -1,
    limit: -1,
    page: -1,
    hasNextPage: false,
    hasPreviousPage: false,
    items: []
  },
  currentProcess: {}
}

export default helpers.createReducer(initialState, {
  [GET_PROCESSPIPELINES_SUCCESS]: (state, action) => {
    return {
      ...state,
      list: action.processPipelines
    }
  },
  [GET_PROCESSPIPELINE_SUCCESS]: (state, action) => {
    return {
      ...state,
      currentProcess: action.processPipeline
    }
  },
  [DELETE_PROCESSPIPELINE_SUCCESS]: (state, action) => {
    return {
      ...state,
      list: {
        ...state.list,
        items: state.list.items.filter(client => client.id !== action.id)
      }
    }
  },
  [CREATE_PROCESSPIPELINE_SUCCESS]: (state, action) => {
    return {
      ...state,
      list: {
        ...state.list,
        items: [...state.list.items, action.processPipeline]
      }
    }
  },
  [EDIT_PROCESSPIPELINE_SUCCESS]: (state, action) => {
    return {
      ...state,
      list: {
        ...state.list,
        items: state.list.items.map(pipeline => {
          if (pipeline.id === action.processPipeline.id) {
            return action.processPipeline
          }
          return pipeline
        })
      }
    }
  },
  [EDIT_ORDERITEM_SUCCESS]: (state, action) => {
    const { item } = action
    return {
      ...state,
      currentProcess: {
        ...state.currentProcess,
        processes: state.currentProcess.processes.map(process => {
          // Add
          if (process.id === item.processId) {
            return { ...process, items: [...process.items, item] }
          }

          // Remove
          return {
            ...process,
            items: process.items.filter(itm => itm.id !== item.id)
          }
        })
      }
    }
  }
})

/*
 |--------------------------------------------------------------------------
 | Action Creators
 |--------------------------------------------------------------------------
 */
export function loadProcessPipelinesSuccess(processPipelines) {
  return { type: GET_PROCESSPIPELINES_SUCCESS, processPipelines }
}

export function loadProcessPipelineSuccess(processPipeline) {
  return { type: GET_PROCESSPIPELINE_SUCCESS, processPipeline }
}

export function createProcessPipelineSuccess(processPipeline) {
  return { type: CREATE_PROCESSPIPELINE_SUCCESS, processPipeline }
}

export function deleteProcessPipelineSuccess(id) {
  return { type: DELETE_PROCESSPIPELINE_SUCCESS, id }
}

export function editProcessPipelineSuccess(processPipeline) {
  return { type: EDIT_PROCESSPIPELINE_SUCCESS, processPipeline }
}

export function editItemSuccess(item) {
  return { type: EDIT_ORDERITEM_SUCCESS, item }
}

export function getProcessPipelines(page, limit) {
  return dispatch => {
    return ProcessPipelinesRepository.getProcessPipelines(
      page,
      limit
    ).then(processPipelines => {
      console.log(processPipelines)
      dispatch(loadProcessPipelinesSuccess(processPipelines))
    })
  }
}

export function getProcessPipeline(id) {
  return dispatch => {
    return ProcessPipelinesRepository.getProcessPipeline(
      id
    ).then(processPipeline => {
      console.log(processPipeline)
      dispatch(loadProcessPipelineSuccess(processPipeline))
      return processPipeline
    })
  }
}

export function deleteProcessPipeline(id) {
  return dispatch => {
    return ProcessPipelinesRepository.deleteProcessPipeline(id).then(result => {
      dispatch(deleteProcessPipelineSuccess(id))
    })
  }
}

export function createProcessPipeline(processPipeline) {
  return dispatch => {
    return ProcessPipelinesRepository.createProcessPipeline(
      processPipeline
    ).then(processPipeline => {
      console.log(processPipeline)
      dispatch(createProcessPipelineSuccess(processPipeline))
      return processPipeline
    })
  }
}

export function editProcessPipeline(processPipeline) {
  return dispatch => {
    return ProcessPipelinesRepository.editProcessPipeline(
      processPipeline
    ).then(result => {
      dispatch(editProcessPipelineSuccess(processPipeline))
      return processPipeline
    })
  }
}

export function editItem(item) {
  return dispatch => {
    return OrdersRepository.editItem(item).then(_ => {
      dispatch(editItemSuccess(item))
      return item
    })
  }
}
