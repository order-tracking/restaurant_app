import React, { Component } from 'react'
import { withRouter } from 'react-router'
import PropTypes from 'prop-types'
import { Button, Divider, Header, Icon, Segment } from 'semantic-ui-react'
import { Link, NavLink } from 'react-router-dom'
import PaginatedProcessPipelines from './PaginatedProcessPipelines'
import { Helmet } from 'react-helmet'
import CenteredSegmentTextComponent from '../../../components/CenteredSegmentTextComponent'
import * as Routes from '../routes'
import * as UrlHelpers from '../../../helpers/UrlHelpers'

class Processes extends Component {
  constructor(props) {
    super(props)
    let params = new URLSearchParams(props.location.search)
    this.state = {
      isLoading: true,
      params: {
        page: parseInt(params.get('page'), 10) || 1,
        limit: parseInt(params.get('limit'), 10) || 5
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    let nextParams = new URLSearchParams(nextProps.location.search)
    let page = parseInt(nextParams.get('page'), 10)
    let limit = parseInt(nextParams.get('limit'), 10)
    if (
      (nextParams.get('page') && page !== this.state.params.page) ||
      (nextParams.get('limit') && limit !== this.state.params.limit)
    ) {
      this.setState({
        isLoading: true,
        params: {
          page: page || this.state.params.page,
          limit: limit || this.state.params.limit
        }
      })
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { page, limit } = this.state.params
    if (this.state.params !== prevState.params) {
      this.props
        .getProcessPipelines(page, limit)
        .then(_ => this.setState({ isLoading: false }))
    }
  }

  componentDidMount() {
    const { page, limit } = this.state.params
    this.props
      .getProcessPipelines(page, limit)
      .then(_ => this.setState({ isLoading: false }))

    this.props.changeBreadcrumbs([
      {
        key: 'Dashboard',
        content: 'Dashboard',
        as: Link,
        to: Routes.HOMEPAGE
      },
      { key: 'Processes', content: 'Processes', active: true }
    ])
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextProps.processPipelines !== this.props.processPipelines ||
      nextState.isLoading !== this.state.isLoading
    )
  }

  render() {
    const { processPipelines } = this.props
    const { params } = this.state
    return (
      <Segment loading={this.state.isLoading}>
        <Helmet title="Process Pipelines" />
        <Segment basic>
          <Header as="h4" floated="left">
            <Icon name="road" />
            <Header.Content>
              Process Pipelines
              <Header.Subheader>Company Pipelines</Header.Subheader>
            </Header.Content>
          </Header>
          <Button
            as={NavLink}
            to={Routes.ADDPROCESSPAGE}
            floated="right"
            content="Create new Process Pipeline"
            icon="plus"
            labelPosition="left"
            primary
          />
        </Segment>
        <Divider clearing />
        {processPipelines.count > 0 && (
          <PaginatedProcessPipelines
            processPipelines={processPipelines}
            deleteProcessPipeline={this.props.deleteProcessPipeline}
            nextLink={
              Routes.PROCESSESPAGE +
              '?' +
              UrlHelpers.getQueryString({
                ...params,
                page: params.page + 1
              })
            }
            previousLink={
              Routes.PROCESSESPAGE +
              '?' +
              UrlHelpers.getQueryString({
                ...params,
                page: params.page - 1
              })
            }
          />
        )}
        {processPipelines.count <= 0 && (
          <CenteredSegmentTextComponent text="No Pipelines yet" />
        )}
      </Segment>
    )
  }
}

Processes.propTypes = {
  processPipelines: PropTypes.object.isRequired,
  changeBreadcrumbs: PropTypes.func.isRequired,
  deleteProcessPipeline: PropTypes.func.isRequired,
  getProcessPipelines: PropTypes.func.isRequired
}

export default withRouter(Processes)
