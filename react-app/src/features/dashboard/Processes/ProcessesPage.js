import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Processes from './Processes'
import { changeBreadcrumbs } from '../Layout/layoutReducer'
import { deleteProcessPipeline, getProcessPipelines } from './processesReducer'

class ProcessesPage extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <Processes
        processPipelines={this.props.processPipelines}
        changeBreadcrumbs={this.props.actions.changeBreadcrumbs}
        deleteProcessPipeline={this.props.actions.deleteProcessPipeline}
        getProcessPipelines={this.props.actions.getProcessPipelines}
      />
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {
    processPipelines: state.processPipelines.list
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      Object.assign(
        {},
        { changeBreadcrumbs },
        {
          getProcessPipelines,
          deleteProcessPipeline
        }
      ),
      dispatch
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProcessesPage)
