import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import {
  Button,
  Divider,
  Form,
  Header,
  Icon,
  Segment,
  Step
} from 'semantic-ui-react'
import { toastr } from 'react-redux-toastr'
import * as Routes from '../../routes'
import ConfirmDeleteComponent from '../../../../components/ConfirmDeleteComponent'

class SingleProcess extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      processPipeline: {
        id: undefined,
        name: undefined,
        processes: []
      },
      editEnabled: false,
      processForEdit: {}
    }

    this.enableEdit = this.enableEdit.bind(this)
    this.updateProcessName = this.updateProcessName.bind(this)
    this.editProcess = this.editProcess.bind(this)
    this.cancelEdit = () => this.setState({ editEnabled: false })
    this.deleteProcessPipeline = this.deleteProcessPipeline.bind(this)
  }

  componentDidMount() {
    this.props.changeBreadcrumbs([
      { key: 'Dashboard', content: 'Dashboard', as: Link, to: Routes.HOMEPAGE },
      {
        key: 'Processes',
        content: 'Processes',
        as: Link,
        to: Routes.PROCESSESPAGE
      },
      { key: 'Details', content: 'Process Pipeline Details', active: true }
    ])

    this.props.getProcessPipeline(this.props.match.params.id).then(_ => {
      this.setState({
        processPipeline: this.props.processPipeline,
        isLoading: false
      })
    })
  }

  enableEdit(event, { value }) {
    this.setState({
      editEnabled: true,
      processForEdit: Object.assign({}, value)
    })
  }

  updateProcessName(event, { name, value }) {
    this.setState({
      processForEdit: {
        ...this.state.processForEdit,
        [name]: value
      }
    })
  }

  editProcess(event) {
    let pipeline = {
      ...this.state.processPipeline,
      processes: this.state.processPipeline.processes.map(process => {
        if (process.id === this.state.processForEdit.id) {
          return Object.assign({}, this.state.processForEdit)
        }
        return process
      })
    }

    this.props
      .editProcessPipeline(pipeline)
      .then(processPipeline => {
        this.setState({
          editEnabled: false,
          processForEdit: {},
          processPipeline: processPipeline
        })
        toastr.success('Pipeline successfully edited', '', {
          showCloseButton: false
        })
      })
      .catch(error => toastr.error(error))
  }

  deleteProcessPipeline() {
    this.props
      .deleteProcessPipeline(this.state.processPipeline.id)
      .then(_ => {
        this.props.history.goBack()
        toastr.success('Process Pipeline successfully deleted', '', {
          showCloseButton: false
        })
      })
      .catch(error => toastr.error(error.message))
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextState.processPipeline !== this.state.processPipeline ||
      nextState.processForEdit !== this.state.processForEdit ||
      nextState.editEnabled !== this.state.editEnabled ||
      nextState.isLoading !== this.state.isLoading
    )
  }

  render() {
    const {
      processPipeline,
      isLoading,
      editEnabled,
      processForEdit
    } = this.state
    return (
      <Segment loading={isLoading}>
        <Segment basic>
          <Header as="h1" floated="left">
            <Header.Content>{processPipeline.name}</Header.Content>
          </Header>
          <ConfirmDeleteComponent
            delete={this.deleteProcessPipeline}
            floated="right"
            color="red"
          />
          <Divider clearing />
          <Header as="h4">
            <Icon name="road" />
            <Header.Content>
              Processes
              <Header.Subheader>Click to edit</Header.Subheader>
            </Header.Content>
          </Header>
          <Step.Group>
            {processPipeline.processes.map(process => (
              <Step
                key={process.id}
                as={Button}
                onClick={this.enableEdit}
                value={process}
              >
                <Step.Content>
                  <Step.Title>{process.name}</Step.Title>
                </Step.Content>
              </Step>
            ))}
          </Step.Group>
          {editEnabled && (
            <Form>
              <Form.Input
                label="Process Name"
                name="name"
                value={processForEdit.name}
                onChange={this.updateProcessName}
              />
              <Button type="button" onClick={this.editProcess}>
                Save
              </Button>
              <Button type="button" onClick={this.cancelEdit}>
                Cancel
              </Button>
            </Form>
          )}
        </Segment>
      </Segment>
    )
  }
}

SingleProcess.propTypes = {
  processPipeline: PropTypes.object.isRequired,
  getProcessPipeline: PropTypes.func.isRequired,
  changeBreadcrumbs: PropTypes.func.isRequired,
  editProcessPipeline: PropTypes.func.isRequired,
  deleteProcessPipeline: PropTypes.func.isRequired
}

export default withRouter(SingleProcess)
