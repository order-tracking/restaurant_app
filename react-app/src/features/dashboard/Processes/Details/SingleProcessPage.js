import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import SingleProcess from './SingleProcess'
import { changeBreadcrumbs } from '../../Layout/layoutReducer'
import {
  deleteProcessPipeline,
  editProcessPipeline,
  getProcessPipeline
} from '../processesReducer'

class SingleProcessPage extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <SingleProcess
        processPipeline={this.props.pipeline}
        changeBreadcrumbs={this.props.actions.changeBreadcrumbs}
        getProcessPipeline={this.props.actions.getProcessPipeline}
        editProcessPipeline={this.props.actions.editProcessPipeline}
        deleteProcessPipeline={this.props.actions.deleteProcessPipeline}
      />
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {
    pipeline: state.processPipelines.currentProcess
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      Object.assign(
        {},
        { changeBreadcrumbs },
        {
          getProcessPipeline,
          editProcessPipeline,
          deleteProcessPipeline
        }
      ),
      dispatch
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SingleProcessPage)
