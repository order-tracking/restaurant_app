import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Button, Label, Step } from 'semantic-ui-react'

class ProcessesToAddList extends Component {
  render() {
    const { processes } = this.props
    return (
      <Step.Group>
        {processes.map((process, i) => (
          <Step key={i}>
            <Label
              as={Button}
              onClick={this.props.removeProcess}
              value={i}
              attached={'bottom'}
              icon="delete"
            />
            <Step.Content>
              <Step.Title>{process.name}</Step.Title>
            </Step.Content>
          </Step>
        ))}
      </Step.Group>
    )
  }
}

ProcessesToAddList.propTypes = {
  processes: PropTypes.array.isRequired,
  removeProcess: PropTypes.func.isRequired
}

export default ProcessesToAddList
