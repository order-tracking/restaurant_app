import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { toastr } from 'react-redux-toastr'
import { Button, Divider, Grid, Header, Icon, Segment } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import ProcessesToAddList from './ProcessesToAddList'
import { withRouter } from 'react-router'
import * as Routes from '../../routes'
import FormValidation from '../../../../components/validation/FormValidation'
import ValidationField from '../../../../components/validation/ValidationField'
import { min } from '../../../../helpers/ValidationRules'

class AddProcessPipeline extends Component {
  constructor(props) {
    super(props)
    this.state = {
      nameCreated: false,
      currentProcessName: '',
      processPipelineUpdated: false,
      processPipeline: {
        name: '',
        processes: []
      }
    }

    this.updateStateNameCreated = this.updateStateNameCreated.bind(this)
    this.addProcessToPipeline = this.addProcessToPipeline.bind(this)
    this.updatePipeline = this.updatePipeline.bind(this)
    this.removeProcess = this.removeProcess.bind(this)
    this.createProcessPipeline = this.createProcessPipeline.bind(this)
  }

  componentDidMount() {
    this.props.changeBreadcrumbs([
      { key: 'Dashboard', content: 'Dashboard', as: Link, to: Routes.HOMEPAGE },
      {
        key: 'Processes',
        content: 'Processes',
        as: Link,
        to: Routes.PROCESSESPAGE
      },
      { key: 'Create', content: 'Create Process Pipeline', active: true }
    ])
  }

  updateStateNameCreated(pipeline) {
    this.setState({
      processPipeline: { ...this.state.processPipeline, name: pipeline.name },
      nameCreated: true
    })
  }

  addProcessToPipeline(process) {
    this.setState({
      processPipeline: {
        ...this.state.processPipeline,
        processes: [
          ...this.state.processPipeline.processes,
          {
            name: process.name,
            orderNumber: -1
          }
        ]
      }
    })
  }

  updatePipeline(event, { name, value }) {
    this.setState({
      processPipeline: {
        ...this.state.processPipeline,
        [name]: value
      }
    })
  }

  removeProcess(event, { value }) {
    this.setState({
      processPipeline: {
        ...this.state.processPipeline,
        processes: this.state.processPipeline.processes.filter(
          (item, i) => i !== value
        )
      }
    })
  }

  createProcessPipeline() {
    const { processPipeline } = this.state
    processPipeline.processes.forEach((process, i) => {
      process.orderNumber = i + 1
    })
    this.props
      .createProcessPipeline(processPipeline)
      .then(processPipeline => {
        this.props.history.push(
          Routes.PROCESSPAGE.replace(':id', processPipeline.id)
        )
        toastr.success('Pipeline Successfully Created', '', {
          showCloseButton: false
        })
      })
      .catch(error => toastr.error(error))
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextState.processPipeline !== this.state.processPipeline ||
      nextState.nameCreated !== this.state.nameCreated ||
      nextState.currentProcessName !== this.state.currentProcessName ||
      nextState.isLoading !== this.state.isLoading
    )
  }

  render() {
    const { processPipeline } = this.state
    return (
      <Segment>
        <Segment basic>
          <Header as="h4">
            <Icon name="add circle" />
            <Header.Content>Create Process Pipeline</Header.Content>
          </Header>

          <Grid>
            <Grid.Row>
              <Grid.Column width="7">
                <FormValidation onSubmit={this.updateStateNameCreated}>
                  <ValidationField
                    placeholder="Name of the pipeline"
                    name="name"
                    label="Process Pipeline Name"
                    rules={[min(3)]}
                    action={
                      <Button
                        content="Create Process Pipeline"
                        icon="plus"
                        labelPosition="left"
                        primary
                      />
                    }
                  />
                </FormValidation>
              </Grid.Column>
              {this.state.nameCreated && (
                <Grid.Column width="7">
                  <FormValidation onSubmit={this.addProcessToPipeline}>
                    <ValidationField
                      placeholder="Process Name"
                      name="name"
                      label="Process Name"
                      rules={[min(3)]}
                      action={
                        <Button
                          content="Add a Process"
                          icon="plus"
                          labelPosition="left"
                          primary
                        />
                      }
                    />
                  </FormValidation>
                </Grid.Column>
              )}
            </Grid.Row>
            <Divider clearing />

            <Grid.Row>
              <Grid.Column>
                {this.state.nameCreated && (
                  <Header>
                    <strong>Pipeline:</strong> {processPipeline.name}
                  </Header>
                )}
              </Grid.Column>
            </Grid.Row>

            <Grid.Row>
              <Grid.Column>
                {processPipeline.processes.length > 0 && (
                  <ProcessesToAddList
                    processes={processPipeline.processes}
                    removeProcess={this.removeProcess}
                  />
                )}
              </Grid.Column>
            </Grid.Row>

            <Grid.Row>
              {this.state.nameCreated &&
                processPipeline.processes.length > 0 && (
                  <Grid.Column width="7">
                    <Button onClick={this.createProcessPipeline}>Create</Button>
                  </Grid.Column>
                )}
            </Grid.Row>
          </Grid>
        </Segment>
      </Segment>
    )
  }
}

AddProcessPipeline.propTypes = {
  changeBreadcrumbs: PropTypes.func.isRequired,
  createProcessPipeline: PropTypes.func.isRequired
}

export default withRouter(AddProcessPipeline)
