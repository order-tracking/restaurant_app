import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { changeBreadcrumbs } from '../../Layout/layoutReducer'
import { createProcessPipeline } from '../processesReducer'

import AddProcessPipeline from './AddProcessPipeline'

class AddProcessPipelinePage extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <AddProcessPipeline
        changeBreadcrumbs={this.props.actions.changeBreadcrumbs}
        createProcessPipeline={this.props.actions.createProcessPipeline}
      />
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      Object.assign(
        {},
        { changeBreadcrumbs },
        {
          createProcessPipeline
        }
      ),
      dispatch
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  AddProcessPipelinePage
)
