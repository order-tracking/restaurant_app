import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { List, Segment } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'
import PaginatedComponent from '../../../components/PaginatedComponent'
import * as Routes from '../routes'

class PaginatedProcessPipelines extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const { processPipelines } = this.props
    return (
      <Segment basic clearing>
        <PaginatedComponent
          hasNextPage={processPipelines.hasNextPage}
          nextLink={this.props.nextLink}
          hasPreviousPage={processPipelines.hasPreviousPage}
          previousLink={this.props.previousLink}
        >
          {processPipelines.items.map(pp => (
            <List.Item
              key={pp.id}
              className="pt10 pb10"
              as={NavLink}
              to={Routes.PROCESSPAGE.replace(':id', pp.id)}
            >
              <List.Content>
                <List.Header as="h2">{pp.name}</List.Header>
              </List.Content>
            </List.Item>
          ))}
        </PaginatedComponent>
      </Segment>
    )
  }
}

PaginatedProcessPipelines.propTypes = {
  processPipelines: PropTypes.object.isRequired,
  deleteProcessPipeline: PropTypes.func.isRequired,
  nextLink: PropTypes.string.isRequired,
  previousLink: PropTypes.string.isRequired
}

export default PaginatedProcessPipelines
