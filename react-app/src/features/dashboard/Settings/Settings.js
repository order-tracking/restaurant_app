import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Header, Icon, Segment } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import { toastr } from 'react-redux-toastr'
import ValidationField from '../../../components/validation/ValidationField'
import FormValidation from '../../../components/validation/FormValidation'
import { isNumber, min } from '../../../helpers/ValidationRules'
import { withRouter } from 'react-router'
import * as Routes from '../routes'

class Settings extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true
    }

    this.onSubmitCompany = this.onSubmitCompany.bind(this)
  }

  componentDidMount() {
    this.props.actions.changeBreadcrumbs([
      {
        key: 'Dashboard',
        content: 'Dashboard',
        as: Link,
        to: Routes.HOMEPAGE
      },
      { key: 'Settings', content: 'Settings', active: true }
    ])

    this.props.actions
      .getCompany(this.props.session.user.companyId)
      .then(this.setState({ isLoading: false }))
  }

  onSubmitCompany(company) {
    console.log(company)
    this.props.actions
      .editCompany({
        ...company,
        id: this.props.company.id
      })
      .then(_ =>
        toastr.success('Company successfully edited', '', {
          showCloseButton: false
        })
      )
      .catch(error => toastr.error(error.message))
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextProps.company !== this.props.company ||
      nextState.isLoading !== this.state.isLoading
    )
  }

  render() {
    const { isLoading } = this.state
    const { company } = this.props
    return (
      <Segment loading={isLoading}>
        <Segment basic>
          <Header as="h4">
            <Icon name="setting" />
            <Header.Content>Settings</Header.Content>
          </Header>
          <FormValidation onSubmit={this.onSubmitCompany}>
            <ValidationField
              name="name"
              value={company.name}
              label="Company Name"
              type="text"
              rules={[min(3)]}
            />
            <ValidationField
              name="address"
              value={company.address}
              label="Address"
              type="text"
              rules={[min(3)]}
            />
            <ValidationField
              name="postalCode"
              value={company.postalCode}
              label="Postal Code"
              type="text"
              rules={[min(4)]}
            />
            <ValidationField
              name="maxAllowedDistance"
              value={company.maxAllowedDistance}
              label="Maximum Distance (meters)"
              type="number"
              rules={[isNumber, min(2)]}
            />
            <Button type="submit">Save</Button>
          </FormValidation>
        </Segment>
      </Segment>
    )
  }
}

Settings.propTypes = {
  session: PropTypes.object.isRequired,
  company: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
}

export default withRouter(Settings)
