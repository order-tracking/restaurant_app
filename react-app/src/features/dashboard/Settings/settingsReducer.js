import * as helpers from '../../../helpers/ReduxHelpers'
import CompanyRepository from '../../../repositories/CompanyRepository'

/*
 |--------------------------------------------------------------------------
 | Types
 |--------------------------------------------------------------------------
 */
export const LOAD_COMPANY_REQUEST =
  'otrack/dashboard/settings/LOAD_COMPANY_REQUEST'
export const LOAD_COMPANY_SUCCESS =
  'otrack/dashboard/settings/LOAD_COMPANY_SUCCESS'
export const LOAD_COMPANY_FAILURE =
  'otrack/dashboard/settings/LOAD_COMPANY_FAILURE'

export const EDIT_COMPANY_REQUEST =
  'otrack/dashboard/settings/EDIT_COMPANY_REQUEST'
export const EDIT_COMPANY_SUCCESS =
  'otrack/dashboard/settings/EDIT_COMPANY_SUCCESS'
export const EDIT_COMPANY_FAILURE =
  'otrack/dashboard/settings/EDIT_COMPANY_FAILURE'

/*
 |--------------------------------------------------------------------------
 | Reducer
 |--------------------------------------------------------------------------
 */
const initialState = {}

export default helpers.createReducer(initialState, {
  [LOAD_COMPANY_SUCCESS]: (state, action) => {
    return action.response
  },
  [EDIT_COMPANY_SUCCESS]: (state, action) => {
    return state
  }
})

/*
 |--------------------------------------------------------------------------
 | Action Creators
 |--------------------------------------------------------------------------
 */
export function getCompany(id) {
  return {
    types: [LOAD_COMPANY_REQUEST, LOAD_COMPANY_SUCCESS, LOAD_COMPANY_FAILURE],
    callAPI: () => CompanyRepository.getCompany(id)
  }
}

export function editCompany(company) {
  return {
    types: [EDIT_COMPANY_REQUEST, EDIT_COMPANY_SUCCESS, EDIT_COMPANY_FAILURE],
    callAPI: () => CompanyRepository.editCompany(company)
  }
}
