import React, { Component } from 'react'
import Settings from './Settings'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { changeBreadcrumbs } from '../Layout/layoutReducer'
import { getCompany, editCompany } from './settingsReducer'

class SettingsPage extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <Settings
        session={this.props.session}
        company={this.props.company}
        actions={this.props.actions}
      />
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {
    session: state.session,
    company: state.company
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      {
        changeBreadcrumbs,
        getCompany,
        editCompany
      },
      dispatch
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsPage)
