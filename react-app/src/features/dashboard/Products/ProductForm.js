import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Form, Grid, Input, Segment } from 'semantic-ui-react'
import { toastr } from 'react-redux-toastr'

class ProductForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentProductTypeName: undefined,
      product: {
        productGroupId: undefined,
        name: undefined,
        processPipelineId: undefined,
        productTypes: []
      }
    }

    this.updateProductState = this.updateProductState.bind(this)
    this.updateCurrentProductType = this.updateCurrentProductType.bind(this)
    this.updateProductTypes = this.updateProductTypes.bind(this)
    this.addProductType = this.addProductType.bind(this)
    this.removeProductType = this.removeProductType.bind(this)
    this.createProduct = this.createProduct.bind(this)
  }

  updateProductState(event, { name, value }) {
    let product = this.state.product
    product[name] = value
    this.setState({ product })
  }

  updateCurrentProductType(event, { value }) {
    this.setState({ currentProductTypeName: value })
  }

  updateProductTypes(event, { name, value }) {
    let product = this.state.product
    product.productTypes[name] = value
    this.setState({ product })
  }

  addProductType() {
    let product = this.state.product
    product.productTypes.push({ name: this.state.currentProductTypeName })
    this.setState({ product, currentProductTypeName: '' })
  }

  removeProductType(event, { value }) {
    let { product } = this.state
    product.productTypes.splice(value, 1)
    this.setState({ product })
  }

  createProduct() {
    this.props
      .createProduct({
        ...this.state.product,
        productGroupId: this.props.productGroupId
      })
      .then(product => {
        toastr.success('Product Successfully Created', '', {
          showCloseButton: false
        })
        this.setState({
          currentProductTypeName: undefined,
          product: {
            productGroupId: undefined,
            name: '',
            pipelineId: undefined,
            productTypes: []
          }
        })
      })
      .catch(error => toastr.error(error))
  }

  render() {
    const { product } = this.state
    const { processPipelines } = this.props
    return (
      <Segment basic>
        <h1>Add a new product</h1>
        <Segment>
          <Form>
            <Form.Input
              required
              label="Name"
              placeholder="Name your product"
              name="name"
              value={this.state.product.name}
              onChange={this.updateProductState}
            />
            <Form.Select
              label="Process Pipeline"
              placeholder="Choose the Product's Pipeline"
              options={MapToOptions(processPipelines)}
              name="pipelineId"
              value={this.state.product.pipelineId}
              onChange={this.updateProductState}
            />
            <label>
              <b>Product Types</b>
            </label>
            {product.productTypes.map((pt, idx) => {
              return (
                <Form.Group key={idx} widths="equal">
                  <Form.Input
                    placeholder="Name of the type"
                    name={idx}
                    value={pt.name}
                    onChange={this.updateProductTypes}
                  />
                  <Button
                    type="button"
                    content="Remove"
                    value={idx}
                    onClick={this.removeProductType}
                  />
                </Form.Group>
              )
            })}
          </Form>

          <Grid columns={2}>
            <Grid.Column width={4}>
              <Input
                fluid
                placeholder="Add Types to your product"
                value={this.state.currentProductTypeName}
                onChange={this.updateCurrentProductType}
              />
            </Grid.Column>
            <Grid.Column>
              <Button
                disabled={!this.state.currentProductTypeName}
                onClick={this.addProductType}
                content="Add"
                primary
              />
            </Grid.Column>
          </Grid>
          <br />
          <Button onClick={this.createProduct}>Submit</Button>
        </Segment>
      </Segment>
    )
  }
}

function MapToOptions(processPipelines) {
  let toReturn = []
  processPipelines.items.forEach(pp => {
    toReturn.push({
      key: pp.id,
      text: pp.name,
      value: pp.id
    })
  })
  toReturn.push({
    key: 'None',
    text: 'None',
    value: undefined
  })
  return toReturn
}

ProductForm.propTypes = {
  productGroupId: PropTypes.number,
  processPipelines: PropTypes.object.isRequired,
  createProduct: PropTypes.func.isRequired
}

export default ProductForm
