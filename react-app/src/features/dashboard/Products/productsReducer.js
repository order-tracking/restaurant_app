import * as helpers from '../../../helpers/ReduxHelpers'
import ProductGroupRepository from '../../../repositories/ProductGroupsRepository'

/*
 |--------------------------------------------------------------------------
 | Types
 |--------------------------------------------------------------------------
 */
export const CREATE_PRODUCTGROUP_SUCCESS =
  'otrack/dashboard/products/CREATE_PRODUCTGROUP_SUCCESS'

export const CREATE_PRODUCT_SUCCESS =
  'otrack/dashboard/products/CREATE_PRODUCT_SUCCESS'

export const GET_PRODUCTGROUPS_SUCCESS =
  'otrack/dashboard/products/GET_PRODUCTGROUPS_SUCCESS'

export const GET_PRODUCTGROUP_SUCCESS =
  'otrack/dashboard/products/GET_PRODUCTGROUP_SUCCESS'

export const DELETE_PRODUCT_SUCCESS =
  'otrack/dashboard/products/DELETE_PRODUCT_SUCCESS'

export const DELETE_PRODUCTGROUP_SUCCESS =
  'otrack/dashboard/products/DELETE_PRODUCTGROUP_SUCCESS'

/*
 |--------------------------------------------------------------------------
 | Reducer
 |--------------------------------------------------------------------------
 */
const initialState = {
  id: undefined,
  name: undefined,
  nextProductGroups: [],
  products: []
}

export default helpers.createReducer(initialState, {
  [GET_PRODUCTGROUPS_SUCCESS]: (state, action) => {
    return {
      ...initialState,
      nextProductGroups: action.productGroups
    }
  },
  [GET_PRODUCTGROUP_SUCCESS]: (state, action) => {
    return action.productGroup
  },
  [CREATE_PRODUCTGROUP_SUCCESS]: (state, action) => {
    return {
      ...state,
      nextProductGroups: [...state.nextProductGroups, action.productGroup]
    }
  },
  [CREATE_PRODUCT_SUCCESS]: (state, action) => {
    return {
      ...state,
      products: [...state.products, action.product]
    }
  },
  [DELETE_PRODUCT_SUCCESS]: (state, action) => {
    return {
      ...state,
      products: state.products.filter(product => product.id !== action.id)
    }
  },
  [DELETE_PRODUCTGROUP_SUCCESS]: (state, action) => {
    return initialState
  }
})

/*
 |--------------------------------------------------------------------------
 | Action Creators
 |--------------------------------------------------------------------------
 */
export function createProductGroupSuccess(productGroup) {
  return { type: CREATE_PRODUCTGROUP_SUCCESS, productGroup }
}

export function createProductSuccess(product) {
  return { type: CREATE_PRODUCT_SUCCESS, product }
}

export function loadProductGroupSuccess(productGroup) {
  return { type: GET_PRODUCTGROUP_SUCCESS, productGroup }
}

export function loadProductGroupsSuccess(productGroups) {
  return { type: GET_PRODUCTGROUPS_SUCCESS, productGroups }
}

export function deleteProductSuccess(id) {
  return { type: DELETE_PRODUCT_SUCCESS, id }
}

export function deleteProductGroupSuccess(id) {
  return { type: DELETE_PRODUCTGROUP_SUCCESS, id }
}

export function getProductGroup(id) {
  return dispatch => {
    return ProductGroupRepository.getProductGroup(id).then(productGroup => {
      console.log(productGroup)
      dispatch(loadProductGroupSuccess(productGroup))
    })
  }
}

export function getFirstProductGroups() {
  return dispatch => {
    return ProductGroupRepository.getFirstProductGroups().then(
      productGroups => {
        console.log(productGroups)
        dispatch(loadProductGroupsSuccess(productGroups))
      }
    )
  }
}

export function createProduct(product) {
  return dispatch => {
    return ProductGroupRepository.createProduct(product).then(product => {
      console.log(product)
      dispatch(createProductSuccess(product))
      return product
    })
  }
}

export function createProductGroup(productGroup) {
  return dispatch => {
    return ProductGroupRepository.createProductGroup(
      productGroup
    ).then(productGroup => {
      console.log(productGroup)
      dispatch(createProductGroupSuccess(productGroup))
      return productGroup
    })
  }
}

export function deleteProduct(id) {
  return dispatch => {
    return ProductGroupRepository.deleteProduct(id).then(result => {
      dispatch(deleteProductSuccess(id))
    })
  }
}

export function deleteProductGroup(id) {
  return dispatch => {
    return ProductGroupRepository.deleteProductGroup(id).then(result => {
      dispatch(deleteProductGroupSuccess(id))
    })
  }
}
