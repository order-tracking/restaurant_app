import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { List, Button, Segment, Header } from 'semantic-ui-react'
import { toastr } from 'react-redux-toastr'
import ProductHelpers from '../../../helpers/ProductHelpers'

class ProductsList extends Component {
  constructor(props) {
    super(props)
    this.state = {}

    this.onDelete = this.onDelete.bind(this)
  }

  onDelete(id) {
    this.props
      .deleteProduct(id)
      .then(_ =>
        toastr.success('Product Successfully Deleted', '', {
          showCloseButton: false
        })
      )
      .catch(error =>
        toastr.error('Unable to delete.', 'Items associated with this Product')
      )
  }

  render() {
    const { products } = this.props
    if (products.length > 0) {
      return (
        <Segment basic>
          <Header as="h1">
            <Header.Content>Products</Header.Content>
          </Header>
          <List animated celled verticalAlign="middle" size="big">
            {products.map(product => (
              <List.Item key={product.id}>
                <List.Content floated="right">
                  <Button
                    onClick={() => {
                      this.onDelete(product.id)
                    }}
                  >
                    Delete
                  </Button>
                </List.Content>
                <List.Content>
                  <List.Header>
                    {product.name} ({ProductHelpers.typesToString(
                      product.productTypes
                    )})
                  </List.Header>
                </List.Content>
              </List.Item>
            ))}
          </List>
        </Segment>
      )
    } else return <div />
  }
}

ProductsList.propTypes = {
  products: PropTypes.array.isRequired,
  deleteProduct: PropTypes.func.isRequired
}

export default ProductsList
