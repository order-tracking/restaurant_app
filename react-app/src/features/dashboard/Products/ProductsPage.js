import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  createProduct,
  createProductGroup,
  deleteProduct,
  deleteProductGroup,
  getFirstProductGroups,
  getProductGroup,
  loadProductGroupsSuccess
} from './productsReducer'
import {
  createProcessPipeline,
  getProcessPipelines,
  loadProcessPipelinesSuccess
} from '../Processes/processesReducer'
import { changeBreadcrumbs } from '../Layout/layoutReducer'
import Products from './Products'

class ProductsPage extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <Products
        id={this.props.match.params.id}
        productGroup={this.props.productGroup}
        processPipelines={this.props.processPipelines}
        actions={this.props.actions}
      />
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {
    productGroup: state.productGroup,
    processPipelines: state.processPipelines.list
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      Object.assign(
        {},
        { changeBreadcrumbs },
        {
          createProductGroup,
          createProduct,
          getProductGroup,
          getFirstProductGroups,
          deleteProduct,
          deleteProductGroup,
          loadProductGroupsSuccess
        },
        {
          createProcessPipeline,
          getProcessPipelines,
          loadProcessPipelinesSuccess
        }
      ),
      dispatch
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductsPage)
