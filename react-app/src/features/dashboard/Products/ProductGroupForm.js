import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Segment } from 'semantic-ui-react'
import ValidationField from '../../../components/validation/ValidationField'
import FormValidation from '../../../components/validation/FormValidation'
import { min } from '../../../helpers/ValidationRules'
import { toastr } from 'react-redux-toastr'
import { withRouter } from 'react-router'

class ProductGroupForm extends Component {
  constructor(props) {
    super(props)
    this.state = {}

    this.createProductGroup = this.createProductGroup.bind(this)
  }

  createProductGroup(productGroup) {
    productGroup.previousProductGroupId = this.props.productGroupId
    console.log(productGroup)
    this.props
      .createProductGroup(productGroup)
      .then(product => {
        this.props.history.push('/dashboard/products/' + product.id)
        toastr.success('Product Group Successfully Created', '', {
          showCloseButton: false
        })
      })
      .catch(error => toastr.error(error))
  }

  render() {
    return (
      <Segment basic>
        <h1>Create a Product Group</h1>
        <Segment>
          <FormValidation onSubmit={this.createProductGroup}>
            <ValidationField
              name="name"
              label="Name"
              placeholder="Food, Beverage, Meat, Fish, etc."
              rules={[min(3)]}
            />
            <Button type="submit">Create</Button>
          </FormValidation>
        </Segment>
      </Segment>
    )
  }
}

ProductGroupForm.propTypes = {
  productGroupId: PropTypes.number,
  createProductGroup: PropTypes.func.isRequired
}

export default withRouter(ProductGroupForm)
