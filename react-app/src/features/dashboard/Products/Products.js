import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router'
import { Link, NavLink } from 'react-router-dom'
import { Button, Confirm, Header, Segment } from 'semantic-ui-react'
import { Helmet } from 'react-helmet'
import ProductForm from './ProductForm'
import ProductsList from './ProductsList'
import ProductGroupForm from './ProductGroupForm'
import { toastr } from 'react-redux-toastr'
import * as Routes from '../routes'

class Products extends Component {
  constructor(props) {
    super(props)
    this.state = {
      pgCreationEnabled: false,
      isLoading: true,
      open: false,
      breadCrumbs: [
        {
          key: 'Dashboard',
          content: 'Dashboard',
          as: Link,
          to: Routes.HOMEPAGE
        },
        { key: 'Products', content: 'Products', active: true }
      ]
    }

    this.show = () => this.setState({ open: true })
    this.handleCancel = this.handleCancel.bind(this)
    this.handleConfirm = this.handleConfirm.bind(this)
    this.add = () => this.setState({ pgCreationEnabled: true })
  }

  handleConfirm() {
    this.setState({ open: false })
    this.props.actions
      .deleteProductGroup(this.props.id)
      .then(_ => {
        toastr.success('Product Group Successfully Deleted', '', {
          showCloseButton: false
        })
        this.props.history.goBack()
      })
      .catch(error => toastr.error(error))
  }

  handleCancel() {
    this.setState({ open: false })
  }

  componentDidMount() {
    let promises = []
    if (this.props.id !== undefined) {
      promises.push(this.props.actions.getProductGroup(this.props.id))
    } else {
      promises.push(this.props.actions.getFirstProductGroups())
    }
    promises.push(this.props.actions.getProcessPipelines())

    Promise.all(promises).then(_ => this.setState({ isLoading: false }))

    this.props.actions.changeBreadcrumbs(this.state.breadCrumbs)
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.id !== nextProps.id) {
      this.setState({ pgCreationEnabled: false, isLoading: true })
      Promise.all([
        this.props.actions.getProductGroup(nextProps.id),
        this.props.actions.getProcessPipelines()
      ]).then(_ => this.setState({ isLoading: false }))
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextProps.productGroup !== this.props.productGroup ||
      nextState.isLoading !== this.state.isLoading ||
      nextState.pgCreationEnabled !== this.state.pgCreationEnabled ||
      nextState.open !== this.state.open
    )
  }

  render() {
    const { pgCreationEnabled } = this.state
    const { productGroup, processPipelines } = this.props
    return (
      <div>
        <Helmet title="Products" />
        <Header as="h2" attached="top" textAlign="center">
          {// Back Button
          productGroup.id && (
            <Button
              floated="left"
              onClick={this.props.history.goBack}
              color="orange"
            >
              Back
            </Button>
          )}
          {productGroup.name || 'Product Groups'}
          {// Delete Product Group
          productGroup.id && (
            <Button floated="right" onClick={this.show} color="red">
              Delete
            </Button>
          )}
        </Header>
        <Segment loading={this.state.isLoading} basic attached>
          {// Navigation Buttons to other Product Groups
          productGroup.products.length <= 0 &&
            productGroup.nextProductGroups.map(pg => (
              <Button
                as={NavLink}
                to={Routes.PRODUCTPAGE.replace(':id', pg.id)}
                key={pg.id}
                size="massive"
                primary
              >
                {pg.name}
              </Button>
            ))}
          {// Button to enable creation of Product Group
          productGroup.products.length <= 0 &&
            productGroup.nextProductGroups.length > 0 && (
              <Button onClick={this.add} size="massive" color="orange">
                Add
              </Button>
            )}
          {// Create Product Group
          productGroup.products.length <= 0 &&
            (pgCreationEnabled ||
              productGroup.nextProductGroups.length <= 0) && (
              <ProductGroupForm
                productGroupId={productGroup.id}
                createProductGroup={this.props.actions.createProductGroup}
              />
            )}
          {// Products List and Create a Product
          productGroup.nextProductGroups.length <= 0 && (
            <div>
              <ProductsList
                products={productGroup.products}
                deleteProduct={this.props.actions.deleteProduct}
              />

              <ProductForm
                productGroupId={productGroup.id}
                processPipelines={processPipelines}
                createProduct={this.props.actions.createProduct}
              />
            </div>
          )}
        </Segment>
        <Confirm
          open={this.state.open}
          content="Warning! This will delete the following Product Groups. Are you sure?"
          onCancel={this.handleCancel}
          onConfirm={this.handleConfirm}
        />
      </div>
    )
  }
}

Products.propTypes = {
  productGroup: PropTypes.object.isRequired,
  processPipelines: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
}

export default withRouter(Products)
