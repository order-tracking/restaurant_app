import HomePage from './Home/HomePage'
import OrdersPage from './Orders/OrdersPage'
import PipelinePage from './Pipeline/PipelinePage'
import ProductsPage from './Products/ProductsPage'
import ProcessesPage from './Processes/ProcessesPage'
import SingleProcessPage from './Processes/Details/SingleProcessPage'
import AddProcessPipelinePage from './Processes/Add/AddProcessPipelinePage'
import EmployeesPage from './Employees/EmployeesPage'
import SingleEmployeePage from './Employees/Edit/SingleEmployeePage'
import CustomersPage from './Customers/CustomersPage'
import SingleCustomerPage from './Customers/Edit/SingleCustomerPage'
import SettingsPage from './Settings/SettingsPage'
import * as roles from '../../models/Roles'
import OrderDetailsPage from './Orders/Details/OrderDetailsPage'
import CreateCustomerPage from './Customers/Add/CreateCustomerPage'
import CreateEmployeePage from './Employees/Add/CreateEmployeePage'
import JourneysPage from './Journeys/JourneysPage'
import CreateJourneyPage from './Journeys/Create/CreateJourneyPage'
import AssemblePage from './Assemble/AssemblePage'
import JourneyDetailsPage from './Journeys/Details/JourneyDetailsPage'
import ReportsPage from './Reports/ReportsPage'
import CampaignsPage from './Campaigns/CampaignsPage'
import AddCampaignPage from './Campaigns/Add/AddCampaignPage'
import ViewEmployeesPage from './Employees/View/ViewEmployeesPage'

const BASE = '/dashboard'

export const HOMEPAGE = BASE + '/home'

export const ORDERSPAGE = BASE + '/orders'
export const ORDERPAGE = BASE + '/orders/:id'

export const PIPELINESPAGE = BASE + '/pipeline'
export const PIPELINEPAGE = BASE + '/pipeline/:id'

export const ASSEMBLEPAGE = BASE + '/assemble'

export const JOURNEYSPAGE = BASE + '/journeys'
export const JOURNEYDETAILSPAGE = BASE + '/journeys/:id'
export const ADDJOURNEYPAGE = BASE + '/journeys/create'

export const PRODUCTSPAGE = BASE + '/products'
export const PRODUCTPAGE = BASE + '/products/:id'

export const PROCESSESPAGE = BASE + '/processes'
export const ADDPROCESSPAGE = BASE + '/processes/add'
export const PROCESSPAGE = BASE + '/processes/:id'

export const EMPLOYEESPAGE = BASE + '/employees'
export const ADDEMPLOYEEPAGE = BASE + '/employees/add'
export const EMPLOYEEPAGE = BASE + '/employees/:id'
export const VIEWEMPLOYEESPAGE = BASE + '/employees/view'

export const CUSTOMERSPAGE = BASE + '/customers'
export const ADDCUSTOMERPAGE = BASE + '/customers/add'
export const CUSTOMERPAGE = BASE + '/customers/:id'

export const CAMPAIGNSPAGE = BASE + '/campaigns'
export const ADDCAMPAIGNSPAGE = BASE + '/campaigns/add'

export const REPORTSPAGE = BASE + '/reports'

export const SETTINGSPAGE = BASE + '/settings'

export default [
  {
    path: HOMEPAGE,
    exact: true,
    component: HomePage,
    minRole: roles.EMPLOYEE,
    text: 'Home',
    icon: 'home',
    routes: []
  },
  {
    path: PIPELINESPAGE,
    exact: true,
    component: PipelinePage,
    minRole: roles.EMPLOYEE,
    text: 'Pipeline',
    icon: 'fork',
    routes: [
      {
        path: PIPELINEPAGE,
        component: PipelinePage,
        minRole: roles.EMPLOYEE,
        text: 'Pipeline',
        icon: 'fork'
      }
    ]
  },
  {
    path: ASSEMBLEPAGE,
    exact: true,
    component: AssemblePage,
    minRole: roles.EMPLOYEE,
    text: 'Assemble',
    icon: 'shopping basket',
    routes: []
  },
  {
    path: JOURNEYSPAGE,
    exact: true,
    component: JourneysPage,
    minRole: roles.EMPLOYEE,
    text: 'Journeys',
    icon: 'motorcycle',
    routes: [
      {
        path: ADDJOURNEYPAGE,
        component: CreateJourneyPage,
        minRole: roles.EMPLOYEE
      },
      {
        path: JOURNEYDETAILSPAGE,
        component: JourneyDetailsPage,
        minRole: roles.ADMIN
      }
    ]
  },
  {
    path: ORDERSPAGE,
    exact: true,
    component: OrdersPage,
    minRole: roles.EMPLOYEE,
    text: 'Orders',
    icon: 'ticket',
    routes: [
      {
        path: ORDERPAGE,
        component: OrderDetailsPage,
        minRole: roles.EMPLOYEE,
        text: 'Order',
        icon: 'ticket'
      }
    ]
  },
  {
    path: PRODUCTSPAGE,
    exact: true,
    component: ProductsPage,
    minRole: roles.EMPLOYEE,
    text: 'Products',
    icon: 'sitemap',
    routes: [
      {
        path: PRODUCTPAGE,
        component: ProductsPage,
        minRole: roles.EMPLOYEE,
        text: 'Products',
        icon: 'home'
      }
    ]
  },
  {
    path: PROCESSESPAGE,
    exact: true,
    component: ProcessesPage,
    minRole: roles.ADMIN,
    text: 'Processes',
    icon: 'road',
    routes: [
      {
        path: ADDPROCESSPAGE,
        component: AddProcessPipelinePage,
        minRole: roles.ADMIN,
        routes: []
      },
      {
        path: PROCESSPAGE,
        component: SingleProcessPage,
        minRole: roles.ADMIN,
        routes: []
      }
    ]
  },
  {
    path: EMPLOYEESPAGE,
    exact: true,
    component: EmployeesPage,
    minRole: roles.ADMIN,
    text: 'Employees',
    icon: 'users',
    routes: [
      {
        path: ADDEMPLOYEEPAGE,
        component: CreateEmployeePage,
        minRole: roles.ADMIN,
        routes: []
      },
      {
        path: VIEWEMPLOYEESPAGE,
        component: ViewEmployeesPage,
        minRole: roles.ADMIN,
        routes: []
      },
      {
        path: EMPLOYEEPAGE,
        component: SingleEmployeePage,
        minRole: roles.ADMIN,
        routes: []
      }
    ]
  },
  {
    path: CUSTOMERSPAGE,
    exact: true,
    component: CustomersPage,
    minRole: roles.ADMIN,
    text: 'Customers',
    icon: 'users',
    routes: [
      {
        path: ADDCUSTOMERPAGE,
        component: CreateCustomerPage,
        minRole: roles.ADMIN,
        routes: []
      },
      {
        path: CUSTOMERPAGE,
        component: SingleCustomerPage,
        minRole: roles.ADMIN,
        routes: []
      }
    ]
  },
  {
    path: CAMPAIGNSPAGE,
    exact: true,
    component: CampaignsPage,
    minRole: roles.ADMIN,
    text: 'Campaigns',
    icon: 'announcement',
    routes: [
      {
        path: ADDCAMPAIGNSPAGE,
        component: AddCampaignPage,
        minRole: roles.ADMIN,
        routes: []
      }
    ]
  },
  {
    path: REPORTSPAGE,
    component: ReportsPage,
    minRole: roles.ADMIN,
    text: 'Reports',
    icon: 'bar graph',
    routes: []
  },
  {
    path: SETTINGSPAGE,
    component: SettingsPage,
    minRole: roles.OWNER,
    text: 'Settings',
    icon: 'setting',
    routes: []
  }
]
