import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Customers from './Customers'
import { actions as customerActions } from './customersReducer'
import { changeBreadcrumbs } from '../Layout/layoutReducer'

class CustomersPage extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <Customers
        customers={this.props.customers}
        actions={this.props.actions}
      />
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {
    customers: state.customers
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      Object.assign({}, { changeBreadcrumbs }, customerActions),
      dispatch
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomersPage)
