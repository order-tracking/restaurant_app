import * as helpers from '../../../helpers/ReduxHelpers'
import CustomersRepository from '../../../repositories/CustomersRepository'

/*
 |--------------------------------------------------------------------------
 | Types
 |--------------------------------------------------------------------------
 */
export const GET_CUSTOMERS_SUCCESS =
  'otrack/dashboard/customers/GET_CUSTOMERS_SUCCESS'

export const GET_CUSTOMER_SUCCESS =
  'otrack/dashboard/customers/GET_CUSTOMER_SUCCESS'

export const CREATE_CUSTOMER_SUCCESS =
  'otrack/dashboard/customers/CREATE_CUSTOMER_SUCCESS'

export const DELETE_CUSTOMER_SUCCESS =
  'otrack/dashboard/customers/DELETE_CUSTOMER_SUCCESS'

export const EDIT_CUSTOMER_SUCCESS =
  'otrack/dashboard/customers/EDIT_CUSTOMER_SUCCESS'

/*
 |--------------------------------------------------------------------------
 | Reducer
 |--------------------------------------------------------------------------
 */
const initialState = {
  count: -1,
  limit: -1,
  page: -1,
  hasNextPage: false,
  hasPreviousPage: false,
  items: []
}

export default helpers.createReducer(initialState, {
  [GET_CUSTOMERS_SUCCESS]: (state, action) => {
    return action.customers
  },
  [GET_CUSTOMER_SUCCESS]: (state, action) => {
    let customer = state.items.find(
      customer => customer.id === action.customer.id
    )

    if (customer === undefined) {
      return { ...state, items: [...state.items, action.customer] }
    }
    return {
      ...state,
      items: helpers.updateItemInArray(
        state.items,
        customer.id,
        old => action.customer
      )
    }
  },
  [CREATE_CUSTOMER_SUCCESS]: (state, action) => {
    return { ...state, items: [...state.items, action.customer] }
  },
  [DELETE_CUSTOMER_SUCCESS]: (state, action) => {
    return {
      ...state,
      items: state.items.filter(customer => customer.id !== action.id)
    }
  },
  [EDIT_CUSTOMER_SUCCESS]: (state, action) => {
    console.log('Edit customer success!')
    return state
  }
})

/*
 |--------------------------------------------------------------------------
 | Action Creators
 |--------------------------------------------------------------------------
 */
const loadCustomersSuccess = customers => {
  return { type: GET_CUSTOMERS_SUCCESS, customers }
}

const getCustomerSuccess = customer => {
  return { type: GET_CUSTOMER_SUCCESS, customer }
}

const createCustomerSuccess = customer => {
  return { type: CREATE_CUSTOMER_SUCCESS, customer }
}

const editCustomerSuccess = id => {
  return { type: EDIT_CUSTOMER_SUCCESS, id }
}

const deleteCustomerSuccess = id => {
  return { type: DELETE_CUSTOMER_SUCCESS, id }
}

const getCustomers = (page = 1, limit = 5) => {
  return dispatch => {
    return CustomersRepository.getCustomersPaginated(
      page,
      limit
    ).then(customers => {
      console.log(customers)
      dispatch(loadCustomersSuccess(customers))
    })
  }
}

const createCustomer = customer => {
  return dispatch => {
    return CustomersRepository.createCustomer(customer).then(customer => {
      console.log(customer)
      dispatch(createCustomerSuccess(customer))
      return customer
    })
  }
}

const deleteCustomer = id => {
  return dispatch => {
    return CustomersRepository.deleteCustomer(id).then(result => {
      dispatch(deleteCustomerSuccess(id))
    })
  }
}

const getCustomer = id => {
  return dispatch => {
    return CustomersRepository.getCustomerById(id).then(customer => {
      console.log(customer)
      dispatch(getCustomerSuccess(customer))
      return customer
    })
  }
}

const editCustomer = customer => {
  return dispatch => {
    return CustomersRepository.editCustomer(customer).then(result => {
      dispatch(editCustomerSuccess(customer.id))
    })
  }
}

export const actions = {
  loadCustomersSuccess,
  createCustomerSuccess,
  deleteCustomerSuccess,
  getCustomerSuccess,
  getCustomers,
  createCustomer,
  deleteCustomer,
  getCustomer,
  editCustomer
}
