import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Divider, Grid, Segment } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import { toastr } from 'react-redux-toastr'
import { withRouter } from 'react-router'
import ValidationField from '../../../../components/validation/ValidationField'
import FormValidation from '../../../../components/validation/FormValidation'
import { isNumber, min } from '../../../../helpers/ValidationRules'
import * as Routes from '../../routes'
import ConfirmDeleteComponent from '../../../../components/ConfirmDeleteComponent'

class SingleCustomer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      customer: {
        id: undefined,
        name: undefined,
        phoneNumber: undefined
      }
    }

    this.updateCustomerState = this.updateCustomerState.bind(this)
    this.editCustomer = this.editCustomer.bind(this)
    this.deleteCustomer = this.deleteCustomer.bind(this)
  }

  componentDidMount() {
    this.props.changeBreadcrumbs([
      {
        key: 'Dashboard',
        content: 'Dashboard',
        as: Link,
        to: Routes.HOMEPAGE
      },
      {
        key: 'Customers',
        content: 'Customers',
        as: Link,
        to: Routes.CUSTOMERSPAGE
      },
      { key: 'Edit', content: 'Edit Customer', active: true }
    ])

    this.props.getCustomer(this.props.match.params.id).then(customer =>
      this.setState({
        isLoading: false,
        customer: customer
      })
    )
  }

  updateCustomerState(event, { name, value }) {
    let customer = this.state.customer
    customer[name] = value
    this.setState({ customer })
  }

  editCustomer(customer) {
    let theCustomer = {
      id: this.state.customer.id,
      name: customer.name,
      phoneNumber: customer.phoneNumber
    }
    this.props
      .editCustomer(theCustomer)
      .then(_ =>
        toastr.success('Customer Successfully Edited', '', {
          showCloseButton: false
        })
      )
      .catch(error => toastr.error(error))
  }

  deleteCustomer() {
    this.props
      .deleteCustomer(this.state.customer.id)
      .then(_ => {
        this.props.history.goBack()
        toastr.success('Customer Successfully Deleted', '', {
          showCloseButton: false
        })
      })
      .catch(error =>
        toastr.error('Unable to Delete Customer. Please try again')
      )
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextState.customer !== this.state.customer ||
      nextState.isLoading !== this.state.isLoading
    )
  }

  render() {
    const { customer, isLoading } = this.state
    return (
      <Segment loading={isLoading}>
        <Grid>
          <Grid.Row>
            <Grid.Column width={16}>
              <FormValidation onSubmit={this.editCustomer}>
                <ValidationField
                  name="name"
                  label="Name"
                  placeholder="Name"
                  type="text"
                  value={customer.name}
                  rules={[min(3)]}
                />
                <ValidationField
                  name="phoneNumber"
                  label="Phone number"
                  placeholder="Phone Number"
                  value={customer.phoneNumber}
                  rules={[isNumber, min(6)]}
                />
                <Divider hidden />
                <Button type="submit" floated="left">
                  Submit
                </Button>
                <ConfirmDeleteComponent
                  delete={this.deleteCustomer}
                  floated="right"
                  color="red"
                />
              </FormValidation>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
    )
  }
}

SingleCustomer.propTypes = {
  getCustomer: PropTypes.func.isRequired,
  changeBreadcrumbs: PropTypes.func.isRequired,
  editCustomer: PropTypes.func.isRequired,
  deleteCustomer: PropTypes.func.isRequired
}

export default withRouter(SingleCustomer)
