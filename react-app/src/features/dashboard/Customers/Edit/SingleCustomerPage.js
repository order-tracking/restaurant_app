import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import SingleCustomer from './SingleCustomer'
import { actions as customerActions } from '../customersReducer'
import { changeBreadcrumbs } from '../../Layout/layoutReducer'

class SingleCustomerPage extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <SingleCustomer
        getCustomer={this.props.actions.getCustomer}
        changeBreadcrumbs={this.props.actions.changeBreadcrumbs}
        editCustomer={this.props.actions.editCustomer}
        deleteCustomer={this.props.actions.deleteCustomer}
      />
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      Object.assign({}, { changeBreadcrumbs }, customerActions),
      dispatch
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SingleCustomerPage)
