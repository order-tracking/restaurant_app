import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Divider, Header, Icon, Segment } from 'semantic-ui-react'
import { Link, NavLink } from 'react-router-dom'
import CustomersPaginatedList from './CustomersPaginatedList'
import { withRouter } from 'react-router'
import { Helmet } from 'react-helmet'
import CenteredSegmentTextComponent from '../../../components/CenteredSegmentTextComponent'
import * as Routes from '../routes'
import * as UrlHelpers from '../../../helpers/UrlHelpers'

class Customers extends Component {
  constructor(props) {
    super(props)
    let params = new URLSearchParams(props.location.search)
    this.state = {
      isLoading: true,
      params: {
        page: parseInt(params.get('page'), 10) || 1,
        limit: parseInt(params.get('limit'), 10) || 5
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    let nextParams = new URLSearchParams(nextProps.location.search)
    let page = parseInt(nextParams.get('page'), 10)
    let limit = parseInt(nextParams.get('limit'), 10)
    if (
      (nextParams.get('page') && page !== this.state.params.page) ||
      (nextParams.get('limit') && limit !== this.state.params.limit)
    ) {
      this.setState({
        isLoading: true,
        params: {
          page: page || this.state.params.page,
          limit: limit || this.state.params.limit
        }
      })
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { page, limit } = this.state.params
    if (this.state.params !== prevState.params) {
      this.props.actions
        .getCustomers(page, limit)
        .then(_ => this.setState({ isLoading: false }))
    }
  }

  componentDidMount() {
    const { page, limit } = this.state.params
    this.props.actions.changeBreadcrumbs([
      {
        key: 'Dashboard',
        content: 'Dashboard',
        as: Link,
        to: Routes.HOMEPAGE
      },
      { key: 'Customers', content: 'Customers', active: true }
    ])

    this.props.actions
      .getCustomers(page, limit)
      .then(_ => this.setState({ isLoading: false }))
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextProps.customers !== this.props.customers ||
      nextState.isLoading !== this.state.isLoading
    )
  }

  render() {
    const { isLoading, params } = this.state
    return (
      <Segment loading={isLoading}>
        <Helmet title="Customers" />
        <Segment basic>
          <Header as="h4" floated="left">
            <Icon name="users" />
            <Header.Content>
              Customers
              <Header.Subheader>Company Customers</Header.Subheader>
            </Header.Content>
          </Header>
          <Button
            as={NavLink}
            to={Routes.ADDCUSTOMERPAGE}
            floated="right"
            content="Create a new Customer"
            icon="plus"
            labelPosition="left"
            primary
          />
        </Segment>
        <Divider clearing />
        {this.props.customers.count > 0 && (
          <CustomersPaginatedList
            customers={this.props.customers}
            nextLink={
              Routes.CUSTOMERSPAGE +
              '?' +
              UrlHelpers.getQueryString({
                ...params,
                page: params.page + 1
              })
            }
            previousLink={
              Routes.CUSTOMERSPAGE +
              '?' +
              UrlHelpers.getQueryString({
                ...params,
                page: params.page - 1
              })
            }
            deleteCustomer={this.props.actions.deleteCustomer}
          />
        )}
        {this.props.customers.count <= 0 && (
          <CenteredSegmentTextComponent text="No Customers yet" />
        )}
      </Segment>
    )
  }
}

Customers.propTypes = {
  customers: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
}

export default withRouter(Customers)
