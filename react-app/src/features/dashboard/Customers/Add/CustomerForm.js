import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Header, Icon, Segment } from 'semantic-ui-react'
import ValidationField from '../../../../components/validation/ValidationField'
import FormValidation from '../../../../components/validation/FormValidation'
import { isNumber, min } from '../../../../helpers/ValidationRules'

class CustomerForm extends Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.onSubmitCustomer = this.onSubmitCustomer.bind(this)
  }

  onSubmitCustomer(customer) {
    this.props.submitCustomer({
      Name: customer.firstName + ' ' + customer.lastName,
      PhoneNumber: customer.phoneNumber
    })
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.isLoading !== this.state.isLoading
  }

  render() {
    return (
      <Segment basic>
        <Header as="h4">
          <Icon name="add user" />
          <Header.Content>Create Customer</Header.Content>
        </Header>
        <FormValidation onSubmit={this.onSubmitCustomer}>
          <ValidationField
            name="firstName"
            label="First name"
            placeholder="First Name"
            type="text"
            rules={[min(3)]}
          />
          <ValidationField
            name="lastName"
            label="Last name"
            placeholder="Last Name"
            type="text"
            rules={[min(3)]}
          />
          <ValidationField
            name="phoneNumber"
            label="Phone number"
            placeholder="Phone Number"
            rules={[isNumber, min(6)]}
          />
          <Button type="submit">Create</Button>
        </FormValidation>
      </Segment>
    )
  }
}

CustomerForm.propTypes = {
  submitCustomer: PropTypes.func.isRequired
}

export default CustomerForm
