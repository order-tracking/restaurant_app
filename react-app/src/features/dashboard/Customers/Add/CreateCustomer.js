import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Segment } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import CustomerForm from './CustomerForm'
import * as Routes from '../../routes'
import { withRouter } from 'react-router'
import { toastr } from 'react-redux-toastr'

class CreateCustomer extends Component {
  constructor(props) {
    super(props)

    this.createCustomer = this.createCustomer.bind(this)
  }

  createCustomer(customer) {
    this.props.actions
      .createCustomer(customer)
      .then(customer => {
        toastr.success('Customer successfully created', '', {
          showCloseButton: false
        })
        this.props.history.push(Routes.CUSTOMERPAGE.replace(':id', customer.id))
      })
      .catch(error => toastr.error(error))
  }

  componentDidMount() {
    this.props.actions.changeBreadcrumbs([
      {
        key: 'Dashboard',
        content: 'Dashboard',
        as: Link,
        to: Routes.HOMEPAGE
      },
      {
        key: 'Customers',
        content: 'Customers',
        as: Link,
        to: Routes.CUSTOMERSPAGE
      },
      { key: 'Create', content: 'Create', active: true }
    ])
  }

  render() {
    return (
      <Segment>
        <CustomerForm submitCustomer={this.createCustomer} />
      </Segment>
    )
  }
}

CreateCustomer.propTypes = {
  actions: PropTypes.object.isRequired
}

export default withRouter(CreateCustomer)
