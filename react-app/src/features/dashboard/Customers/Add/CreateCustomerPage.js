import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { changeBreadcrumbs } from '../../Layout/layoutReducer'
import { actions as customerActions } from '../customersReducer'
import CreateCustomer from './CreateCustomer'

class CreateCustomerPage extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return <CreateCustomer actions={this.props.actions} />
  }
}

function mapStateToProps(state, ownProps) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      { changeBreadcrumbs, createCustomer: customerActions.createCustomer },
      dispatch
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateCustomerPage)
