import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { List, Segment } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'
import * as Routes from '../routes'
import PaginatedComponent from '../../../components/PaginatedComponent'

class CustomersPaginatedList extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const { customers } = this.props
    return (
      <Segment basic vertical clearing>
        <PaginatedComponent
          hasNextPage={customers.hasNextPage}
          nextLink={this.props.nextLink}
          hasPreviousPage={customers.hasPreviousPage}
          previousLink={this.props.previousLink}
        >
          {customers.items.map(customer => (
            <List.Item
              className="pt10 pb10"
              key={customer.id}
              as={NavLink}
              to={Routes.CUSTOMERPAGE.replace(':id', customer.id)}
            >
              <List.Icon
                name="user circle"
                size="large"
                verticalAlign="middle"
              />
              <List.Content>
                <List.Header>{customer.name}</List.Header>
                <List.Description>
                  Phone Number: {customer.phoneNumber}
                </List.Description>
              </List.Content>
            </List.Item>
          ))}
        </PaginatedComponent>
      </Segment>
    )
  }
}

CustomersPaginatedList.propTypes = {
  customers: PropTypes.object.isRequired,
  deleteCustomer: PropTypes.func.isRequired,
  nextLink: PropTypes.string.isRequired,
  previousLink: PropTypes.string.isRequired
}

export default CustomersPaginatedList
