import React from 'react'
import {
  Button,
  Form,
  Grid,
  Header,
  Icon,
  Input,
  Modal,
  Segment
} from 'semantic-ui-react'
import ProductList from './ProductList'
import SelectProductModal from '../SelectProduct/SelectProductModal'
import PropTypes from 'prop-types'
import {
  getHoursFormated,
  getTimeFromNow
} from '../../../../helpers/DateHelpers'

class ProductListComponent extends React.Component {
  state = {
    products: this.props.products,
    modal: false
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ products: nextProps.products })
  }

  onClickAdd = () => {
    this.refs.modal.openModal()
  }

  onFinish = estimatedTime => {
    this.props.onFinalProductList(this.state.products, estimatedTime)
  }

  /*
    |--------------------------------------------------------------------------
    | Add Products
    |--------------------------------------------------------------------------
    */
  addProduct = productEntry => {
    const exists = this.state.products.findIndex(p => {
      if (p.product.id !== productEntry.product.id) return false
      if (p.type === null && productEntry.type !== null) return false
      if (p.type !== null && productEntry.type === null) return false

      if (p.type) {
        return p.type.name === productEntry.type.name
      }

      return true
    })

    if (exists !== -1) {
      const prods = [...this.state.products]
      prods[exists].qty++

      this.setState({
        products: prods
      })
    } else {
      this.setState((prevState, props) => ({
        products: [productEntry, ...prevState.products]
      }))
    }
  }

  removeProduct = productId => {
    this.setState((prevState, props) => ({
      products: prevState.products.filter(p => p.product.id !== productId)
    }))
  }

  onProductChange = (idx, productEntry) => {
    const prods = [...this.state.products]
    prods[idx] = productEntry

    this.setState({
      products: prods
    })
  }

  onEstimatedTime = time => {
    if (time === -1) {
      this.setState({ modal: false })
      return
    }

    this.onFinish(time)

    this.setState({ modal: false })
  }

  onFinishClick = () => {
    this.setState({ modal: true })
  }

  render() {
    const { modal } = this.state
    const hasProducts = this.state.products.length > 0

    let duration = -1

    console.log('received address', this.props.address)
    if (this.props.address && this.props.address.duration) {
      duration = this.props.address.duration
    }

    return (
      <Segment
        clearing
        vertical
        className="no-padding-top no-padding-bottom mt10 mb10"
      >
        <Grid>
          <Grid.Row>
            <Grid.Column width={5}>
              <Header as="h3" color="blue">
                <Icon name="cart" />
                <Header.Content>
                  Items
                  <Header.Subheader>Order products</Header.Subheader>
                </Header.Content>
              </Header>
            </Grid.Column>
            <Grid.Column width={6} textAlign="center">
              {hasProducts && (
                <Button
                  content="Finish Order"
                  icon="send"
                  labelPosition="left"
                  color="green"
                  onClick={this.onFinishClick}
                />
              )}
            </Grid.Column>
            <Grid.Column width={5} textAlign="right">
              <Button
                content="Add Item"
                icon="add to cart"
                labelPosition="left"
                color="teal"
                onClick={this.onClickAdd}
              />
            </Grid.Column>
          </Grid.Row>

          <Grid.Row>
            <Grid.Column width={16}>
              <ProductList
                products={this.state.products}
                onProductChange={this.onProductChange}
                onProductRemove={this.removeProduct}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>

        <SelectProductModal ref="modal" onProduct={this.addProduct} />

        {modal && (
          <EstimatedTimeModal
            onComplete={this.onEstimatedTime}
            duration={duration}
          />
        )}
      </Segment>
    )
  }
}

ProductListComponent.propTypes = {
  onFinalProductList: PropTypes.func.isRequired,
  products: PropTypes.array.isRequired
}

/*
|--------------------------------------------------------------------------
| Estimated time Modal
|--------------------------------------------------------------------------
*/
class EstimatedTimeModal extends React.Component {
  state = {
    value: 10
  }

  closeModal = () => {
    this.props.onComplete(-1)
  }

  onChange = (e, { value }) => {
    if (value < 0) return
    this.setState({ value })
  }

  save = () => {
    if (this.state.value < 1) return

    this.props.onComplete(this.state.value)
  }

  onSubmit = e => {
    e.preventDefault()
  }

  render() {
    const { value } = this.state
    const { duration } = this.props

    let minutes = parseInt(value, 10)

    if (duration !== -1) {
      minutes += parseInt(duration, 10)
    }

    console.log(minutes)
    const estimated = getTimeFromNow(minutes)

    return (
      <Modal open={true} onClose={this.closeModal} size="small">
        <Modal.Header>What is the estimated time for this order?</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            {duration !== -1 && (
              <p>Travel estimated duration: {duration} minutes.</p>
            )}
            <p>Delivery time: {getHoursFormated(estimated)}</p>

            <Form onSubmit={this.onSubmit}>
              <Form.Field>
                <label>Order duration:</label>

                <Input
                  label={{ basic: true, content: 'minutes' }}
                  labelPosition="right"
                  name="estimated"
                  value={value}
                  onChange={this.onChange}
                  type="number"
                />
              </Form.Field>
            </Form>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button
            color="green"
            icon="save"
            content="Finish Order"
            onClick={this.save}
          />
        </Modal.Actions>
      </Modal>
    )
  }
}

EstimatedTimeModal.propTypes = {
  onComplete: PropTypes.func.isRequired
}

export default ProductListComponent
