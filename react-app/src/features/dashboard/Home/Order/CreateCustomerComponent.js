import React from 'react'
import PropTypes from 'prop-types'
import { Button, Header, Icon, Segment } from 'semantic-ui-react'
import ValidationField from '../../../../components/validation/ValidationField'
import FormValidation from '../../../../components/validation/FormValidation'
import { min } from '../../../../helpers/ValidationRules'
import CustomersRepository from '../../../../repositories/CustomersRepository'

/**
 * Should call onFinish prop with the customer object if created with success
 * or with the error
 */
class CreateCustomerComponent extends React.Component {
  state = {
    loading: false,
    error: null
  }

  handleSubmit = values => {
    // prevent another submit while submiting
    if (this.state.loading) return

    this.setState({ loading: true })

    CustomersRepository.createCustomer({
      Name: values.name,
      PhoneNumber: values.phoneNumber
    })
      .then(_ => CustomersRepository.getCustomerByPhone(values.phoneNumber))
      .then(customer => {
        this.setState({ loading: false, error: null })
        this.props.onFinish(customer)
      })
      .catch(error => {
        this.setState({ loading: false, error: error })
      })
  }

  render() {
    const phone = this.props.phone

    return (
      <Segment vertical loading={this.state.loading}>
        <Header as="h5" dividing color="blue">
          <Icon name="add user" />
          <Header.Content>Create a new customer</Header.Content>
        </Header>

        <FormValidation
          onSubmit={this.handleSubmit}
          customError={this.state.error}
        >
          <ValidationField name="name" label="Name" rules={[min(3)]} />
          <ValidationField
            name="phoneNumber"
            label="Phone"
            value={phone}
            rules={[min(9)]}
          />

          <Button type="submit">Create</Button>
        </FormValidation>
      </Segment>
    )
  }
}

CreateCustomerComponent.propTypes = {
  onFinish: PropTypes.func.isRequired
}

CreateCustomerComponent.defaultProps = {
  phone: ''
}

export default CreateCustomerComponent
