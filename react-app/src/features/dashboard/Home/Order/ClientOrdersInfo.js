import React from 'react'
import PropTypes from 'prop-types'
import { Button, Header, List, Segment } from 'semantic-ui-react'
import { dateNormal } from '../../../../helpers/DateHelpers'
import { itemsToString, mapLink } from '../../../../helpers/OrderHelpers'

class ClientOrdersInfo extends React.Component {
  state = {
    orders: this.props.orders
  }

  handleUseAddress = order => {
    this.props.onSelectOrder(order)
  }

  render() {
    let { orders } = this.state
    let nOrders = orders.length

    return (
      <Segment vertical className="mb10">
        <Header as="h5" dividing color="blue">
          <Header.Content style={{ width: '100%' }}>
            <strong>Last {nOrders} orders</strong>:{' '}
          </Header.Content>
        </Header>

        <List divided relaxed>
          {orders.map((order, i) => (
            <List.Item key={i}>
              <List.Content floated="right">
                <Button
                  color="blue"
                  content="Use"
                  icon="marker"
                  title="Use this address"
                  labelPosition="left"
                  onClick={() => this.handleUseAddress(order)}
                />

                <Button
                  as="a"
                  href={'orders/' + order.id}
                  target="_blank"
                  icon="external"
                  title="Open Order Details"
                />
                <Button
                  as="a"
                  href={mapLink(order)}
                  target="_blank"
                  icon="map"
                  title="Open Map"
                />
              </List.Content>

              <List.Content>
                <List.Header>{itemsToString(order)}</List.Header>
                <List.Description>
                  {order.address} <strong>at</strong>{' '}
                  {dateNormal(order.createdAt)}
                </List.Description>
              </List.Content>
            </List.Item>
          ))}
        </List>
      </Segment>
    )
  }
}

ClientOrdersInfo.propTypes = {
  orders: PropTypes.array.isRequired,
  onSelectOrder: PropTypes.func.isRequired
}

export default ClientOrdersInfo
