import React from 'react'
import PropTypes from 'prop-types'
import FindClientComponent from './FindClientComponent'
import ClientInfoArea from './ClientInfoArea'
import { toastr } from 'react-redux-toastr'

class StartOrderComponent extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      customer: null
    }

    this.setClient = this.setClient.bind(this)
    this.onComplete = this.onComplete.bind(this)
  }

  setClient(customer) {
    this.setState({ customer: customer })
  }

  onComplete() {
    toastr.success('Order Successfully Created', '', {
      showCloseButton: false
    })
    this.props.onComplete()
  }

  render() {
    const { customer } = this.state

    return (
      <div>
        {!customer && <FindClientComponent onClient={this.setClient} />}

        {customer && (
          <ClientInfoArea
            customer={customer}
            switchClient={this.setClient}
            onComplete={this.onComplete}
          />
        )}
      </div>
    )
  }
}

StartOrderComponent.propTypes = {
  onComplete: PropTypes.func.isRequired
}

export default StartOrderComponent
