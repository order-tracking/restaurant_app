import React from 'react'
import PropTypes from 'prop-types'
import { Button, Grid, Header, Icon, Label, Segment } from 'semantic-ui-react'
import ValidationField from '../../../../components/validation/ValidationField'
import FormValidation from '../../../../components/validation/FormValidation'
import { min } from '../../../../helpers/ValidationRules'
import OrdersRepository from '../../../../repositories/OrdersRepository'

//import { parseDistance } from '../../../../helpers/OrderHelpers'

class SelectAddressComponent extends React.Component {
  state = {
    loading: false,
    distance: 0,
    maxAllowed: 0,
    duration: 0
  }

  handleSubmit = values => {
    if (this.state.distance >= 0) {
      values.distance = this.state.distance
      values.duration = this.state.duration
    }

    this.props.onFinish(values)
  }

  handleCalculate = values => {
    this.setState({ loading: true })
    OrdersRepository.getAddressDistance(
      values.address,
      values.address2,
      values.postalCode
    )
      .then(res => {
        this.setState({
          distance: res.distance,
          maxAllowed: res.maxAllowedDistance,
          duration: res.duration,
          loading: false
        })
      })
      .catch(err => {
        this.setState({
          distance: -1,
          loading: false
        })
      })
  }

  handleCalculateClick = e => {
    e.preventDefault()
    this.refs.validation.triggerValidation(this.handleCalculate)
  }

  render() {
    const address = this.props.address
      ? this.props.address
      : {
          address: '',
          address2: '',
          postalCode: ''
        }

    const { distance, maxAllowed, loading, duration } = this.state

    return (
      <Segment vertical>
        <Header as="h5" dividing color="blue">
          <Icon name="map outline" />
          <Header.Content>Customer Address</Header.Content>
        </Header>

        <FormValidation ref="validation" onSubmit={this.handleSubmit}>
          <Grid>
            <Grid.Row>
              <Grid.Column>
                <ValidationField
                  name="address"
                  label="Address"
                  placeholder="Elm Street, 12"
                  value={address.address}
                  rules={[min(4)]}
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row columns="2">
              <Grid.Column>
                <ValidationField
                  name="address2"
                  required={false}
                  label="Address 2"
                  placeholder="1st floor"
                  value={address.address2}
                  rules={[min(2)]}
                />
              </Grid.Column>
              <Grid.Column>
                <ValidationField
                  name="postalCode"
                  label="Postal Code"
                  placeholder="1234-567"
                  value={address.postalCode}
                  rules={[min(4)]}
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row columns="2">
              <Grid.Column>
                <Button loading={loading} onClick={this.handleCalculateClick}>
                  Calculate Distance
                </Button>

                <DistanceComponent
                  distance={distance}
                  maxAllowed={maxAllowed}
                  duration={duration}
                />
              </Grid.Column>
              <Grid.Column textAlign="right">
                <Button type="submit" color="blue">
                  Use this address
                </Button>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </FormValidation>
      </Segment>
    )
  }
}

SelectAddressComponent.propTypes = {
  onFinish: PropTypes.func.isRequired,
  address: PropTypes.object
}

/*
|--------------------------------------------------------------------------
| Distance
|--------------------------------------------------------------------------
*/

const DistanceComponent = ({ distance, maxAllowed, duration }) => {
  const km = parseFloat(distance / 1000).toFixed(2)

  if (!distance) {
    return null
  }

  if (distance < 0) {
    return (
      <Label as="span" pointing="left">
        Error calculating the distance.
      </Label>
    )
  }

  if (distance <= maxAllowed) {
    return (
      <Label as="span" color="teal" pointing="left">
        Distance: {km} km, Minutes: {duration}
      </Label>
    )
  }

  return (
    <Label as="span" color="orange" pointing="left">
      Too far: {km} km, Minutes: {duration}
    </Label>
  )
}

export default SelectAddressComponent
