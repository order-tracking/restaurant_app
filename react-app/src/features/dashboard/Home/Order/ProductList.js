import React from 'react'
import {
  Button,
  Form,
  Image,
  Input,
  List,
  Modal,
  TextArea
} from 'semantic-ui-react'
import defaultProduct from '../../../../assets/img/default_product.jpg'
import PropTypes from 'prop-types'
import { strLimit } from '../../../../helpers/OrderHelpers'

class ProductList extends React.Component {
  state = {
    modal: -1
  }

  onChange = (e, { name, value }) => {
    if (value < 0) return

    const idx = parseInt(name, 10)

    const product = this.props.products[idx]
    product.qty = value

    this.props.onProductChange(idx, product)
  }

  addNote = idx => {
    this.setState({ modal: idx })
  }

  onNote = note => {
    if (note === null) {
      this.setState({ modal: -1 })
      return
    }

    const product = this.props.products[this.state.modal]
    product.note = note

    this.props.onProductChange(this.state.modal, product)

    this.setState({
      modal: -1
    })
  }

  render() {
    const { modal } = this.state
    const { products } = this.props
    const hasProducts = products.length > 0

    return (
      <div style={{ paddingLeft: '15px' }} className="mb20">
        {hasProducts && (
          <List divided verticalAlign="middle">
            {products.map((p, i) => (
              <List.Item key={i}>
                <List.Content floated="right">
                  <Input
                    label="Qty"
                    type="number"
                    name={i}
                    className="qtyProductInput"
                    value={p.qty}
                    onChange={this.onChange}
                    size="small"
                  />

                  <Button
                    icon="sticky note"
                    color="yellow"
                    content="Note"
                    labelPosition="left"
                    onClick={() => this.addNote(i)}
                  />
                  <Button
                    icon="remove"
                    content="Remove"
                    labelPosition="left"
                    onClick={() => this.props.onProductRemove(p.product.id)}
                  />
                </List.Content>
                <Image avatar src={imageSrc(p.product)} />
                <List.Content>
                  <List.Header as="h3">{p.product.name}</List.Header>
                  <List.Description>{productType(p)}</List.Description>
                </List.Content>
              </List.Item>
            ))}
          </List>
        )}

        {modal !== -1 && (
          <ProductNoteModal
            product={products[modal]}
            finishedNote={this.onNote}
          />
        )}
      </div>
    )
  }
}

ProductList.propTypes = {
  onProductChange: PropTypes.func.isRequired,
  onProductRemove: PropTypes.func.isRequired,
  products: PropTypes.array.isRequired
}

function imageSrc(product) {
  if (product.photo) {
    return product.photo
  }

  return defaultProduct
}

function productType(entry) {
  let qty = 'Qty: ' + entry.qty + ' '

  let note = ''
  if (entry.note && entry.note.length > 0) {
    note = ' Note: ' + strLimit(entry.note)
  }

  if (entry.type) {
    return qty + 'Type: ' + entry.type.name + note
  }

  return qty + note
}

/*
|--------------------------------------------------------------------------
| Modal
|--------------------------------------------------------------------------
*/
class ProductNoteModal extends React.Component {
  state = {
    modalValue: this.props.product.note ? this.props.product.note : ''
  }

  closeModal = () => {
    this.props.finishedNote(null)
  }

  onChangeTextArea = (e, { value }) => {
    this.setState({ modalValue: value })
  }

  saveNote = () => {
    this.props.finishedNote(this.state.modalValue)
  }

  onSubmit = e => {
    e.preventDefault()
  }

  render() {
    const { product } = this.props
    const { modalValue } = this.state
    return (
      <Modal open={true} onClose={this.closeModal}>
        <Modal.Header>
          Write a note about the item: {product.product.name}
        </Modal.Header>
        <Modal.Content image>
          <Image wrapped size="medium" src={defaultProduct} />
          <Modal.Description style={{ width: '100%' }}>
            <Form onSubmit={this.onSubmit}>
              <TextArea
                placeholder="Without cheese.."
                onChange={this.onChangeTextArea}
                value={modalValue}
              />
            </Form>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button content="Cancel" onClick={this.closeModal} icon="remove" />
          <Button
            color="green"
            icon="save"
            content="Save Note"
            onClick={this.saveNote}
          />
        </Modal.Actions>
      </Modal>
    )
  }
}

ProductNoteModal.propTypes = {
  product: PropTypes.object.isRequired,
  finishedNote: PropTypes.func.isRequired
}

export default ProductList
