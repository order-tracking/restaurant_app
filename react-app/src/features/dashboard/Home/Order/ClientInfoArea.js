import React from 'react'
import PropTypes from 'prop-types'
import { Button, Header, Icon, Segment } from 'semantic-ui-react'
import ClientOrdersInfo from './ClientOrdersInfo'
import SelectAddressComponent from './SelectAddressComponent'
import ProductListComponent from './ProductListComponent'
import OrdersRepository from '../../../../repositories/OrdersRepository'

class ClientInfoArea extends React.Component {
  state = {
    customer: this.props.customer,
    address: null,
    finalAddress: null,
    products: [],
    loading: false
  }

  switchClient = () => {
    this.props.switchClient(null)
  }

  switchAddress = () => {
    this.setState({
      finalAddress: null
    })
  }

  onAddressSelect = order => {
    this.setState({
      address: {
        address: order.address,
        address2: order.address2,
        postalCode: order.postalCode
      }
    })
  }

  onAddressEnter = values => {
    let address = {
      address: values.address,
      address2: values.address2,
      postalCode: values.postalCode,
      distance: values.distance ? values.distance : null,
      duration: values.distance ? values.duration : null
    }

    this.setState({
      finalAddress: address,
      address: address
    })
  }

  /*
  |--------------------------------------------------------------------------
  | End order
  |--------------------------------------------------------------------------
  */
  onEnd = (productEntries, estimatedTime) => {
    const { finalAddress, customer } = this.state
    OrdersRepository.createOrder(
      finalAddress,
      customer,
      productEntries,
      estimatedTime
    )
      .then(r => {
        this.props.onComplete()
        openInNewTab('/dashboard/orders/' + r.id)
      })
      .catch(e => {
        console.log('Error creating an order', e)
        this.setState({
          products: productEntries,
          loading: false
        })
      })
  }

  render() {
    let { customer, address, finalAddress, products, loading } = this.state

    return (
      <Segment
        loading={loading}
        vertical
        className="no-padding-top no-padding-bottom"
      >
        {!finalAddress && (
          <SelectAddressComponent
            onFinish={this.onAddressEnter}
            address={address}
          />
        )}

        {!finalAddress &&
          customer.orders.length > 0 && (
            <ClientOrdersInfo
              orders={customer.orders}
              onSelectOrder={this.onAddressSelect}
            />
          )}

        {finalAddress && (
          <ProductListComponent
            products={products}
            onFinalProductList={this.onEnd}
            address={finalAddress}
          />
        )}

        {finalAddress && (
          <AddressInfo address={finalAddress} onSwitch={this.switchAddress} />
        )}

        <CustomerInfo customer={customer} switchClient={this.switchClient} />
      </Segment>
    )
  }
}

ClientInfoArea.propTypes = {
  customer: PropTypes.object.isRequired,
  switchClient: PropTypes.func.isRequired,
  onComplete: PropTypes.func.isRequired
}

function openInNewTab(url) {
  var win = window.open(url, '_blank')
  win.focus()
}

/*
|--------------------------------------------------------------------------
| Customer info
|--------------------------------------------------------------------------
*/
const CustomerInfo = ({ customer, switchClient }) => {
  return (
    <Segment
      clearing
      vertical
      className="no-padding-top no-padding-bottom mt10"
    >
      <Header as="h3" color="blue" floated="left">
        <Icon name="user circle" />
        <Header.Content>
          {customer.name}
          <Header.Subheader>{customer.phoneNumber}</Header.Subheader>
        </Header.Content>
      </Header>
      <Header as="h5" floated="right" textAlign="right">
        <Button
          icon="exchange"
          content="Change"
          size="small"
          onClick={switchClient}
        />
      </Header>
    </Segment>
  )
}

/*
|--------------------------------------------------------------------------
| Address Info
|--------------------------------------------------------------------------
*/

const AddressInfo = ({ address, onSwitch }) => {
  let distance = null
  if (address.distance) {
    distance = parseFloat(address.distance / 1000).toFixed(2)
  }

  return (
    <Segment
      clearing
      vertical
      className="no-padding-top no-padding-bottom mt10 mb10"
    >
      <Header as="h3" color="orange" floated="left">
        <Icon name="map outline" />
        <Header.Content>
          {address.address}, {address.address2}
          {distance ? ' (' + address.postalCode + ')' : ''}
          <Header.Subheader>
            {distance
              ? 'Distance: ' + distance + ' km, Minutes: ' + address.duration
              : 'Postal code: ' + address.postalCode}
          </Header.Subheader>
        </Header.Content>
      </Header>
      <Header as="h5" floated="right" textAlign="right">
        <Button
          icon="exchange"
          content="Change"
          size="small"
          onClick={onSwitch}
        />
      </Header>
    </Segment>
  )
}

export default ClientInfoArea
