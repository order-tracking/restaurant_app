import React from 'react'
import PropTypes from 'prop-types'
import CustomersRepository from '../../../../repositories/CustomersRepository'
import { Button, Grid } from 'semantic-ui-react'
import FormValidation from '../../../../components/validation/FormValidation'
import ValidationField from '../../../../components/validation/ValidationField'
import { min, numbers } from '../../../../helpers/ValidationRules'

class SearchByPhoneInput extends React.PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      customerLoading: false,
      customer: null,
      hasResult: false,
      started: false
    }

    this.handleSubmit = this.handleSubmit.bind(this)
  }

  /*
  |--------------------------------------------------------------------------
  | Handle form and input events
  |--------------------------------------------------------------------------
  */
  handleSubmit(values) {
    if (this.state.customerLoading) return

    this.startSearch(values.phone)
  }

  /*
  |--------------------------------------------------------------------------
  | Search customer
  |--------------------------------------------------------------------------
  */
  startSearch(phone) {
    this.setState({ customerLoading: true, started: true })
    CustomersRepository.getCustomerByPhone(phone)
      .then(customer => {
        this.searchComplete(customer)
      })
      .catch(error => {
        this.searchComplete(phone)
      })
  }

  searchComplete(customer) {
    if (typeof customer === 'object') {
      this.setState({
        customerLoading: false,
        customer: customer,
        hasResult: true
      })

      this.props.onFinish(customer)
    } else {
      this.setState({
        customerLoading: false,
        hasResult: false
      })

      this.props.onFinish(customer) //pass the phone
    }
  }

  render() {
    const { customerLoading, started, hasResult } = this.state
    return (
      <Grid>
        <Grid.Row columns="2">
          <Grid.Column>
            <FormValidation onSubmit={this.handleSubmit}>
              <ValidationField
                placeholder="Search the customer by typing his phone number.."
                name="phone"
                type="number"
                label="Client Phone"
                rules={[numbers, min(9)]}
                action={<FindClientButton loading={customerLoading} />}
              />
            </FormValidation>
          </Grid.Column>

          <Grid.Column style={{ paddingTop: '30px' }}>
            {customerLoading && 'Searching for the customer..'}
            {started && !customerLoading && hasResult && 'Customer found..'}
            {started &&
              !customerLoading &&
              !hasResult &&
              'No customer found, create one now.'}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }
}

SearchByPhoneInput.propTypes = {
  onFinish: PropTypes.func.isRequired
}

const FindClientButton = ({ loading }) => {
  return (
    <Button
      loading={loading}
      content="Find"
      labelPosition="right"
      icon="user"
      type="submit"
    />
  )
}

export default SearchByPhoneInput
