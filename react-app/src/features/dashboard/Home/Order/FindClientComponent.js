import React from 'react'
import PropTypes from 'prop-types'
import CreateCustomerComponent from './CreateCustomerComponent'
import SearchByPhoneInput from './SearchByPhoneInput'

//import CustomersRepository from '../../../../repositories/CustomersRepository'

class FindClientComponent extends React.Component {
  state = {
    customer: null,
    searched: false,
    phone: ''
  }

  finishedSearch = customer => {
    if (typeof customer === 'object') {
      customer = this.parseCustomer(customer)
      this.setState({ customer: customer, searched: true })
      this.props.onClient(customer)
    } else {
      this.setState({ customer: null, phone: customer, searched: true })
    }
  }

  finishCreate = customer => {
    this.setState({ customer: this.parseCustomer(customer) })

    if (customer) {
      this.props.onClient(customer)
    }
  }

  parseCustomer(customer) {
    if (customer.orders && Array.isArray(customer.orders)) {
      customer.orders = customer.orders.slice(0, 5)
    } else {
      customer.orders = []
    }

    return customer
  }

  render() {
    let { customer, searched, phone } = this.state

    return (
      <div>
        {!customer && <SearchByPhoneInput onFinish={this.finishedSearch} />}

        {searched &&
          !customer && (
            <CreateCustomerComponent
              phone={phone}
              onFinish={this.finishCreate}
            />
          )}
      </div>
    )
  }
}

FindClientComponent.propTypes = {
  onClient: PropTypes.func.isRequired
}

export default FindClientComponent
