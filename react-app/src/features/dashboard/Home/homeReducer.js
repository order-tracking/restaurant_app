import * as helpers from '../../../helpers/ReduxHelpers'
import StatsRepository from '../../../repositories/StatsRepository'

/*
 |--------------------------------------------------------------------------
 | Types
 |--------------------------------------------------------------------------
 */
export const LOAD_STATS_REQUEST = 'otrack/dashboard/home/LOAD_STATS_REQUEST'
export const LOAD_STATS_SUCCESS = 'otrack/dashboard/home/LOAD_STATS_SUCCESS'
export const LOAD_STATS_FAILURE = 'otrack/dashboard/home/LOAD_STATS_FAILURE'

/*
 |--------------------------------------------------------------------------
 | Reducer
 |--------------------------------------------------------------------------
 */
const initialState = {
  stats: {
    isFetching: true,
    isCached: false,
    items: {
      pendingOrders: 0,
      distributorsWorking: 0,
      employeesWorking: 0,
      ordersCompletedToday: 0
    }
  }
}

export default helpers.createReducer(initialState, {
  [LOAD_STATS_REQUEST]: (state, action) => {
    return {
      ...state,
      stats: {
        ...state.stats,
        isFetching: true
      }
    }
  },
  [LOAD_STATS_SUCCESS]: (state, action) => {
    return {
      ...state,
      stats: {
        ...state.stats,
        isFetching: false,
        isCached: true,
        items: action.response
      }
    }
  },
  [LOAD_STATS_FAILURE]: (state, action) => {
    return {
      ...state,
      stats: {
        ...state.stats,
        isFetching: false,
        items: {}
      }
    }
  }
})

/*
|--------------------------------------------------------------------------
| Action Creators
|--------------------------------------------------------------------------
*/
export function loadStats() {
  return {
    types: [LOAD_STATS_REQUEST, LOAD_STATS_SUCCESS, LOAD_STATS_FAILURE],
    callAPI: () => StatsRepository.getDailyStats(),
    shouldCallAPI: state => state.home.stats.isCached === false
  }
}
