import React from 'react'
import PropTypes from 'prop-types'
import { Breadcrumb, Button, Grid, Segment } from 'semantic-ui-react'
import ProductGroupsRepository from '../../../../repositories/ProductGroupsRepository'
import ProductList from './ProductList'

class SelectProduct extends React.Component {
  state = {
    loading: true,
    rootGroups: [],
    groups: []
  }

  componentDidMount() {
    ProductGroupsRepository.getFirstProductGroups()
      .then(res => {
        console.log(res)
        this.setState({ rootGroups: res, loading: false })
      })
      .catch(e => {
        console.error(e)
        this.setState({ rootGroup: [], loading: false })
      })
  }

  onClick = id => {
    this.setState({ loading: true })
    ProductGroupsRepository.getProductGroup(id).then(res => {
      this.setState({ groups: [...this.state.groups, res], loading: false })
    })
  }

  back = () => {
    this.setState({ groups: this.state.groups.slice(0, -1) })
  }

  onSelect = product => {
    this.props.onProduct(product)
  }

  render() {
    const { loading, groups, rootGroups } = this.state
    const group = groups.length > 0 ? groups[groups.length - 1] : null
    const hasProducts = group ? group.products.length !== 0 : false
    const hasNextGroups = group ? group.nextProductGroups.length !== 0 : false

    const sections = groups.map(g => {
      return {
        key: g.id,
        content: g.name,
        link: false
      }
    })

    return (
      <Segment loading={loading} vertical>
        <Segment vertical>
          <Grid columns={3}>
            <Grid.Column>
              <Button
                color="orange"
                content="Back"
                onClick={this.back}
                disabled={groups.length === 0}
              />
            </Grid.Column>
            <Grid.Column textAlign="center">
              <Breadcrumb icon="right angle" sections={sections} />
            </Grid.Column>
            <Grid.Column />
          </Grid>
        </Segment>

        <Segment vertical>
          {!group &&
            rootGroups.map((g, i) => {
              return (
                <ButtonGroup
                  key={i}
                  name={g.name}
                  id={g.id}
                  onClick={this.onClick}
                />
              )
            })}

          {group &&
            !hasProducts &&
            group.nextProductGroups.map((g, i) => {
              return (
                <ButtonGroup
                  key={i}
                  name={g.name}
                  id={g.id}
                  onClick={this.onClick}
                />
              )
            })}

          {group &&
            hasProducts && (
              <ProductList productGroup={group} onSelect={this.onSelect} />
            )}

          {group &&
            !hasProducts &&
            !hasNextGroups && (
              <div>No more groups or products for this category yet.</div>
            )}
        </Segment>
      </Segment>
    )
  }
}

const ButtonGroup = ({ name, id, onClick }) => {
  return (
    <Button size="massive" primary content={name} onClick={() => onClick(id)} />
  )
}

SelectProduct.propTypes = {
  onProduct: PropTypes.func.isRequired
}

export default SelectProduct
