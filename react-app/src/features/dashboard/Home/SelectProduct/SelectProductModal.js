import React from 'react'
import PropTypes from 'prop-types'
import SelectProduct from './SelectProduct'
import { Modal } from 'semantic-ui-react'

class SelectProductModal extends React.Component {
  state = {
    open: false
  }

  openModal = () => {
    this.setState({ open: true })
  }

  closeModal = () => {
    this.setState({ open: false })
  }

  onProduct = product => {
    this.closeModal()
    this.props.onProduct(product)
  }

  render() {
    const { open } = this.state

    return (
      <div>
        <Modal open={open} onClose={this.closeModal}>
          <Modal.Content className="no-padding-top no-padding-bottom">
            <Modal.Description>
              <SelectProduct onProduct={this.onProduct} />
            </Modal.Description>
          </Modal.Content>
        </Modal>
      </div>
    )
  }
}

SelectProductModal.propTypes = {
  onProduct: PropTypes.func.isRequired
}

export default SelectProductModal
