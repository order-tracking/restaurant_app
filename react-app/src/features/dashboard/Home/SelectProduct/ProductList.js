import React from 'react'
import PropTypes from 'prop-types'
import { Button, Grid, Segment, Select } from 'semantic-ui-react'
import ProductsRepository from '../../../../repositories/ProductsRepository'
import ProductEntry from '../../../../models/ProductEntry'

class ProductList extends React.Component {
  render() {
    const { productGroup } = this.props

    return (
      <Segment vertical>
        {productGroup.products.map((product, i) => {
          return (
            <ProductItem
              key={i}
              id={product.id}
              onSelect={this.props.onSelect}
            />
          )
        })}
      </Segment>
    )
  }
}

ProductList.propTypes = {
  productGroup: PropTypes.object.isRequired,
  onSelect: PropTypes.func.isRequired
}

/*
|--------------------------------------------------------------------------
| Single Product
|--------------------------------------------------------------------------
*/
class ProductItem extends React.Component {
  state = {
    loading: true,
    product: null,
    types: [],
    choosed: null,
    error: false
  }

  cancel = false

  componentDidMount() {
    ProductsRepository.getProduct(this.props.id).then(res => {
      // fix race condition for then the component is destroyed before the
      // request is finished, was giving an error because of setState on a
      // non existent component
      if (!this.cancel) {
        this.setState({
          product: res,
          types: this.getTypes(res),
          loading: false
        })
      }
    })
  }

  componentWillUnmount() {
    this.cancel = true
  }

  onChange = (e, elem) => {
    this.setState({ choosed: elem.value, error: false })
  }

  onClick = () => {
    const { product, choosed } = this.state

    if (this.hasTypes()) {
      if (choosed) {
        this.props.onSelect(
          new ProductEntry(
            product,
            product.productTypes.find(p => p.id === choosed)
          )
        )
      } else {
        this.setState({ error: true })
      }
    } else {
      this.props.onSelect(new ProductEntry(product, null))
    }
  }

  getTypes = product => {
    let types = product.productTypes.map(t => {
      return {
        key: t.id,
        value: t.id,
        text: t.name
      }
    })

    return types
  }

  hasTypes = () => {
    return this.state.types.length !== 0
  }

  render() {
    const { product, loading, types, error } = this.state
    const hasTypes = this.hasTypes()

    return (
      <Segment vertical loading={loading} style={{ minHeight: '66px' }}>
        {product && (
          <Grid columns={3}>
            <Grid.Column>{product.name}</Grid.Column>
            <Grid.Column>
              {hasTypes && (
                <Select
                  error={error}
                  onChange={this.onChange}
                  placeholder="Select Type"
                  options={types}
                />
              )}
            </Grid.Column>
            <Grid.Column textAlign="right">
              <Button primary content="Select" onClick={this.onClick} />
            </Grid.Column>
          </Grid>
        )}
      </Segment>
    )
  }
}

ProductItem.propTypes = {
  id: PropTypes.number.isRequired,
  onSelect: PropTypes.func.isRequired
}

export default ProductList
