import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { toggleMenu, changeBreadcrumbs } from '../Layout/layoutReducer'
import { loadStats } from './homeReducer'
import Home from './Home'

const HomePage = ({ pageActions, stats, homeActions }) => {
  return (
    <Home stats={stats} homeActions={homeActions} pageActions={pageActions} />
  )
}

HomePage.propTypes = {
  pageActions: PropTypes.object.isRequired
}

const mapStateToProps = (state, ownProps) => {
  return {
    stats: state.home.stats
  }
}

const mapDispatchToProps = dispatch => {
  return {
    pageActions: bindActionCreators(
      { toggleMenu, changeBreadcrumbs },
      dispatch
    ),
    homeActions: bindActionCreators({ loadStats }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage)
