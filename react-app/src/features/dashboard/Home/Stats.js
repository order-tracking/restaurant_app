import React from 'react'
import PropTypes from 'prop-types'
import { Header, Icon, Segment, Statistic } from 'semantic-ui-react'

class Stats extends React.PureComponent {
  componentDidMount() {
    this.props.fetch()
  }

  render() {
    const { isFetching, items } = this.props

    return (
      <div>
        <Header as="h4">
          <Icon name="line chart" />
          <Header.Content>
            Statistics
            <Header.Subheader>Your stats for today</Header.Subheader>
          </Header.Content>
        </Header>
        <Segment loading={isFetching}>
          <Statistic.Group widths="four" size="tiny">
            <Statistic>
              <Statistic.Value>{items.pendingOrders}</Statistic.Value>
              <Statistic.Label>Pending Orders</Statistic.Label>
            </Statistic>

            <Statistic>
              <Statistic.Value>{items.distributorsWorking}</Statistic.Value>
              <Statistic.Label>Distributors Working</Statistic.Label>
            </Statistic>

            <Statistic>
              <Statistic.Value>{items.employeesWorking}</Statistic.Value>
              <Statistic.Label>Employees Working</Statistic.Label>
            </Statistic>

            <Statistic>
              <Statistic.Value>{items.ordersCompletedToday}</Statistic.Value>
              <Statistic.Label>Orders Completed</Statistic.Label>
            </Statistic>
          </Statistic.Group>
        </Segment>
      </div>
    )
  }
}

Stats.propTypes = {
  items: PropTypes.object.isRequired,
  isFetching: PropTypes.bool.isRequired,
  fetch: PropTypes.func.isRequired
}

export default Stats
