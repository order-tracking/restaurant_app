import React, { PureComponent } from 'react'
import { Header, Icon, Grid, Button, Segment } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Helmet } from 'react-helmet'
import Stats from './Stats'
import StartOrderComponent from './Order/StartOrderComponent'
import moment from 'moment'

class Home extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      orders: []
    }

    this.startOrder = this.startOrder.bind(this)
    this.handleRemoveOrder = this.handleRemoveOrder.bind(this)
  }

  componentDidMount() {
    this.props.pageActions.changeBreadcrumbs([
      {
        key: 'Dashboard',
        content: 'Dashboard',
        as: Link,
        to: '/dashboard/home'
      },
      { key: 'Home', content: 'Home', active: true }
    ])
    this.startOrder()
  }

  startOrder() {
    this.setState(prevState => {
      return {
        orders: [...prevState.orders, moment()]
      }
    })
  }

  handleRemoveOrder(index) {
    this.setState(prevState => {
      return {
        orders: prevState.orders.filter((_, i) => i !== index)
      }
    })
  }

  render() {
    return (
      <Grid>
        <Helmet title="Home" />

        <Grid.Row>
          <Grid.Column>
            <Stats
              {...this.props.stats}
              fetch={this.props.homeActions.loadStats}
            />
          </Grid.Column>
        </Grid.Row>

        <Grid.Row>
          <Grid.Column width="3">
            <Header as="h4">
              <Icon name="add to cart" />
              <Header.Content>
                New Order
                <Header.Subheader>Start a new Order</Header.Subheader>
              </Header.Content>
            </Header>
          </Grid.Column>
          <Grid.Column width="7">
            <Button
              onClick={this.startOrder}
              content="Create a new Order"
              icon="plus"
              labelPosition="left"
              primary
            />
          </Grid.Column>
        </Grid.Row>

        {this.state.orders.map((date, i) => (
          <SingleOrder
            key={i}
            id={i}
            removeHandle={this.handleRemoveOrder}
            date={date}
          />
        ))}
      </Grid>
    )
  }
}

class SingleOrder extends React.Component {
  constructor(props) {
    super(props)
    this._onClick = this._onClick.bind(this)
  }

  _onClick() {
    this.props.removeHandle(this.props.id)
  }

  render() {
    const { id, date } = this.props
    return (
      <Grid.Row>
        <Grid.Column>
          <Segment color="blue" clearing attached="top">
            <strong>Order</strong>
            : {id + 1} &nbsp;&nbsp;
            <strong>Started at:</strong>&nbsp;
            {date.format('HH[h]:mm[m]')}
            <Button
              floated="right"
              size="mini"
              icon="cancel"
              content="Cancel"
              onClick={this._onClick}
              labelPosition="left"
            />
          </Segment>
          <Segment attached>
            <StartOrderComponent onComplete={this._onClick} />
          </Segment>
        </Grid.Column>
      </Grid.Row>
    )
  }
}

SingleOrder.propTypes = {
  id: PropTypes.number.isRequired,
  removeHandle: PropTypes.func.isRequired,
  date: PropTypes.any.isRequired
}

Home.propTypes = {
  pageActions: PropTypes.object.isRequired,
  homeActions: PropTypes.object.isRequired,
  stats: PropTypes.object.isRequired
}

export default Home
