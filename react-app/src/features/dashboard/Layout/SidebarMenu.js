import React from 'react'
import { Sidebar, Menu, Icon } from 'semantic-ui-react'
import { NavLink, Route } from 'react-router-dom'
import logo from '../../../assets/img/otrack_vertical_white.svg'
import { withRouter } from 'react-router'
import PropTypes from 'prop-types'
import dashboardRoutes from '../routes'

const SidebarMenu = ({ session, location }) => {
  return (
    <Sidebar as={Menu} vertical visible icon inverted>
      <Menu.Item className="logo-container">
        <img src={logo} alt="oTrack" className="logo" />
      </Menu.Item>

      {session.isAuthenticated &&
        dashboardRoutes
          .filter(r => session.user.hasRole(r.minRole))
          .map((route, i) => (
            <Route
              key={i}
              path={route.path}
              children={({ match }) => (
                <Menu.Item
                  as={NavLink}
                  to={route.path}
                  exact={route.exact}
                  isActive={() => match}
                >
                  <Icon name={route.icon} />
                  <p>{route.text}</p>
                </Menu.Item>
              )}
            />
          ))}
    </Sidebar>
  )
}

SidebarMenu.propTypes = {
  session: PropTypes.object.isRequired
}

export default withRouter(SidebarMenu)
