import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { Segment } from 'semantic-ui-react'

import ReduxToastr from 'react-redux-toastr'
import Navbar from './Navbar'
import PageNotFoundComponent from '../../../components/PageNotFoundComponent'
import UnAuthorizedComponent from '../../../components/UnAuthorizedComponent'
import { Route, Switch, withRouter } from 'react-router-dom'
import PrivateRoute from '../../../components/PrivateRouteComponent'
import { toggleMenu, changeBreadcrumbs } from './layoutReducer'
import { requestLogout } from '../../auth/authReducer'
import dashboardRoutes from '../routes'

const MainContent = ({ session, collapsed, actions, sections, onLogout }) => {
  let routes = dashboardRoutes
  dashboardRoutes.forEach(item => {
    routes = routes.concat(item.routes)
  })
  return (
    <Segment basic>
      <Navbar
        collapsed={collapsed}
        triggerCollapse={actions.toggleMenu}
        sections={sections}
        session={session}
        onLogout={onLogout}
      />

      <Switch>
        {routes.map((route, i) => (
          <PrivateRoute
            key={i}
            path={route.path}
            exact={route.exact}
            component={route.component}
            session={session}
            minRole={route.minRole}
            routes={route.routes}
          />
        ))}
        <Route
          path="/dashboard/unauthorized"
          render={() => <UnAuthorizedComponent homeLink="/dashboard/home" />}
        />
        <Route
          render={() => <PageNotFoundComponent homeLink="/dashboard/home" />}
        />
      </Switch>
      <ReduxToastr
        timeOut={2500}
        newestOnTop={false}
        preventDuplicates
        position="bottom-right"
        transitionIn="fadeIn"
        transitionOut="fadeOut"
      />
    </Segment>
  )
}

MainContent.propTypes = {
  session: PropTypes.object.isRequired,
  onLogout: PropTypes.func.isRequired
}

const mapStateToProps = (state, ownProps) => {
  return {
    session: state.session,
    collapsed: state.page.menu_collapsed,
    sections: state.page.breadcrumbs
  }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ toggleMenu, changeBreadcrumbs }, dispatch),
    onLogout: () => dispatch(requestLogout())
  }
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(MainContent)
)
