import * as helpers from '../../../helpers/ReduxHelpers'

/*
 |--------------------------------------------------------------------------
 | Types
 |--------------------------------------------------------------------------
 */
export const COLLAPSE = 'otrack/dashboard/layout/COLLAPSE'
export const BREADCRUMBS = 'otrack/dashboard/layout/BREADCRUMBS'

/*
 |--------------------------------------------------------------------------
 | Reducer
 |--------------------------------------------------------------------------
 */
const initialState = {
  menu_collapsed: false,
  breadcrumbs: []
}

export default helpers.createReducer(initialState, {
  [COLLAPSE]: (state, action) => {
    return { ...state, menu_collapsed: action.collapsed }
  },
  [BREADCRUMBS]: (state, action) => {
    return { ...state, breadcrumbs: action.sections }
  }
})

/*
|--------------------------------------------------------------------------
| Action Creators
|--------------------------------------------------------------------------
*/
export function toggleMenu(collapse) {
  return { type: COLLAPSE, collapsed: collapse }
}

export function changeBreadcrumbs(sections) {
  return { type: BREADCRUMBS, sections: sections }
}
