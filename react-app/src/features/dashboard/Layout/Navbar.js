import React from 'react'
import { Menu, Button, Breadcrumb, Header, Icon } from 'semantic-ui-react'
import PropTypes from 'prop-types'

class Navbar extends React.PureComponent {
  constructor(props) {
    super(props)
    this.click = this.click.bind(this)
  }

  click() {
    this.props.triggerCollapse(!this.props.collapsed)
  }

  render() {
    const icon = this.props.collapsed ? 'arrow right' : 'arrow left'
    const title = this.props.collapsed ? 'Reveal Menu' : 'Hide Menu'
    const { session, sections, onLogout } = this.props

    return (
      <Menu className="top" secondary>
        <Menu.Item className="item-no-border" fitted>
          <Button basic onClick={this.click} icon={icon} title={title} />
        </Menu.Item>
        <Menu.Item className="item-no-border" fitted>
          <Breadcrumb icon="right angle" sections={sections} />
        </Menu.Item>
        <Menu.Menu position="right">
          <Menu.Item fitted>
            <Header as="h5">
              <Icon name="user circle" />
              {session.isAuthenticated
                ? session.user.name + ' (' + session.user.role + ')'
                : ''}
            </Header>
          </Menu.Item>
          <Menu.Item fitted>
            <Button
              onClick={onLogout}
              icon="sign out"
              content="Logout"
              title="Logout"
            />
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    )
  }
}

Navbar.propTypes = {
  collapsed: PropTypes.bool.isRequired,
  triggerCollapse: PropTypes.func.isRequired,
  sections: PropTypes.array.isRequired,
  session: PropTypes.object.isRequired,
  onLogout: PropTypes.func.isRequired
}

export default Navbar
