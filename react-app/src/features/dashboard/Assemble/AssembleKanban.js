import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { DragDropContext } from 'react-dnd'
import MultiBackend from 'react-dnd-multi-backend'
import { Grid, Segment } from 'semantic-ui-react'
import AssembleGridColumnComponent from './AssembleGridColumnComponent'
import * as DnDHelpers from '../../../helpers/DnDHelpers'

class AssembleKanban extends Component {
  constructor(props) {
    super(props)
    this.state = {
      verified: props.orders.items.filter(order => !order.readyToDistribute),
      distribute: props.orders.items.filter(order => order.readyToDistribute)
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      verified: nextProps.orders.items.filter(
        order => !order.readyToDistribute
      ),
      distribute: nextProps.orders.items.filter(
        order => order.readyToDistribute
      )
    })
  }

  render() {
    return (
      <Segment basic>
        <Grid stackable columns="equal">
          <AssembleGridColumnComponent
            name="Verficação"
            completed={false}
            orders={this.state.verified}
            editOrder={this.props.editOrder}
          />
          <AssembleGridColumnComponent
            name="Distribuição"
            completed={true}
            orders={this.state.distribute}
            editOrder={this.props.editOrder}
          />
        </Grid>
      </Segment>
    )
  }
}

AssembleKanban.propTypes = {
  orders: PropTypes.object.isRequired,
  editOrder: PropTypes.func.isRequired
}

export default DragDropContext(MultiBackend(DnDHelpers.getBackends()))(
  AssembleKanban
)
