import React, { Component } from 'react'
import { changeBreadcrumbs } from '../Layout/layoutReducer'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Assemble from './Assemble'
import { editOrder, getReadyOrders } from '../Orders/ordersReducer'

class AssemblePage extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <Assemble
        orders={this.props.orders}
        setBreadcrumbs={this.props.actions.changeBreadcrumbs}
        getReadyOrders={this.props.actions.getReadyOrders}
        editOrder={this.props.actions.editOrder}
      />
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {
    orders: state.orders
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      { changeBreadcrumbs, getReadyOrders, editOrder },
      dispatch
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AssemblePage)
