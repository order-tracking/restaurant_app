import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Divider, Grid } from 'semantic-ui-react'
import OrderGridRowComponent from './OrderGridRowComponent'
import { DropTarget } from 'react-dnd'
import * as DnDHelpers from '../../../helpers/DnDHelpers'

const orderTarget = {
  // Props returned by the drop result in DragSources
  drop(props) {
    return {
      completed: props.completed
    }
  }
}

class AssembleGridColumnComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const { connectDropTarget } = this.props
    return connectDropTarget(
      // react-dnd only allows native elements be passed.
      // className is equal to <Grid.Column textAlign="center" >
      <div className="center aligned column">
        <Grid.Row>
          {' '}
          <b>{this.props.name}</b>{' '}
        </Grid.Row>
        <Divider />
        {this.props.orders.map(order => {
          return (
            <OrderGridRowComponent
              key={order.id}
              order={order}
              editOrder={this.props.editOrder}
            />
          )
        })}
      </div>
    )
  }
}

AssembleGridColumnComponent.propTypes = {
  name: PropTypes.string.isRequired,
  orders: PropTypes.array.isRequired
}

export default DropTarget('Item', orderTarget, DnDHelpers.dropTargetCollect)(
  AssembleGridColumnComponent
)
