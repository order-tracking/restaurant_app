import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Header, Label, List, Segment } from 'semantic-ui-react'
import { DragSource } from 'react-dnd'
import { toastr } from 'react-redux-toastr'
import * as DnDHelpers from '../../../helpers/DnDHelpers'

const orderSource = {
  beginDrag(props) {
    return {
      order: props.order
    }
  },

  endDrag(props, monitor, component) {
    const dropResult = monitor.getDropResult()
    console.log(dropResult)
    console.log('PROPS', props)

    if (!dropResult || props.order.readyToDistribute === dropResult.completed) {
      return
    }
    let order = Object.assign({}, props.order)
    order.readyToDistribute = dropResult.completed

    props
      .editOrder(order)
      .then(_ => {
        if (dropResult.completed) {
          toastr.success('Order Ready to Distribute', '', {
            showCloseButton: false
          })
        }
      })
      .catch(error => toastr.error('Could not update. Try again'))
  }
}

class OrderGridRowComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const { connectDragSource, isDragging, order } = this.props
    return connectDragSource(
      <div
        style={{
          opacity: isDragging ? 0.5 : 1,
          fontSize: 25,
          fontWeight: 'bold',
          cursor: 'move'
        }}
      >
        <Segment raised>
          <Label attached="top" content={'Order ' + order.id} />
          <Header as="h5"> Customer: {order.customer.name} </Header>
          <List bulleted>
            {order.items.map(item => {
              return (
                <List.Item key={item.id}>
                  {item.quantity} {item.product.name}{' '}
                  {item.productType ? ': ' + item.productType.name + ' ' : ''}
                </List.Item>
              )
            })}
          </List>
        </Segment>
      </div>
    )
  }
}

OrderGridRowComponent.propTypes = {
  order: PropTypes.object.isRequired,
  editOrder: PropTypes.func.isRequired
}

export default DragSource('Item', orderSource, DnDHelpers.dragSourceCollect)(
  OrderGridRowComponent
)
