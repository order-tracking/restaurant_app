import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Divider, Header, Icon, Segment } from 'semantic-ui-react'
import { Link, NavLink } from 'react-router-dom'
import * as Routes from '../routes'
import { Helmet } from 'react-helmet'
import AssembleKanban from './AssembleKanban'
import CenteredSegmentTextComponent from '../../../components/CenteredSegmentTextComponent'

class Assemble extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true
    }
  }

  componentDidMount() {
    this.props.setBreadcrumbs([
      { key: 'Dashboard', content: 'Dashboard', as: Link, to: Routes.HOMEPAGE },
      { key: 'Assemble', content: 'Assemble', active: true }
    ])

    this.props.getReadyOrders().then(_ => this.setState({ isLoading: false }))

    this.intervalId = setInterval(this.props.getReadyOrders, 2000)
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      !this.props.orders !== nextProps.orders ||
      nextState.isLoading !== this.state.isLoading
    )
  }

  componentWillUnmount() {
    clearInterval(this.intervalId)
  }

  render() {
    const { isLoading } = this.state
    return (
      <Segment loading={isLoading}>
        <Helmet title="Assemble Orders" />
        <Header as="h4" floated="left">
          <Icon name="shopping basket" />
          <Header.Content>
            Assemble Orders
            <Header.Subheader>Orders Ready to be Assembled</Header.Subheader>
          </Header.Content>
        </Header>
        <Button
          as={NavLink}
          to={Routes.ADDJOURNEYPAGE}
          floated="right"
          content="Create a Journey"
          icon="plus"
          labelPosition="left"
          primary
        />
        <Divider clearing />
        {this.props.orders.count > 0 && (
          <AssembleKanban
            orders={this.props.orders}
            editOrder={this.props.editOrder}
          />
        )}
        {this.props.orders.count <= 0 && (
          <CenteredSegmentTextComponent text="No Orders Ready to Assemble yet" />
        )}
      </Segment>
    )
  }
}

Assemble.propTypes = {
  orders: PropTypes.object.isRequired,
  setBreadcrumbs: PropTypes.func.isRequired,
  getReadyOrders: PropTypes.func.isRequired,
  editOrder: PropTypes.func.isRequired
}

export default Assemble
