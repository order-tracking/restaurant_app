import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { changeBreadcrumbs } from '../../Layout/layoutReducer'
import { deleteOrder, getOrder } from '../ordersReducer'
import OrderDetails from './OrderDetails'

class OrderDetailsPage extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <OrderDetails
        getOrder={this.props.actions.getOrder}
        deleteOrder={this.props.actions.deleteOrder}
        setBreadcrumbs={this.props.actions.changeBreadcrumbs}
      />
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      { getOrder, deleteOrder, changeBreadcrumbs },
      dispatch
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderDetailsPage)
