import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router'
import { Divider, Header, Icon, List, Segment, Grid } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import * as Routes from '../../routes'
import { Helmet } from 'react-helmet'
import * as DateHelpers from '../../../../helpers/DateHelpers'
import HyperTrackComponent from '../../Journeys/Details/HyperTrackComponent'
import ConfirmDeleteComponent from '../../../../components/ConfirmDeleteComponent'
import { toastr } from 'react-redux-toastr'

class OrderDetails extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      order: {
        customer: {},
        items: []
      }
    }

    this.deleteOrder = this.deleteOrder.bind(this)
  }

  componentDidMount() {
    this.props.setBreadcrumbs([
      { key: 'Dashboard', content: 'Dashboard', as: Link, to: Routes.HOMEPAGE },
      { key: 'Orders', content: 'Orders', as: Link, to: Routes.ORDERSPAGE },
      { key: 'Order', content: 'Order Details', active: true }
    ])
    this.props.getOrder(this.props.match.params.id).then(order => {
      this.setState({ isLoading: false, order })
    })
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextProps.order !== this.props.order ||
      nextState.isLoading !== this.state.isLoading
    )
  }

  deleteOrder() {
    this.props
      .deleteOrder(this.props.match.params.id)
      .then(_ => {
        this.props.history.goBack()
        toastr.success('Order successfully deleted', '', {
          showCloseButton: false
        })
      })
      .catch(error => toastr.error(error))
  }

  render() {
    const { isLoading, order } = this.state
    return (
      <Segment loading={isLoading} clearing>
        <Helmet title="Order Info" />
        <Segment basic>
          <Header as="h4" floated="left">
            <Icon name="ticket" />
            <Header.Content>
              Order
              <Header.Subheader>Order Information</Header.Subheader>
            </Header.Content>
          </Header>
          <ConfirmDeleteComponent
            delete={this.deleteOrder}
            floated="right"
            color="red"
          />
          <Divider clearing />
          {/*Order Information: Address, Customer...*/}
          <Segment basic clearing>
            <Header as="h4" floated="left">
              <Icon name="user outline circle" />
              <Header.Content>
                {order.customer.name}
                <Header.Subheader>
                  Number: {order.customer.phoneNumber}
                </Header.Subheader>
              </Header.Content>
            </Header>
            <Header as="h4" floated="right">
              <Icon name="map" />
              <Header.Content>
                {order.address}
                <Header.Subheader>
                  Postal Code: {order.postalCode}
                </Header.Subheader>
              </Header.Content>
            </Header>
          </Segment>
          {/*Order Items*/}
          <Segment padded>
            <Header as="h4">
              <Icon name="food" />
              <Header.Content>
                Items
                <Header.Subheader>Order Items</Header.Subheader>
              </Header.Content>
            </Header>
            <List animated celled verticalAlign="middle">
              {order.items.map(item => (
                <List.Item key={item.id} className="pt10 pb10">
                  <List.Icon
                    size="large"
                    verticalAlign="middle"
                    name={item.completed ? 'checkmark' : 'attention'}
                  />
                  <List.Content>
                    <List.Header>
                      {item.product.name}{' '}
                      {item.productType
                        ? ': ' + item.productType.name + ' '
                        : ''}
                      {item.note ? '(' + item.note + ')' : ''}
                    </List.Header>
                    <List.Description>
                      Quantity: {item.quantity}
                    </List.Description>
                  </List.Content>
                </List.Item>
              ))}
            </List>
          </Segment>
          {/*Order Status*/}
          <Segment basic>
            <OrderStatusHeader order={order} />
          </Segment>
          {(order.deliveredAt || order.onGoing) && (
            <HyperTrackComponent order={order} height="600px" />
          )}
        </Segment>
      </Segment>
    )
  }
}

const OrderStatusHeader = ({ order }) => {
  const info = !order.readyToDistribute
    ? 'Items in Process'
    : !order.deliveredAt
      ? order.onGoing ? 'Order is On Going' : 'Ready to Distribute'
      : undefined

  return (
    <Grid columns={2}>
      <Grid.Column>
        <div>
          <Header as="h4" color="blue" floated="left">
            Created:{' '}
          </Header>
          <Header as="h4" floated="left">
            {DateHelpers.dateNormalWithHours(order.createdAt)}
          </Header>
        </div>
      </Grid.Column>
      <Grid.Column>
        <div>
          <Header as="h4" floated="right">
            {info ||
              'Completed at ' +
                DateHelpers.dateNormalWithHours(order.deliveredAt)}
          </Header>
          <Header as="h4" color="blue" floated="right">
            Status:{' '}
          </Header>
        </div>
      </Grid.Column>
    </Grid>
  )
}

OrderDetails.propTypes = {
  getOrder: PropTypes.func.isRequired,
  deleteOrder: PropTypes.func.isRequired,
  setBreadcrumbs: PropTypes.func.isRequired
}

export default withRouter(OrderDetails)
