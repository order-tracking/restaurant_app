import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Divider, Header, Icon, Segment } from 'semantic-ui-react'
import { Link, NavLink } from 'react-router-dom'
import { Helmet } from 'react-helmet'
import OrdersPaginatedList from './OrdersPaginatedList'
import { withRouter } from 'react-router'
import CenteredSegmentTextComponent from '../../../components/CenteredSegmentTextComponent'
import * as Routes from '../routes'
import * as UrlHelpers from '../../../helpers/UrlHelpers'

class Orders extends Component {
  constructor(props) {
    super(props)
    let params = new URLSearchParams(props.location.search)
    this.state = {
      isLoading: true,
      params: {
        page: parseInt(params.get('page'), 10) || 1,
        limit: parseInt(params.get('limit'), 10) || 10
      }
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextProps.orders !== this.props.orders ||
      nextState.isLoading !== this.state.isLoading
    )
  }

  componentWillReceiveProps(nextProps) {
    let nextParams = new URLSearchParams(nextProps.location.search)
    let page = parseInt(nextParams.get('page'), 10)
    let limit = parseInt(nextParams.get('limit'), 10)
    if (
      (nextParams.get('page') && page !== this.state.params.page) ||
      (nextParams.get('limit') && limit !== this.state.params.limit)
    ) {
      this.setState({
        isLoading: true,
        params: {
          page: page || this.state.params.page,
          limit: limit || this.state.params.limit
        }
      })
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { page, limit } = this.state.params
    if (this.state.params !== prevState.params) {
      this.props
        .getOrders(page, limit)
        .then(_ => this.setState({ isLoading: false }))
    }
  }

  componentDidMount() {
    const { page, limit } = this.state.params
    this.props.setBreadcrumbs([
      { key: 'Dashboard', content: 'Dashboard', as: Link, to: Routes.HOMEPAGE },
      { key: 'Orders', content: 'Orders', active: true }
    ])
    this.props
      .getOrders(page, limit)
      .then(_ => this.setState({ isLoading: false }))
  }

  //TODO: Dropdown to see incomplete orders
  //TODO: Search to see orders made by a specific customer?
  render() {
    const { isLoading, params } = this.state
    const { orders } = this.props
    return (
      <Segment loading={isLoading}>
        <Helmet title="Orders" />
        <Segment basic>
          <Header as="h4" floated="left">
            <Icon name="ticket" />
            <Header.Content>
              Orders
              <Header.Subheader>Company Orders</Header.Subheader>
            </Header.Content>
          </Header>
          <Button
            as={NavLink}
            to={Routes.HOMEPAGE}
            floated="right"
            content="Create an Order"
            icon="plus"
            labelPosition="left"
            primary
          />
        </Segment>
        <Divider clearing />
        {orders.count > 0 && (
          <OrdersPaginatedList
            orders={orders}
            nextLink={
              Routes.ORDERSPAGE +
              '?' +
              UrlHelpers.getQueryString({
                ...params,
                page: params.page + 1
              })
            }
            previousLink={
              Routes.ORDERSPAGE +
              '?' +
              UrlHelpers.getQueryString({
                ...params,
                page: params.page - 1
              })
            }
          />
        )}
        {orders.count <= 0 && (
          <CenteredSegmentTextComponent text="No Orders yet" />
        )}
      </Segment>
    )
  }
}

Orders.propTypes = {
  orders: PropTypes.object.isRequired,
  getOrders: PropTypes.func.isRequired,
  setBreadcrumbs: PropTypes.func.isRequired
}

export default withRouter(Orders)
