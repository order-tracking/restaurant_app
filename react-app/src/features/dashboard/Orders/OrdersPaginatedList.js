import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { List, Segment } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'
import * as Routes from '../routes'
import PaginatedComponent from '../../../components/PaginatedComponent'
import { getTimeFromNowText } from '../../../helpers/DateHelpers'

class OrdersPaginatedList extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const { orders } = this.props
    return (
      <Segment basic clearing>
        <PaginatedComponent
          hasNextPage={orders.hasNextPage}
          nextLink={this.props.nextLink}
          hasPreviousPage={orders.hasPreviousPage}
          previousLink={this.props.previousLink}
        >
          {orders.items.map((order, i) => (
            <List.Item
              key={i}
              className="pt10 pb10"
              as={NavLink}
              to={Routes.ORDERPAGE.replace(':id', order.id)}
            >
              <List.Icon
                size="large"
                name={
                  order.onGoing
                    ? 'motorcycle'
                    : order.deliveredAt ? 'checkmark' : 'hourglass empty'
                }
              />
              <List.Content>
                <List.Header>
                  ID: #{order.id} - Created{' '}
                  {getTimeFromNowText(order.createdAt)}
                </List.Header>
                <List.Description>
                  <strong>Customer:</strong> {order.customer.name} &nbsp;
                  <strong>Status:</strong>{' '}
                  {order.onGoing
                    ? 'In Progress'
                    : order.deliveredAt ? 'Delivered' : 'Pending'}
                </List.Description>
              </List.Content>
            </List.Item>
          ))}
        </PaginatedComponent>
      </Segment>
    )
  }
}

OrdersPaginatedList.propTypes = {
  orders: PropTypes.object.isRequired,
  nextLink: PropTypes.string.isRequired,
  previousLink: PropTypes.string.isRequired
}

export default OrdersPaginatedList
