import * as helpers from '../../../helpers/ReduxHelpers'
import OrdersRepository from '../../../repositories/OrdersRepository'

/*
 |--------------------------------------------------------------------------
 | Types
 |--------------------------------------------------------------------------
 */
export const GET_ORDERS_SUCCESS = 'otrack/dashboard/orders/GET_ORDERS_SUCCESS'

export const GET_ORDER_SUCCESS = 'otrack/dashboard/orders/GET_ORDER_SUCCESS'

export const EDIT_ORDER_SUCCESS = 'otrack/dashboard/orders/EDIT_ORDER_SUCCESS'

export const DELETE_ORDER_SUCCESS =
  'otrack/dashboard/orders/DELETE_ORDER_SUCCESS'

/*
 |--------------------------------------------------------------------------
 | Reducer
 |--------------------------------------------------------------------------
 */
const initialState = {
  count: -1,
  limit: -1,
  page: -1,
  hasNextPage: false,
  hasPreviousPage: false,
  items: []
}

export default helpers.createReducer(initialState, {
  [GET_ORDERS_SUCCESS]: (state, action) => {
    return action.orders
  },
  [GET_ORDER_SUCCESS]: (state, action) => {
    return {
      ...state,
      items: [...state.items, action.order]
    }
  },
  [EDIT_ORDER_SUCCESS]: (state, action) => {
    return {
      ...state,
      items: state.items.map(order => {
        if (order.id === action.order.id) {
          return action.order
        }
        return order
      })
    }
  },
  [DELETE_ORDER_SUCCESS]: (state, action) => {
    return {
      ...state,
      items: state.items.filter(order => order.id !== action.id)
    }
  }
})

/*
 |--------------------------------------------------------------------------
 | Action Creators
 |--------------------------------------------------------------------------
 */
export function loadOrdersSuccess(orders) {
  return { type: GET_ORDERS_SUCCESS, orders }
}

export function loadOrderSuccess(order) {
  return { type: GET_ORDER_SUCCESS, order }
}

export function editOrderSuccess(order) {
  return { type: EDIT_ORDER_SUCCESS, order }
}

export function deleteOrderSuccess(id) {
  return { type: DELETE_ORDER_SUCCESS, id }
}

export function getOrders(page, limit) {
  return dispatch => {
    return OrdersRepository.getOrders(page, limit).then(orders => {
      console.log(orders)
      dispatch(loadOrdersSuccess(orders))
    })
  }
}

export function getReadyOrders(page, limit) {
  return dispatch => {
    return OrdersRepository.getReadyOrders(page, limit).then(orders => {
      console.log(orders)
      dispatch(loadOrdersSuccess(orders))
    })
  }
}

export function getOrder(id) {
  return dispatch => {
    return OrdersRepository.getOrder(id).then(order => {
      console.log(order)
      dispatch(loadOrderSuccess(order))
      return order
    })
  }
}

export function editOrder(order) {
  return dispatch => {
    return OrdersRepository.editOrder(order).then(result => {
      console.log(result)
      dispatch(editOrderSuccess(order))
    })
  }
}

export function deleteOrder(id) {
  return dispatch => {
    return OrdersRepository.deleteOrder(id).then(_ => {
      dispatch(deleteOrderSuccess(id))
    })
  }
}
