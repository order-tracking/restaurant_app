import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { changeBreadcrumbs } from '../Layout/layoutReducer'
import { getOrders } from './ordersReducer'
import Orders from './Orders'

const OrdersPage = ({ orders, actions }) => {
  return (
    <Orders
      orders={orders}
      getOrders={actions.getOrders}
      setBreadcrumbs={actions.changeBreadcrumbs}
    />
  )
}

OrdersPage.propTypes = {
  orders: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
}

const mapStateToProps = (state, ownProps) => {
  return {
    orders: state.orders
  }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ changeBreadcrumbs, getOrders }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrdersPage)
