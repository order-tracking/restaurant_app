import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { debounce } from 'throttle-debounce'
import {
  Form,
  Divider,
  Header,
  Icon,
  Input,
  Segment,
  Statistic,
  Grid
} from 'semantic-ui-react'
import { Helmet } from 'react-helmet'
import * as Routes from '../routes'
import { Link } from 'react-router-dom'
import ReportsRepository from '../../../repositories/ReportsRepository'
import { withRouter } from 'react-router'

class Reports extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      days: 10,
      reports: {
        totalOrders: 0,
        totalJourneys: 0,
        totalEmployees: 0,
        totalCustomers: 0,
        totalProducts: 0,
        totalPipelines: 0,
        totalProcesses: 0,
        totalFeedbacks: 0,
        totalCampaigns: 0
      }
    }

    this.changeDays = this.changeDays.bind(this)
    this.getReports = debounce(500, this.getReports)
  }

  componentDidMount() {
    this.props.setBreadcrumbs([
      {
        key: 'Dashboard',
        content: 'Dashboard',
        as: Link,
        to: Routes.HOMEPAGE
      },
      { key: 'Reports', content: 'Reports', active: true }
    ])

    const { days } = this.state
    ReportsRepository.getReports(days).then(reports => {
      console.log(reports)
      this.setState({ isLoading: false, reports })
    })
  }

  changeDays(event, { value }) {
    if (value === '' || value < 1) {
      value = 1
    }

    this.setState({ days: value })
    this.getReports()
  }

  getReports() {
    this.setState({ isLoading: true })
    ReportsRepository.getReports(this.state.days).then(reports => {
      console.log(reports)
      this.setState({ isLoading: false, reports })
    })
  }

  render() {
    const { isLoading, reports } = this.state
    return (
      <Segment>
        <Helmet title="Reports" />
        <Grid stackable columns={2}>
          <Grid.Column width={4}>
            <Header as="h4" floated="left">
              <Icon name="bar graph" />
              <Header.Content>
                Reports
                <Header.Subheader>Company Statistics</Header.Subheader>
              </Header.Content>
            </Header>
          </Grid.Column>
          <Grid.Column width={12}>
            <Form.Field inline>
              <label>
                <b>Search by Days</b>
              </label>
              <Input
                type="number"
                fluid
                value={this.state.days}
                onChange={this.changeDays}
              />
            </Form.Field>
          </Grid.Column>
        </Grid>
        <Divider clearing />
        <Segment basic textAlign="center" loading={isLoading}>
          <Header as="h4" textAlign="left">
            <Icon name="food" />
            <Header.Content>Company Work made</Header.Content>
          </Header>
          <Statistic>
            <Statistic.Value>
              {/*<Icon name='ticket' />*/}
              {reports.totalOrders}
            </Statistic.Value>
            <Statistic.Label>Orders made</Statistic.Label>
          </Statistic>
          <Statistic>
            <Statistic.Value>
              {/*<Icon name='motorcycle' />*/}
              {reports.totalJourneys}
            </Statistic.Value>
            <Statistic.Label>Journeys made</Statistic.Label>
          </Statistic>
          <Divider />
          <Header as="h4" textAlign="left">
            <Icon name="talk" />
            <Header.Content>Customer Communication</Header.Content>
          </Header>
          <Statistic value={reports.totalFeedbacks} label="Feedbacks given" />
          <Statistic value={reports.totalCampaigns} label="Campaigns made" />
          <Divider />
          <Header as="h4" textAlign="left">
            <Icon name="users" />
            <Header.Content>Company Users</Header.Content>
          </Header>
          <Statistic value={reports.totalEmployees} label="Total Employees" />
          <Statistic value={reports.totalCustomers} label="Total Customers" />
          <Divider />
          <Header as="h4" textAlign="left">
            <Icon name="settings" />
            <Header.Content>Company Configurations</Header.Content>
          </Header>
          <Statistic
            value={reports.totalPipelines}
            label="Total Pipelines created"
          />
          <Statistic
            value={reports.totalProcesses}
            label="Total Processes created"
          />
          <Statistic
            value={reports.totalProducts}
            label="Total Products created"
          />
        </Segment>
      </Segment>
    )
  }
}

Reports.propTypes = {
  setBreadcrumbs: PropTypes.func.isRequired
}

export default withRouter(Reports)
