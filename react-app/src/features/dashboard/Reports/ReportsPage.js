import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { changeBreadcrumbs } from '../Layout/layoutReducer'
import { connect } from 'react-redux'
import Reports from './Reports'

class ReportsPage extends Component {
  render() {
    return <Reports setBreadcrumbs={this.props.actions.changeBreadcrumbs} />
  }
}

function mapStateToProps(state, ownProps) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ changeBreadcrumbs }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReportsPage)
