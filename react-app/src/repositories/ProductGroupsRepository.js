import axios from 'axios'

class ProductGroupsRepository {
  static getFirstProductGroups() {
    return axios.get('/productgroups/first')
  }

  static getProductGroup(id) {
    return axios('/productgroups/' + id)
  }

  static createProductGroup(productGroup) {
    return axios.post('/productgroups', productGroup)
  }

  static createProduct(product) {
    return axios.post('/products', product)
  }

  static deleteProductGroup(id) {
    return axios.delete('/productgroups/' + id)
  }

  static deleteProduct(id) {
    return axios.delete('/products/' + id)
  }
}

export default ProductGroupsRepository
