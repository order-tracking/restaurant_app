import axios from 'axios'

class CampaignsRepository {
  static getCampaigns() {
    return axios('/campaigns')
  }

  static create(campaigns) {
    return axios.post('/campaigns', campaigns)
  }
}

export default CampaignsRepository
