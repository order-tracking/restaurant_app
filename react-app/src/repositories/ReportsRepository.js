import axios from 'axios'

class ReportsRepository {
  static getReports(days) {
    return axios('/reports', { params: { days: days } })
  }
}

export default ReportsRepository
