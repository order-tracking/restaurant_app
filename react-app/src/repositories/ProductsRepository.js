import axios from 'axios'

const BASE = '/products'

class ProductsRepository {
  static getProduct(id) {
    return axios(`${BASE}/${id}`)
  }
}

export default ProductsRepository
