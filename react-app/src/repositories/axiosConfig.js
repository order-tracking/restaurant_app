import axios from 'axios'

/**
 * Axios default config for every request
 * Loaded in index.js
 *
 * More info:
 * https://github.com/mzabriskie/axios
 */
axios.defaults.baseURL = process.env.REACT_APP_API_URL
//axios.defaults.timeout = 5000
axios.defaults.responseType = 'json'
axios.defaults.headers.post['Content-Type'] = 'application/json'

axios.interceptors.request.use(config => {
  return window.UserManager.getUser().then(user => {
    if (user) {
      config.headers.common['Authorization'] = 'Bearer ' + user.access_token
    }

    return config
  })
})

//  return the body of the response by default
axios.interceptors.response.use(
  resp => {
    if (
      (resp.status > 300 && resp.status < 399) || // in case of a redirect
      [204].includes(resp.status) // created or no content
    ) {
      return resp // return the full response
    } else {
      return resp.data // only the body
    }
  },
  error => {
    console.error('Axios error response:', error.response)

    try {
      // return the error field from json
      if (error.response.data && typeof error.response.data === 'object') {
        let tmp = error.response.data
        return Promise.reject(tmp[Object.keys(tmp)[0]][0])
      }
    } catch (err) {
      console.error(err)
    }

    return Promise.reject(error)
  }
)
