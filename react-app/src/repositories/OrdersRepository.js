import axios from 'axios'

const BASE = '/orders'

class OrdersRepository {
  static getOrders(page, limit) {
    return axios.get(BASE, { params: { page: page, limit: limit } })
  }

  static getReadyOrders(page, limit) {
    return axios.get(BASE + '/ready', { params: { page: page, limit: limit } })
  }

  static getOrder(orderId) {
    return axios.get(`${BASE}/${orderId}`)
  }

  static editOrder(order) {
    return axios.put(`${BASE}/${order.id}`, order)
  }

  static deleteOrder(id) {
    return axios.delete(`${BASE}/${id}`)
  }

  static createOrder(addressInfo, customerInfo, productEntries, estimatedTime) {
    return axios.post(`${BASE}`, {
      EstimatedTime: estimatedTime,
      CustomerId: customerInfo.id,
      Address: addressInfo.address,
      Address2: addressInfo.address2,
      PostalCode: addressInfo.postalCode,
      Items: productEntries.map(p => {
        let item = {
          Note: p.note,
          Quantity: p.qty,
          ProductId: p.product.id
        }

        if (p.type) {
          item.ProductTypeId = p.type.id
        }

        return item
      })
    })
  }

  static editItem(item) {
    return axios.put(`${BASE}/items/${item.id}`, item)
  }

  static getAddressDistance(address, address2, postalCode) {
    return axios.post(`${BASE}/distance`, {
      Address: address,
      Address2: address2,
      PostalCode: postalCode
    })
  }
}

export default OrdersRepository
