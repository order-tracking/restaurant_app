import axios from 'axios'

class EmployeesRepository {
  static getEmployees(page, limit) {
    return axios('/employees', { params: { page: page, limit: limit } })
  }

  static getEmployee(id) {
    return axios('/employees/' + id)
  }

  static createEmployee(employee) {
    return axios.post('/employees', employee)
  }

  static editEmployee(employee) {
    return axios.put('/employees/' + employee.id, employee)
  }

  static deleteEmployee(id) {
    return axios.delete('/employees/' + id)
  }
}

export default EmployeesRepository
