import axios from 'axios'

class JourneysRepository {
  static getJourneys(page, limit, search) {
    return axios('/journeys', {
      params: { page: page, limit: limit, search: search }
    })
  }

  static getJourney(id) {
    return axios('/journeys/' + id)
  }

  static createJourney(journey) {
    return axios.post('/journeys', journey)
  }

  static editJourney(journey) {
    return axios.put('/journeys/' + journey.id, journey)
  }

  static deleteJourney(id) {
    return axios.delete('/journeys/' + id)
  }
}

export default JourneysRepository
