import axios from 'axios'

const BASE = '/customers'

class CustomersRepository {
  static getCustomersPaginated(page, limit) {
    return axios(BASE, { params: { page: page, limit: limit } })
  }

  static getCustomerById(id) {
    return axios.get(`${BASE}/${id}`)
  }

  static getCustomerByPhone(phone) {
    return axios.get(`${BASE}/phoneNumber/${phone}`)
  }

  static createCustomer(client) {
    return axios.post(BASE, client)
  }

  static editCustomer(client) {
    return axios.put(`${BASE}/${client.id}`, client)
  }

  static deleteCustomer(id) {
    return axios.delete(`${BASE}/${id}`)
  }
}

export default CustomersRepository
