import axios from 'axios'

const BASE = '/companies'

class CompanyRepository {
  static getCompany(id) {
    return axios(`${BASE}/${id}`)
  }

  static editCompany(company) {
    return axios.put(`${BASE}/${company.id}`, company)
  }
}

export default CompanyRepository
