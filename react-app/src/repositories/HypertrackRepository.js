import axios from 'axios'

class HypertrackRepository {
  static getSecretKey() {
    return axios('hypertrack/key')
  }
}

export default HypertrackRepository
