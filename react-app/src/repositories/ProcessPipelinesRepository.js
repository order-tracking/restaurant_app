import axios from 'axios'

class ProcessPipelinesRepository {
  static getProcessPipelines(page, limit) {
    return axios('/processpipelines', { params: { page: page, limit: limit } })
  }

  static getProcessPipeline(id) {
    return axios.get('/processpipelines/' + id)
  }

  static deleteProcessPipeline(id) {
    return axios.delete('/processpipelines/' + id)
  }

  static createProcessPipeline(processPipeline) {
    return axios.post('/processpipelines', processPipeline)
  }

  static editProcessPipeline(processPipeline) {
    return axios.put('/processpipelines/' + processPipeline.id, processPipeline)
  }

  static getProcessItems(id) {
    return axios.get('/processpipelines/processes/' + id + '/items')
  }
}

export default ProcessPipelinesRepository
