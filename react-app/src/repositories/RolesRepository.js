import axios from 'axios'

class RolesRepository {
  static getRoles() {
    return axios.get('/roles')
  }
}

export default RolesRepository
