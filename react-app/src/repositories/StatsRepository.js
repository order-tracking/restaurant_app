import axios from 'axios'

class StatsRepository {
  static getDailyStats() {
    return axios.get('/stats/today')
  }
}

export default StatsRepository
