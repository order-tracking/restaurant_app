export const getQueryString = params => {
  let url = new URLSearchParams()
  for (const paramName in params) {
    if (params[paramName]) url.set(paramName, params[paramName])
  }
  return url.toString()
}
