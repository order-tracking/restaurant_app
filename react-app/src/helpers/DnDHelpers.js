import HTML5Backend from 'react-dnd-html5-backend'
import TouchBackend from 'react-dnd-touch-backend'
import { TouchTransition } from 'react-dnd-multi-backend'

// Required to support both browser Drag and Drop as mobile Drag and Drop
export function getBackends() {
  return {
    backends: [
      {
        backend: HTML5Backend,
        preview: true
      },
      {
        backend: TouchBackend({ enableMouseEvents: true }),
        preview: true,
        transition: TouchTransition
      }
    ]
  }
}

export function dropTargetCollect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
  }
}

export function dragSourceCollect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  }
}
