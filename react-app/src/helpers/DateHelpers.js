import * as moment from 'moment'

/**
 * Parse UTC date to custom format with hours
 *
 *  @param date - Date in UTC
 * @returns {string}
 */
export const dateNormalWithHours = date => {
  let parsed = moment.utc(date).local()

  return parsed.format('DD/MM/YY HH[h]:mm[m]')
}

/**
 * Parse UTC date to custom format
 *
 *  @param date - Date in UTC
 * @returns {string}
 */
export const dateNormal = date => {
  let parsed = moment.utc(date).local()

  return parsed.format('DD/MM/YY')
}

/**
 * Calculates the difference of the current time with the time given
 *
 *  @param date - Date in UTC
 * @returns {int}
 */
export const differenceCurrentDate = date => {
  let parsed = moment.utc(date).local()

  let diff = moment.duration(parsed.diff(moment())).asMinutes()

  return Math.round(diff)
}

/**
 * Get hours from date
 *
 * FROM LOCAL DATE
 * @param date
 * @returns {string}
 */
export const getHoursFormated = date => {
  let parsed = moment(date)

  return parsed.format('HH[h]:mm[m]')
}

export const getTimeFromNow = minutes => {
  return moment().add(minutes, 'm')
}

export const getTimeFromNowText = date => {
  return moment
    .utc(date)
    .local()
    .fromNow()
}
