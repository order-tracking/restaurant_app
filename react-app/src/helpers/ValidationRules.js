/**
 * Validate valid number
 * @param value
 */
export const isNumber = value =>
  isNaN(Number(value)) ? 'Must be a number.' : true

/**
 * Validate email
 * @param value
 */
export const isEmail = value => {
  if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
    return 'Invalid email address.'
  }

  return true
}

/**
 * Minimum length
 * @param minVal
 */
export const min = minVal => value => {
  if (value.length < minVal) {
    return 'Minimum length is ' + minVal + '.'
  }

  return true
}

/**
 * Only numbers
 *
 * @param value
 */
export const numbers = value => {
  if (isNaN(value)) {
    return 'Only numbers'
  }

  return true
}

/**
 * Max length
 * @param maxVal
 */
export const max = maxVal => value => {
  if (value.length > maxVal) {
    return 'Maximum length is ' + maxVal + '.'
  }

  return true
}
