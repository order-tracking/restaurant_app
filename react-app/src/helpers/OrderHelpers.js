/**
 * Get a string with the order items
 *
 * @param order
 */
export const itemsToString = order => {
  let products = order.items.map(item => {
    return item.product.name
  })

  return strLimit(products.join(', '), 70)
}

export const mapLink = order => {
  let address = `${order.address}, ${order.postalCode}`

  return 'http://maps.google.com/?q=' + encodeURIComponent(address)
}

export const getKms = distance => {
  if (distance <= 0) {
    return '?'
  }

  return parseFloat(distance / 1000).toFixed(2)
}

/**
 * Cut long strings
 *
 * @param text
 * @param count
 * @param insertDots
 * @returns {string}
 */
export const strLimit = (text, count = 30, insertDots = true) => {
  return text.slice(0, count) + (text.length > count && insertDots ? '...' : '')
}
