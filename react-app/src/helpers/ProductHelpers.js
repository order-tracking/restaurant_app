import { strLimit } from './OrderHelpers'

class ProductHelpers {
  static typesToString(productTypes) {
    if (!productTypes || productTypes.length <= 0) return 'No Types'
    let types = productTypes.map(item => {
      return item.name
    })
    return strLimit(types.join(', '), 70)
  }
}

export default ProductHelpers
