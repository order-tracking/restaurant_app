# Restaurant Web App

[![build status](https://gitlab.com/order-tracking/restaurant_app/badges/master/build.svg)](https://gitlab.com/order-tracking/restaurant_app/commits/master)

## Folders

- `react-app` - The app built using `create-react-app` and Redux
- `mockApi` - Local API for testing the app to avoid the need for a local database while developing
- `server` - Node app to serve the static files of the app, we use this instead of nginx because maybe in the future we will need to implement server side rendering

## Continuous Delivery
Continuous Delivery is active for this repository.
When pushing on the:
- `master` branch:
    - The app is deployed to `https://otrack.info` (Production)
- `develop` branch:
    - The app is deployed to `https://staging.otrack.info` (Staging)

See the Pipelines for more details when pushing.

## Docker image

The docker image build process:

- Compiles the react app for production usage. (webpack bundle)
- Runs the node server

