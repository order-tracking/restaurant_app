import { Router, Request, Response } from 'express'
import * as faker from 'faker'
const router = Router()

router.get('/today', (req: Request, res: Response) => {
  res.send({
    pendingOrders: faker.random.number({ min: 1, max: 5 }),
    distributorsWorking: faker.random.number({ min: 1, max: 5 }),
    employeesWorking: faker.random.number({ min: 1, max: 5 }),
    ordersCompletedToday: faker.random.number({ min: 1, max: 5 })
  })
})

export default router
