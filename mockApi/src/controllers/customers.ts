import { Router, Request, Response } from 'express'
import { customers } from '../generators/CustomerGenerator'
import { collection, paginateStore } from '../utils'
import { createOrder } from '../generators/OrdersGenerator'
import { requiredFields } from '../middlewares'
import { random } from '../utils'
const router = Router()

const store = customers

/**
 * Get all customers paginated
 */
router.get('/', (req: Request, res: Response) => {
  res.send(paginateStore(req, store))
})

/**
 * Get customer by id
 */
router.get('/:id', (req: Request, res: Response) => {
  let search = store.find(u => u.id == req.params.id)

  if (!search) return res.sendNotFound()

  res.send(search)
})

/**
 * Get customer by phone
 * Should return the customer DTO with the last 5 orders
 */
router.get('/phoneNumber/:phoneNumber', (req: Request, res: Response) => {
  let result = store.find(
    customer => customer.phoneNumber == req.params.phoneNumber
  )

  if (result) {
    let customer = result

    if (Math.random() > 0.3) {
      // append some orders
      let orders = collection(id => createOrder(id, result), 5)

      // fix circular json
      customer = {
        ...result,
        orders: orders
      }
    } else {
      customer.orders = []
    }

    return res.send(customer)
  }

  return res.sendNotFound()
})

/**
 * Create a new customer
 */
router.post(
  '/',
  requiredFields('name', 'phoneNumber'),
  (req: Request, res: Response) => {
    const { name, phoneNumber } = req.body

    let exists = store.find(u => u.phoneNumber == phoneNumber)

    if (exists) return res.sendError('That phone number already exists.')

    let id = random(1000, 9999)
    store.push({
      id: id,
      name: name,
      phoneNumber: phoneNumber
    })

    res.createdAt('/customers/' + id)
  }
)

export default router
