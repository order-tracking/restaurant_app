import { Router, Request, Response } from 'express'
import { paginateStore } from '../utils'
import { IProduct, products } from '../generators/ProductGenerator'
const router = Router()

const store: IProduct[] = products

router.get('/', (req: Request, res: Response) => {
  res.send(paginateStore(req, store))
})

export default router
