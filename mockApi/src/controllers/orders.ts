import { Router, Request, Response } from 'express'
import { paginateStore } from '../utils'
import { IOrder, orders } from '../generators/OrdersGenerator'
import { requiredFields } from '../middlewares'
import * as faker from 'faker'
const router = Router()

const store: IOrder[] = orders

router.get('/', (req: Request, res: Response) => {
  res.send(paginateStore(req, store))
})

router.get('/:id', (req: Request, res: Response) => {
  let search = store.find(u => u.id == req.params.id)

  if (!search) return res.sendNotFound()

  res.send(search)
})

router.post(
  '/distance',
  requiredFields('address', 'address2', 'postalCode'),
  (req: Request, res: Response) => {
    const maxAllowed = 3000

    if (Math.random() < 0.2) { //simulate failure getting the distance
      return res.send({
        distance: -1,
        maxAllowedDistance: maxAllowed
      })
    }

    res.send({
      distance: faker.random.number({
        min: 100,
        max: 5000
      }),
      maxAllowedDistance: maxAllowed
    })
  }
)

export default router
