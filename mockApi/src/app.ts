import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as log4js from 'log4js'
import * as fs from 'fs'
import * as path from 'path'
import { utilsHandler, errorHandler, notFoundHandler } from './middlewares'

/**
 * log4js Logger
 */
const logger = log4js.getLogger('Express')

/**
 * Create the Express object
 */
const app = express()

/**
 * Express configuration
 */
app.use(log4js.connectLogger(logger, { level: 'auto' })) // replaces morgan
app.use(bodyParser.json())
app.use(utilsHandler) // adds some useful functions to req and res objects
//app.use(bodyParser.urlencoded({ extended: false }))

// Allow CORS
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type')
  next()
})

// Simulate some delay
app.use((req, res, next) => {
  setTimeout(next, 1000)
})

/**
 * Primary app routes
 */
const controllersPath = path.join(__dirname, '/controllers/')
fs.readdirSync(controllersPath).forEach(controller => {
  if (controller.endsWith('.js')) {
    let controllerPath = '/' + controller.split('.')[0]
    logger.warn('Loaded Controller:', controllerPath)
    app.use(
      controllerPath,
      require(path.join(controllersPath, controller)).default
    )
  }
})

/**
 * Catch 404
 */
app.use(notFoundHandler)

/**
 * Last error handler
 */
app.use(errorHandler)

export default app
