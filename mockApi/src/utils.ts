/**
 * oTrack pagination
 *
 * @param items
 * @param page
 * @param limit
 * @param totalItems
 * @returns {{count: any; limit: any; page: any; hasNextPage: boolean; hasPreviousPage: boolean; items: any}}
 */
export function paginate(items, page, limit, totalItems) {
  let totalPages = totalItems > 0 ? Math.ceil(totalItems / limit) : 0

  return {
    count: totalItems,
    limit: limit,
    page: page,
    hasNextPage: page < totalPages,
    hasPreviousPage: page > 1,
    items: items
  }
}

/**
 * Paginate array
 *
 * @param req
 * @param {Array<any>} store
 * @returns {{count: number; limit: any; page: number; hasNextPage: boolean; hasPreviousPage: boolean; items: Array<any>}}
 */
export function paginateStore(req: any, store: Array<any>) {
  let page = req.query.page ? req.query.page : 1
  let limit = req.query.limit ? req.query.limit : 10
  let idx = page - 1

  return paginate(
    store.slice(idx * limit, (idx + 1) * limit),
    page,
    limit,
    store.length
  )
}

/**
 * Create a collection where the id is passed to the entity
 *
 * @param {(i: number) => any} entity
 * @param {number} total
 * @param customEntities pre-defined entities
 * @returns {Array<Object>}
 */
export function collection<T>(
  entity: (i: number) => T,
  total: number = 100,
  customEntities: Array<T> = []
): Array<T> {
  let collection: Array<T> = []

  for (let i = 1; i <= total; ++i) {
    collection.push(entity(i))
  }

  return customEntities.concat(collection)
}

/**
 * Random between
 *
 * @param {number} min
 * @param {number} max
 * @returns {number}
 */
export function random(min: number, max: number): number {
  return Math.floor(Math.random() * (max - min + 1) + min)
}
