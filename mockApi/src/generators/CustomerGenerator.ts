import * as faker from 'faker'
import { collection } from '../utils'

export const customers: ICustomer[] = collection<ICustomer>(
  id => create(id),
  100,
  [
    {
      id: 1000,
      name: 'Carlos Florencio',
      phoneNumber: '925706007'
    }
  ]
)

export interface ICustomer {
  id: number
  name: string
  phoneNumber: string
  orders?: Array<any>
}

export function create(id: number): ICustomer {
  let customer = {
    id: id,
    name: faker.name.firstName() + ' ' + faker.name.lastName(),
    phoneNumber: faker.phone.phoneNumber('### ### ###')
  }

  return customer
}
