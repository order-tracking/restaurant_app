import * as faker from 'faker'
import { collection } from '../utils'
import { ICustomer, customers } from './CustomerGenerator'
import { createOrderItem, IOrderItem } from './OrderItemGenerator'

export const orders: IOrder[] = collection<IOrder>(id => createOrder(id))

export interface IOrder {
  id: number
  address: string
  address2: string
  postalCode: string
  readyToDistribute: boolean
  customer: ICustomer
  deliveredAt: Date
  createdAt: Date
  items: Array<IOrderItem>
}

export function createOrder(id: number, customer?: ICustomer): IOrder {
  return {
    id: id,
    address: faker.address.streetName(),
    address2: faker.address.secondaryAddress(),
    postalCode: faker.address.zipCode('####'),
    readyToDistribute: faker.random.boolean(),
    createdAt: faker.date.recent(),
    deliveredAt: faker.date.future(),
    customer: customer ? customer : faker.random.arrayElement(customers),
    items: collection<IOrderItem>(
      oId => createOrderItem(oId, id),
      faker.random.number({ min: 1, max: 10 })
    )
  }
}
