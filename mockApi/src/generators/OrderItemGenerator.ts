import * as faker from 'faker'
import { IProduct, IProductType, products } from './ProductGenerator'

export interface IOrderItem {
  id: number
  orderId: number
  product: IProduct
  observation?: string
  quantity: number
  completed: boolean
  productType: IProductType
}

export function createOrderItem(id: number, orderId: number): IOrderItem {
  let product: IProduct = faker.random.arrayElement(products)
  return {
    id: id,
    orderId: orderId,
    product: product,
    quantity: faker.random.number({ min: 1, max: 3 }),
    completed: faker.random.boolean(),
    productType: faker.random.arrayElement(product.types) as IProductType
  }
}
