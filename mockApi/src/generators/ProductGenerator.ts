import * as faker from 'faker'
import { collection } from '../utils'

export const products: IProduct[] = collection<IProduct>(id => create(id))

export interface IProduct {
  id: number
  name: string
  types: Array<IProductType>
}

export function create(id: number): IProduct {
  return {
    id: id,
    name: faker.lorem.words(2),
    types: collection(
      tId => createProductType(tId, id),
      faker.random.number({ min: 1, max: 3 })
    ) as IProductType[]
  }
}

/*
|--------------------------------------------------------------------------
| Product Type
|--------------------------------------------------------------------------
*/
export interface IProductType {
  id: number
  productId: number
  name: string
}

export function createProductType(id: number, productId: number): IProductType {
  let types = ['Large', 'Medium', 'Small', '500ml', '1L', 'XL']

  return {
    id: id,
    productId: productId,
    name: faker.random.arrayElement(types)
  }
}
