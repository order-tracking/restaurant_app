# Mock API for local testing


Endpoints implemented:

- `/stats/today`: GET
- `/products`: GET
- `/orders`: GET
- `/customers`: GET


## Requirements
- `node` (using v8)
- `yarn` (faster than npm)

## Instructions to develop
- `yarn install` - installs the dependencies
- `yarn start` - watch's `.ts` files and compiles to `build` folder then the server is reloaded at `localhost:3000`
- `yarn test` - runs the tests with jest (`typescript` tests)
- `yarn build` - compiles `.ts` files to `build` folder
- `yarn serve` - starts the app (`node build/bin/server.js`)
- `yarn prettier` - runs prettier to prettify the code (also runs before each commit)

## Docker instructions
- `docker build -t myapi .`
- `docker run -d -p 80:3000 myapi`
