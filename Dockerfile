FROM node:8

ARG REACT_APP_API_URL=https://api-staging.otrack.info
ARG REACT_APP_IDENTITY_URL=https://identity-staging.otrack.info
ARG REACT_APP_HYPERTRACK_KEY=pk_test_12730a0f97d7a2d3f5767a7721a5137f7bf2533f

COPY . /client

ENV CI=true
ENV REACT_APP_API_URL=${REACT_APP_API_URL}
ENV REACT_APP_IDENTITY_URL=${REACT_APP_IDENTITY_URL}
ENV REACT_APP_HYPERTRACK_KEY=${REACT_APP_HYPERTRACK_KEY}

# Build output files from the react app
WORKDIR /client/react-app
RUN yarn install --production=false
RUN yarn build

# Start the server to serve the react app
WORKDIR /client/server
RUN yarn install


EXPOSE 9000
CMD ["yarn","start"]
